import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FireService } from './services/fire.service';
import { Cliente } from './model/cliente.model';

declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  user: any;
  title = 'despacho-armas';
  aniversariantes: Cliente[] = [];
  constructor(
    private auth: AngularFireAuth,
    private authService: AuthService,
    private router: Router,
    private fireService: FireService,
  ) {
    this.auth.authState.subscribe((user) => {
      this.user = user;
    });

    setTimeout(() => {
      jQuery('[data-widget="treeview"]').Treeview('init');
      console.log(jQuery('ul').Treeview({}));
    }, 1000);

    this.fireService.getAniversariantes().then(a => {
      this.aniversariantes = a;
    })
  }

  goToAniversariantes() {
    this.router.navigateByUrl('aniversariantes', {
      state: { aniversariantes: this.aniversariantes },
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
