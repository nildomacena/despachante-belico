import { Cliente } from './../model/cliente.model';
import { PreCadastro } from './../model/preCadastro.model';
import { Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { HttpClient } from '@angular/common/http';
import { UtilService } from './../services/util.service';
import { Modalidade } from './../model/modalidade.model';
import { FireService } from './../services/fire.service';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { GenericValidator } from '../services/cpf.validator';
import { first } from 'rxjs/operators';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import * as firebase from 'firebase';

defineLocale('pt-br', ptBrLocale);

declare var jQuery: any;

@Component({
  selector: 'app-pre-cadastro',
  templateUrl: './pre-cadastro.component.html',
  styleUrls: ['./pre-cadastro.component.scss'],
})
export class PreCadastroComponent implements OnInit {
  formPreCadastro: FormGroup;
  modalidades: Modalidade[];
  mascaraTelefone: string = '(00)0000-0000||(00)00000-0000';
  isMilitar: boolean = false;
  preCadastro: PreCadastro;
  salvando: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private fireService: FireService,
    private utilService: UtilService,
    private http: HttpClient,
    private localeService: BsLocaleService,
    private router: Router
  ) {
    this.fireService.getPreCadastros().subscribe(async (values) => {
      console.log(await this.fireService.preCadastroParaCliente(values[0]));
    });
    this.localeService.use('pt-br');

    this.modalidades = this.utilService.modalidades;

    this.formPreCadastro = this.formBuilder.group({
      id: '',
      nome: new FormControl('', [Validators.required]),
      sexo: new FormControl('', [Validators.required]),
      dataNascimento: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [
        Validators.required,
        GenericValidator.isValidCpf(),
      ]),
      identidade: new FormControl('', [Validators.required]),
      naturalidade: new FormControl('', [Validators.required]),
      uf: new FormControl('', [Validators.required]),
      orgaoIdentidade: new FormControl('', [Validators.required]),
      expedicao: new FormControl(''),
      estadoCivil: new FormControl('', [Validators.required]),
      nomeMae: new FormControl(''),
      nomePai: new FormControl(''),
      profissao: new FormControl('', [Validators.required]),
      aposentado: new FormControl(''),
      ordem: new FormControl(''),
      posto: new FormControl(''),
      matricula: new FormControl(''),
      obm: new FormControl(''),
      logradouro: new FormControl(''),
      cep: new FormControl(''),
      bairro: new FormControl(''),
      complemento: new FormControl(''),
      numero: new FormControl(''),
      estado: new FormControl(''),
      cidade: new FormControl(''),
      telefone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      atividadeApostilada: new FormControl(''),
      modalidade: new FormControl('', [Validators.required]),
    });

    this.formPreCadastro.controls['modalidade'].valueChanges.subscribe(
      (value) => {
        console.log(value);
        if (value != 'cac' && value != 'cidadao') {
          this.formPreCadastro.controls['profissao'].patchValue(
            this.utilService.getModalidadeById(value).nome
          );
          this.isMilitar = false;
        } else {
          this.formPreCadastro.controls['profissao'].patchValue('');
        }
      }
    );

    if (this.router.getCurrentNavigation()?.extras?.state) {
      console.log(this.router.getCurrentNavigation().extras.state.preCadastro);
      this.updateForm(
        this.router.getCurrentNavigation().extras.state.preCadastro
      );
      //this.formCliente.patchValue(this.clienteNav.formGroup)
    }
  }

  ngOnInit(): void {}

  consolee() {
    console.log(this.formPreCadastro);
  }

  updateForm(preCadastro: PreCadastro) {
    this.preCadastro = preCadastro;
    this.formPreCadastro.patchValue(preCadastro);
    this.formPreCadastro.controls['dataNascimento'].patchValue(
      preCadastro.dataNascimento.toDate()
    );
    this.formPreCadastro.controls['expedicao'].patchValue(
      preCadastro.expedicao.toDate()
    );
  }

  async checaCPF(alterandoCliente: boolean): Promise<boolean> {
    if (
      this.formPreCadastro.controls['cpf'].invalid &&
      this.formPreCadastro.controls['cpf'].value
    ) {
      alert('CPF Inválido.\nVerifique as informações!');
      setTimeout(() => {
        jQuery('#cpf').focus();
      }, 500);
    } else if (
      !alterandoCliente &&
      !(await this.fireService.checaBDCpf(
        this.formPreCadastro.controls['cpf'].value
      ))
    ) {
      alert('CPF já cadastrado.\nVerifique as informações!');
      setTimeout(() => {
        jQuery('#cpf').focus();
      }, 500);
      return true;
    }
    console.log(this.formPreCadastro.controls['cpf']);
    return this.formPreCadastro.controls['cpf'].invalid;
  }

  async buscaCEP() {
    console.log(this.formPreCadastro.controls['cep']);
    if (this.formPreCadastro.controls['cep'].invalid) {
      return;
    }
    let url = `https://viacep.com.br/ws/${this.formPreCadastro.value['cep']}/json`;
    let response = await this.http.get(url).pipe(first()).toPromise();
    this.formPreCadastro.controls['logradouro'].patchValue(
      response['logradouro']
    );
    this.formPreCadastro.controls['bairro'].patchValue(response['bairro']);
    this.formPreCadastro.controls['cidade'].patchValue(response['localidade']);
    this.formPreCadastro.controls['estado'].patchValue(response['uf']);
    console.log(response);
  }

  async submitForm() {
    if (this.formPreCadastro.invalid) {
      this.utilService.toastrErro('Erro!', 'Formulário inválido');
      return;
    }
    if (!this.preCadastro) {
      this.novoCadastro();
    } else {
      this.confirmarPreCadastro();
    }
  }

  async novoCadastro() {
    console.log(this.formPreCadastro);
    try {
      this.salvando = true;
      let value = await this.fireService.salvarPreCadastro(
        this.formPreCadastro.value
      );
      alert('Pré cadastro salvo! ');
      this.utilService.toastrSucesso('Sucesso', 'Informações salvas');
      this.formPreCadastro.reset();
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro(
        'Erro durante o processo',
        error.code ?? error
      );
    }
    this.salvando = false;
  }

  async confirmarPreCadastro() {
    console.log('confirmarPreCadastro');
    try {
      this.salvando = true;
      let cliente: Cliente = await this.fireService.preCadastroParaCliente(
        this.formPreCadastro.value
      );
      this.router.navigateByUrl('cadastro-cliente', {
        state: { cliente: cliente },
      });
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro(
        'Erro durante o processo',
        error.code ?? error
      );
    }
    this.salvando = false;
  }
}
