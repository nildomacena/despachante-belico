import { Router } from '@angular/router';
import { FireService } from './../services/fire.service';
import { PreCadastro } from './../model/preCadastro.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-pre-cadastros',
  templateUrl: './lista-pre-cadastros.component.html',
  styleUrls: ['./lista-pre-cadastros.component.scss'],
})
export class ListaPreCadastrosComponent implements OnInit {
  preCadastros: PreCadastro[] = [];
  filtrados: PreCadastro[] = [];
  pesquisa: string;
  salvando: boolean;

  constructor(private fireService: FireService, private router: Router) {
    this.fireService.getPreCadastros().subscribe((p) => {
      this.preCadastros = this.filtrados = p;
      console.log(this.preCadastros);
    });
  }

  ngOnInit(): void {}

  onSearch() {
    console.log(this.pesquisa);
    if (this.pesquisa.length <= 0) {
      this.filtrados = this.preCadastros;
    } else {
      this.filtrados = this.filtrados.filter((a) =>
        a.nome.toLowerCase().includes(this.pesquisa.toLowerCase())
      );
    }
  }

  limparFiltros() {
    this.pesquisa = '';
    this.filtrados = this.preCadastros;
  }

  irParaPrecadastro(preCadastro: PreCadastro): void {
    this.router.navigateByUrl('pre-cadastro', {
      state: { preCadastro: preCadastro },
    });
  }
}
