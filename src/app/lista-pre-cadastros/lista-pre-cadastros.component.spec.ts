import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPreCadastrosComponent } from './lista-pre-cadastros.component';

describe('ListaPreCadastrosComponent', () => {
  let component: ListaPreCadastrosComponent;
  let fixture: ComponentFixture<ListaPreCadastrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaPreCadastrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPreCadastrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
