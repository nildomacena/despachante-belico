import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarTransferenciaComponent } from './cadastrar-transferencia.component';

describe('CadastrarTransferenciaComponent', () => {
  let component: CadastrarTransferenciaComponent;
  let fixture: ComponentFixture<CadastrarTransferenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastrarTransferenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
