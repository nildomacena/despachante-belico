import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Arma } from '../model/arma.model';
import { Calibre } from '../model/calibre.model';
import { Cliente } from '../model/cliente.model';
import { Fornecedor } from '../model/fornecedor.model';
import { Modelo } from '../model/modelo.model';
import { Transferencia } from '../model/transferencia.model';
import { FireService } from '../services/fire.service';
import { ProcedimentosService } from '../services/procedimentos.service';
import { UtilService } from '../services/util.service';

declare var jQuery: any;
@Component({
  selector: 'app-cadastrar-transferencia',
  templateUrl: './cadastrar-transferencia.component.html',
  styleUrls: ['./cadastrar-transferencia.component.scss'],
})
export class CadastrarTransferenciaComponent implements OnInit {
  formTransferencia: FormGroup;
  clientes: Cliente[];
  clienteSelecionado: Cliente;
  clientesFiltrados: Cliente[];
  modeloSelecionado: Modelo;
  modelos: Modelo[];
  modelosFiltrados: Modelo[];
  pesquisa: string;
  calibres: Calibre[];
  fornecedores: Fornecedor[];
  fornecedoresFiltrados: Fornecedor[];
  fornecedorSelecionado: Fornecedor;
  salvando: boolean;
  modalidade: string;
  arma: Arma;
  isSingleClick: Boolean = true; //Variável para controlar click simples e click duplo

  constructor(
    private fireService: FireService,
    private formBuilder: FormBuilder,
    private procedimentosService: ProcedimentosService,
    private utilService: UtilService,
    private router: Router
  ) {
    this.formTransferencia = this.formBuilder.group({
      doador: new FormControl('', [Validators.required]),
      doadorNome: new FormControl('', [Validators.required]),
      recebedor: new FormControl('', [Validators.required]),
      recebedorNome: new FormControl('', [Validators.required]),
      arma: new FormControl('', [Validators.required]),
      armaRegistro: new FormControl(''),
      observacao: new FormControl(''),
    });
    this.initPromises();
    console.log(this.router.getCurrentNavigation().extras.state);
    if (this.router.getCurrentNavigation()?.extras?.state) {
      if (this.router.getCurrentNavigation().extras.state.doador) {
        this.formTransferencia.patchValue({
          doador: this.router.getCurrentNavigation().extras.state.doador,
          doadorNome:
            this.router.getCurrentNavigation().extras.state.doador.nome,
        });
      }
      if (this.router.getCurrentNavigation().extras.state.recebedor) {
        this.formTransferencia.patchValue({
          recebedor: this.router.getCurrentNavigation().extras.state.recebedor,
          recebedorNome:
            this.router.getCurrentNavigation().extras.state.recebedor.nome,
        });
      }
      if (this.router.getCurrentNavigation().extras.state.arma) {
        this.formTransferencia.patchValue({
          arma: this.router.getCurrentNavigation().extras.state.arma,
          armaDescricao:
            this.router.getCurrentNavigation().extras.state.arma.descricao,
        });
        this.arma = this.router.getCurrentNavigation().extras.state.arma;
      }
      if (this.router.getCurrentNavigation().extras.state.modalidade) {
        console.log(
          'this.router.getCurrentNavigation().extras.state.modalidade',
          this.router.getCurrentNavigation().extras.state.modalidade
        );
        this.modalidade =
          this.router.getCurrentNavigation().extras.state.modalidade;
      }
    }
  }

  ngOnInit(): void {
    jQuery('#doadorModal').on('hidden.bs.modal', (e) => {
      if (this.clienteSelecionado) {
        this.formTransferencia.patchValue({
          doador: this.clienteSelecionado,
          doadorNome: this.clienteSelecionado.nome,
        });
      }
      this.pesquisa = '';
      this.clienteSelecionado = null;
      this.clientesFiltrados = this.clientes;
    });
    jQuery('#recebedorModal').on('hidden.bs.modal', (e) => {
      if (this.clienteSelecionado) {
        this.formTransferencia.patchValue({
          recebedor: this.clienteSelecionado,
          recebedorNome: this.clienteSelecionado.nome,
        });
      }
      this.pesquisa = '';
      this.clienteSelecionado = null;
      this.clientesFiltrados = this.clientes;
    });
  }

  async pesquisaArma() {
    this.salvando = true;
    try {
      this.arma = await this.fireService.getArmaByRegistro(
        this.formTransferencia.value['armaRegistro']
      );
      this.formTransferencia.patchValue({ arma: this.arma });
    } catch (error) {
      console.error(error);
      if (error == 'multiplos-cadastros')
        this.utilService.toastrErro(
          'Erro!',
          'Existe mais de um cadastro de arma com esse número de registro'
        );
      else if (error == 'nao-encontrado')
        this.utilService.toastrErro(
          'Erro!',
          'Não foram encontrados cadastros com esse número de registro. Verifique as informações e tente novamente'
        );
      else this.utilService.toastrErro();
    }
    this.salvando = false;
  }
  selecionarModalidade(modalidade: string) {
    this.modalidade = modalidade;
  }

  get modalidadeFormatada(): string {
    switch (this.modalidade) {
      case 'sigmaXsinarm':
        return 'Sigma X Sinarm';
      case 'sigmaXsigma':
        return 'Sigma X Sigma';
      case 'sinarmXsinarm':
        return 'Sinarm X Sinarm';
      case 'sinarmXsigma':
        return 'Sinarm X Sigma';
    }
  }

  async initPromises() {
    this.clientesFiltrados = this.clientes =
      await this.fireService.getClientes();
    this.modelosFiltrados = this.modelos = await this.fireService.getModelos();
    this.fornecedoresFiltrados = this.fornecedores =
      await this.fireService.getFornecedores();
    this.calibres = await this.fireService.getCalibres();
    //jQuery("#selectCalibres").selectpicker("refresh");
  }

  reset() {
    this.formTransferencia.reset();
    this.clienteSelecionado = this.arma = this.modalidade = null;
  }

  definirDoador(cliente: Cliente) {
    console.log('cliente: ', cliente);
    this.formTransferencia.patchValue({
      doador: cliente,
      doadorNome: cliente.nome,
    });
    this.pesquisa = '';
    jQuery('#doadorModal').modal('hide');
    this.clienteSelecionado = null;
    this.clientesFiltrados = this.clientes;
  }

  definirRecebedor(cliente: Cliente) {
    console.log('cliente: ', cliente);
    this.formTransferencia.patchValue({
      recebedor: cliente,
      recebedorNome: cliente.nome,
    });
    this.pesquisa = '';
    jQuery('#recebedorModal').modal('hide');
    this.clienteSelecionado = null;
    this.clientesFiltrados = this.clientes;
  }

  cadastrarCliente() {}

  closeSelectedCliente() {}

  onSearch() {
    if (this.pesquisa.length > 0) {
      this.clientesFiltrados = this.clientes.filter((c) =>
        c.pesquisa(this.pesquisa)
      );
    } else {
      this.clientesFiltrados = this.clientes;
    }
  }

  cadastrarArma() {
    this.router.navigateByUrl('cadastrar-arma', {
      state: {
        veioDeTransferencia: true,
        doador: this.formTransferencia.value['doador'],
        recebedor: this.formTransferencia.value['recebedor'],
        modalidade: this.modalidade,
      },
    });
  }

  onSelectCliente(cliente: Cliente) {
    setTimeout(() => {
      if (!this.isSingleClick) return;
      if (cliente == this.clienteSelecionado) this.clienteSelecionado = null;
      else this.clienteSelecionado = cliente;
    }, 250);
  }

  onSelectDoador(cliente: Cliente) {
    this.isSingleClick = false;
    this.definirDoador(cliente);
    this.isSingleClick = true;
  }

  onSelectRecebedor(cliente: Cliente) {
    this.isSingleClick = false;
    this.definirRecebedor(cliente);
    this.isSingleClick = true;
  }

  cancelarModalCliente() {
    this.clienteSelecionado = null;
  }

  async onSubmit() {
    let data = this.formTransferencia.value;
    /* if (data['doador'].cpf == data['recebedor'].cpf) {
      alert('Você não pode cadastrar uma transferência com o mesmo doador e recebedor');
      return;
    } */
    if (
      !confirm('Deseja realmente cadastrar essa Transferência?') ||
      this.formTransferencia.invalid
    )
      return;
    this.salvando = true;
    try {
      let transferencia: Transferencia =
        await this.procedimentosService.cadastrarTransferencia(
          data['doador'],
          data['recebedor'],
          data['arma'],
          this.modalidade,
          data['observacoes'] ?? ''
        );
      console.log(transferencia);
      this.reset();
      this.utilService.toastrSucesso();
      this.router.navigateByUrl('transferencia-detail', {
        state: { transferencia: transferencia },
      });
    } catch (error) {
      this.utilService.toastrErro();
      console.error(error);
    }
    this.salvando = false;
  }
}
