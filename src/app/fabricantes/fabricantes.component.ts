import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Fabricante } from '../model/fabricante.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-fabricantes',
  templateUrl: './fabricantes.component.html',
  styleUrls: ['./fabricantes.component.scss']
})
export class FabricantesComponent implements OnInit {
  fabricantes: Fabricante[] = [];
  formFabricante: FormGroup;
  fabricanteSelecionado: Fabricante;
  pesquisa: string;
  salvando: boolean;

  constructor(private fireService: FireService, private formBuilder: FormBuilder, private utilService: UtilService) {
    this.fireService.getFabricantes().then(fabricantes => {
      this.fabricantes = fabricantes;
      console.log(this.fabricantes);
    });
    this.formFabricante = this.formBuilder.group({
      'nome': new FormControl('', [Validators.required]),
      'cnpj': new FormControl('', [Validators.required,/*  Validators.pattern('^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$') */]),
    });
  }

  ngOnInit(): void {
  }

  onSelectFabricante(fabricante: Fabricante) {
    if (fabricante == this.fabricanteSelecionado) {
      this.resetForm();
      this.fabricanteSelecionado = null;
    }
    else {
      this.fabricanteSelecionado = fabricante;
      this.formFabricante.patchValue({
        nome: fabricante.nome,
        cnpj: fabricante.cnpj
      })
    }
  }

  resetForm() {
    this.fabricanteSelecionado = null;
    this.formFabricante.reset();
  }

  async deletarFabricante() {
    if (confirm(`Deseja realmente deletar o fabricante ${this.fabricanteSelecionado.nome}?`)) {
      try {
        this.salvando = true;
        this.fabricantes = await this.fireService.deleteFabricante(this.fabricanteSelecionado);
        this.resetForm();
        this.salvando = false;
        this.utilService.toastrSucesso();
      } catch (error) {
        this.salvando = false;
        console.error(error);
        this.utilService.toastrErro();
      }
    }
  }

  async submitForm() {
    if (this.formFabricante.invalid) {
      alert('Informações incompletas. Verifique os dados e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      this.fabricantes = await this.fireService.cadastrarFabricante(this.formFabricante.value['nome'].trim(), this.formFabricante.value['cnpj']);
      this.salvando = false;
      this.resetForm();
      this.utilService.toastrSucesso();
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
      console.error(error);
    }
  }

  consolee() {
    console.log(this.formFabricante)
  }
}
