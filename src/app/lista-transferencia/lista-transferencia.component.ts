import { Router } from '@angular/router';
import { Transferencia } from './../model/transferencia.model';
import { ProcedimentosService } from './../services/procedimentos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-transferencia',
  templateUrl: './lista-transferencia.component.html',
  styleUrls: ['./lista-transferencia.component.scss'],
})
export class ListaTransferenciaComponent implements OnInit {
  transferencias: Transferencia[] = [];
  transferenciasFiltradas: Transferencia[] = [];
  pesquisa: string;
  filtroNome: boolean;
  filtroArma: boolean;
  asc: boolean = false;
  filtro: string = '';

  constructor(
    private procedimentoService: ProcedimentosService,
    private router: Router
  ) {
    this.initPromises();
  }

  ngOnInit(): void {
    this.filtro = 'data-filtro';
    this.asc = false;
  }

  async initPromises() {
    this.transferenciasFiltradas = this.transferencias =
      await this.procedimentoService.getTransferencias();
  }

  pesquisar() {
    console.log(this.pesquisa);
    if (this.pesquisa.length > 0) {
      this.transferenciasFiltradas = this.transferenciasFiltradas.filter(
        (t) => {
          return t.pesquisa(this.pesquisa);
        }
      );
    } else this.transferenciasFiltradas = this.transferencias;
  }

  onSort(filtro: string) {
    console.log('onSort', filtro == 'doador');
    if (filtro == this.filtro) {
      this.asc = !this.asc;
    } else {
      this.asc = true;
    }
    this.filtro = filtro;
    if (filtro == 'doador') {
      this.transferenciasFiltradas = this.transferencias.sort((a, b) => {
        if (this.asc)
          return a.doador.nome.trim().toUpperCase() >
            b.doador.nome.trim().toUpperCase()
            ? 1
            : -1;
        else
          return a.doador.nome.trim().toUpperCase() >
            b.doador.nome.trim().toUpperCase()
            ? -1
            : 1;
      });
    }
    if (filtro == 'recebedor') {
      this.transferenciasFiltradas = this.transferencias.sort((a, b) => {
        if (this.asc)
          return a.recebedor.nome.trim().toUpperCase() >
            b.recebedor.nome.trim().toUpperCase()
            ? 1
            : -1;
        else
          return a.recebedor.nome.trim().toUpperCase() >
            b.recebedor.nome.trim().toUpperCase()
            ? -1
            : 1;
      });
    }
    if (filtro == 'data') {
      this.transferenciasFiltradas = this.transferencias.sort((a, b) => {
        if (this.asc) return a.data > b.data ? 1 : -1;
        else return a.data > b.data ? -1 : 1;
      });
    }
    if (filtro == 'arma') {
      this.transferenciasFiltradas = this.transferencias.sort((a, b) => {
        if (this.asc)
          return a.arma.modelo.nome.trim().toUpperCase() >
            b.arma.modelo.nome.trim().toUpperCase()
            ? 1
            : -1;
        else
          return a.arma.modelo.nome.trim().toUpperCase() >
            b.arma.modelo.nome.trim().toUpperCase()
            ? -1
            : 1;
      });
    }
    /* if (campo == 'nome') {
      if (this.filtroNome) {
        this.asc = !this.asc;
      }
      this.filtroNome = true;
      this.filtroArma = this.filtroAtividade = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        if (a.cliente.nome > b.cliente.nome)
          return this.asc ? 1 : -1;
        else if (a.cliente.nome < b.cliente.nome)
          return this.asc ? -1 : 1;
        else
          return 0;
      });
    }
    else if (campo == 'atividade') {
      if (this.filtroAtividade) {
        this.asc = !this.asc;
      }
      this.filtroAtividade = true;
      this.filtroArma = this.filtroNome = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        console.log(a.ultimaAtividade.getTime(), b.ultimaAtividade.getTime())
        if (a.ultimaAtividade.getTime() > b.ultimaAtividade.getTime())
          return this.asc ? 1 : -1;
        else if (a.ultimaAtividade.getTime() < b.ultimaAtividade.getTime())
          return this.asc ? -1 : 1;
        else
          return 0;
      });
    }
    else if (campo == 'arma') {
      if (this.filtroArma) {
        this.asc = !this.asc;
      }
      this.filtroArma = true;
      this.filtroNome = this.filtroAtividade = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        if (a.modelo.nome > b.modelo.nome)
          return this.asc ? 1 : -1;
        else if (a.modelo.nome < b.modelo.nome)
          return this.asc ? -1 : 1;
        else
          return 0;
      });
    } */
  }

  irParaTransferencia(transferencia: Transferencia) {
    console.log(transferencia);
    this.router.navigateByUrl('transferencia-detail', {
      state: { transferencia: transferencia },
    });
  }
}
