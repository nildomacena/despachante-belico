import { FireService } from './fire.service';
import { first } from 'rxjs/operators';
import { Transferencia } from './../model/transferencia.model';
import { Injectable } from '@angular/core';
import { Aquisicao } from '../model/aquisicao.model';
import {
  Cell,
  PdfMakeWrapper,
  Table,
  Txt,
  Line,
  Canvas,
  Img,
  Columns,
  Stack,
  IStack,
  ICell,
  Rect,
} from 'pdfmake-wrapper';
import { UtilService } from './util.service';
import { Cliente } from '../model/cliente.model';
import { StyleDefinition } from 'pdfmake-wrapper/lib/definitions/style-definition';
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { TableCell, TDocumentDefinitions } from 'pdfmake/interfaces';
import { HttpClient } from '@angular/common/http';
import { style } from '@angular/animations';

(<any>pdfmake).vfs = pdfFonts.pdfMake.vfs;

//(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

const BORDA_LATERAL: [boolean, boolean, boolean, boolean] = [
  true,
  false,
  true,
  false,
];
const BORDA_LATERAL_BAIXO: [boolean, boolean, boolean, boolean] = [
  true,
  false,
  true,
  true,
];

@Injectable({
  providedIn: 'root',
})
export class PdfService {
  constructor(
    private utilService: UtilService,
    private fireService: FireService
  ) {
    /*  pdfMake.fonts = {
       myCustom: {
         normal: "times-new-roman.ttf",
         bold: 'times-new-roman-grassetto.ttf',
         italics: 'times-new-roman.ttf',
         bolditalics: 'times-new-roman.ttf'
       },
     } */
  }

  gerarRequerimento(aquisicao: Aquisicao, cliente: Cliente) {
    console.log(aquisicao);
    const pdf = new PdfMakeWrapper();
    pdf.defaultStyle({
      fontSize: 11,
    });
    let primeiroParagrafo: string;
    let tabelaAnexos: any;
    if (!aquisicao.modalidade || aquisicao.modalidade == 'pm') {
      primeiroParagrafo = `Eu, ${cliente.nome}, identidade RGPM nº ${cliente.identidade}, CPF nº ${cliente.cpfFormatado}, nº de Ordem ${cliente.ordem}, e-mail: ${aquisicao.email}, celular ${cliente.telefoneFormatado}, posto/grad/função ${cliente.posto}, vinculado à Polícia Militar do Estado de Alagoas.`;
    }
    if (aquisicao.modalidade == 'bm')
      primeiroParagrafo = `Eu, ${cliente.nome}, identidade RGBM nº ${cliente.identidade}, CPF nº ${cliente.cpfFormatado}, nº de Ordem ${cliente.ordem}, e-mail: ${aquisicao.email}, celular ${cliente.telefoneFormatado}, posto/grad/função ${cliente.posto}, vinculado ao Corpo de Bombeiros Militar do Estado de Alagoas.`;
    if (cliente.aposentadoAMaisdeCincoAnos) {
      tabelaAnexos = new Table([
        [
          new Txt('( X ) Cópia de identidade funcional').alignment('left').end,
          new Txt(
            '( X ) Comprovante de pagamento de taxa de aquisição de PCE'
          ).alignment('right').end,
        ],
        [
          new Cell(
            new Txt(`( X ) Comprovante de residência`).alignment('left').end
          ).end,
          new Cell(
            new Txt(`( X ) Laudo Psicológico`)
              .alignment('left')
              .margin([45, 0]).end
          ).end,
        ],
      ])
        .alignment('center')
        .layout('noBorders')
        .margin([0, 8, 0, 0])
        .widths(['auto', 320]).end;
    } else {
      tabelaAnexos = new Table([
        [
          new Txt('( X ) Cópia de identidade funcional').alignment('left').end,
          new Txt(
            '( X ) Comprovante de pagamento de taxa de aquisição de PCE'
          ).alignment('right').end,
        ],
        [
          new Cell(
            new Txt(`( X ) Comprovante de residência`).alignment('justify').end
          ).colSpan(2).end,
        ],
      ])
        .alignment('center')
        .layout('noBorders')
        .margin([0, 8, 0, 0])
        .widths(['auto', 320]).end;
    }
    pdf.pageMargins([40, 30, 40, 20]);
    pdf.add(new Txt('Anexo C').alignment('center').end);
    pdf.add(
      new Txt('REQUERIMENTO PARA AQUISIÇÃO DE ARMA DE FOGO E ACESSÓRIO')
        .alignment('center')
        .margin(10).end
    );
    pdf.add(
      new Txt(primeiroParagrafo).alignment('justify').margin([0, 10]).end
    );
    pdf.add(
      new Txt('DECLARO que:').alignment('left').margin([0, 5, 45, 5]).end
    );
    pdf.add(
      new Txt(
        '1) a quantidade de arma de fogo a ser adquirida, conforme este requerimento, somada às que já possuo, não extrapola a quantidade prevista no § 8º do art. 3º do Decreto nº9845/2019.'
      ).alignment('justify').end
    );
    pdf.add(
      new Txt(
        '2) a arma de fogo a ser adquirida deverá ser registrada no órgão ao qual estou vinculado e cadastrada no SIGMA;'
      ).alignment('justify').end
    );
    pdf.add(
      new Txt(
        '3) no caso de indeferimento do cadastro da arma no SIGMA, deverei realizar o distrato da compra junto ao fornecedor; e'
      ).alignment('justify').end
    );
    pdf.add(
      new Txt(
        '4) não estou respondendo a inquérito ou a processo criminal por crime doloso.'
      ).alignment('justify').end
    );
    pdf.add(
      new Txt(
        'REQUEIRO autorização para aquisição da(s) arma(s) de fogo a seguir discriminada(s):'
      )
        .alignment('justify')
        .margin([0, 10, 0, 10]).end
    );
    pdf.add(
      new Table([
        ['tipo', 'calibre', 'marca/modelo', 'quantidade'],
        [
          aquisicao.modelo.especie.nome,
          aquisicao.calibre.nome,
          aquisicao.modelo.fabricante.nome + '/' + aquisicao.modelo.nome,
          '1',
        ],
        [
          new Cell(
            new Txt(
              `Fornecedor: ${aquisicao.fornecedor.descricaoCompleta}`
            ).alignment('justify').end
          ).colSpan(4).end,
        ],
        [
          new Cell(
            new Txt(`Local de entrega: ${aquisicao.local}`).alignment(
              'justify'
            ).end
          ).colSpan(4).end,
        ],
      ])
        .alignment('center')
        .heights((row: number) => {
          return 20;
        })
        .widths(['*', 100, 'auto', 100]).end
    );
    pdf.add(
      new Txt('JUSTIFICATIVA PARA AQUISIÇÃO:')
        .alignment('left')
        .margin([0, 15, 0, 0]).end
    );
    pdf.add(new Txt('Para defesa pessoal:').alignment('left').end);
    pdf.add(
      new Txt('ANEXOS (ver orientação no verso)')
        .alignment('left')
        .margin([0, 15, 0, 0]).end
    );
    pdf.add(tabelaAnexos);
    pdf.add(
      new Txt(`Maceió/AL, em ${this.utilService.dataAtualPorExtenso()}`)
        .alignment('center')
        .margin([0, 20, 0, 0]).end
    );
    pdf.add(
      new Txt(`____________________________________ `)
        .alignment('center')
        .margin([0, 25, 0, 0]).end
    );
    pdf.add(
      new Txt(`${cliente.nome}`).alignment('center').margin([0, 0, 0, 0]).end
    );
    if (aquisicao.modalidade == 'pm')
      pdf.add(
        new Txt(`RGPM: nº ${cliente.identidade}`)
          .alignment('center')
          .margin([0, 0, 0, 0]).end
      );
    if (aquisicao.modalidade == 'bm')
      pdf.add(
        new Txt(`RGBM: nº ${cliente.identidade}`)
          .alignment('center')
          .margin([0, 0, 0, 0]).end
      );
    pdf.add(
      new Table([
        [
          new Cell(
            new Txt('DESPACHO DO ÓRGÃO DE VINCULAÇÃO DO ADQUIRENTE')
              .alignment('left')
              .margin([0, 7, 0, 0])
              .fontSize(12).end
          )
            .alignment('center')
            .fillColor('#B9B9B9').end,
        ],
        [
          new Cell(
            new Txt(
              `( ) DEFERIDO - Autorização nº ____________________ /___________ de ____/ ____/ __________`
            )
              .margin([0, 10, 0, 0])
              .alignment('left').end
          ).border([true, false, true, false]).end,
        ],
        [
          new Cell(
            new Txt(`( ) DEFERIDO`).alignment('left').margin([0, 0, 0, 10]).end
          ).border(BORDA_LATERAL).end,
        ],
        [
          new Cell(
            new Canvas([new Line([0, 0], [510, 0]).lineWidth(0.2).end]).end
          )
            .margin([0, 7, 0, 15])
            .border(BORDA_LATERAL_BAIXO).end,
        ],
        [
          new Cell(
            new Txt(`Maceio/AL, ____/____/_________`)
              .alignment('center')
              .margin([0, 20, 0, 15]).end
          ).border(BORDA_LATERAL).end,
        ],
        [
          new Cell(new Canvas([new Line([0, 0], [200, 0]).end]).end)
            .margin([-10, 10, 0, 0])
            .border(BORDA_LATERAL).end,
        ],
        [
          new Cell(
            new Txt(`Chefe da 2ª Seção/EMG`)
              .alignment('center')
              .margin([0, 0, 0, 20]).end
          ).border(BORDA_LATERAL_BAIXO).end,
        ],
      ])
        .alignment('center')
        .margin([0, 8, 0, 0])
        .widths([500]).end
    );
    pdf.create().download(`Requerimento: ${cliente.nome}`);
  }

  gerarEntradaProcesso(aquisicao: Aquisicao) {
    /** Left top Right  Bottom */
    /*     const documentDefinition = { content: 'This is an sample PDF printed with pdfMake' };
        pdfMake.createPdf(documentDefinition,).download();
        console.log(pdfMake.fonts); */
    const pdf = new PdfMakeWrapper();
    pdf.defaultStyle({
      fontSize: 14,
    });

    pdf.add(
      new Txt('POLÍCIA MILITAR DO ESTADO DE ALAGOAS')
        .alignment('center')
        .margin([0, 0, 0, 0]).end
    );
    pdf.add(new Txt('2ª Seção do EMG').alignment('center').margin(0).end);
    pdf.add(
      new Txt('Serviço de Identificação do Policial Militar')
        .alignment('center')
        .margin(0).end
    );
    pdf.add(new Txt('SIDPOM: 3021-5021').alignment('center').margin(0).end);
    pdf.add(
      new Table([
        [
          new Cell(
            new Txt(`Data: ${this.utilService.dataAtual()}`)
              .fontSize(14)
              .alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt('ENTRADA DO PROCESSO').bold().alignment('center').end
          ).end,
          new Cell(new Txt(``).alignment('left').end).border([false]).end,
        ],
      ])
        .alignment('center')
        .margin([0, 15, 0, 0])
        .widths(['*', 'auto', '*']).end
    );

    pdf.add(
      new Table([
        [
          new Cell(
            new Txt(`REQUERENTE:`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`${aquisicao.cliente.nome}`)
              .fontSize(14)
              .decoration('underline')
              .alignment('left')
              .margin([0, 0]).end
          ).border([false]).end,
        ],
      ])
        .alignment('center')
        .margin([0, 15, 0, 0])
        .widths(['auto', '*']).end
    );

    pdf.add(
      new Table([
        [
          new Cell(
            new Txt(`CEDENTE:`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`________________________________________________________`)
              .fontSize(11)
              .alignment('left')
              .margin([0, 0]).end
          ).border([false]).end,
        ],
      ])
        .alignment('center')
        .margin([0, 15, 0, 0])
        .widths(['auto', 'auto']).end
    );

    pdf.add(
      new Table([
        [
          new Cell(
            new Txt(`( ) Transferência`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`( ) Renovação`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`( ) Registro`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`( ) Baixa`).fontSize(14).alignment('left').end
          ).border([false]).end,
        ],
        [
          new Cell(
            new Txt(`( X ) Aut. Aquisição`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`( ) 2ª Via CRAF`).fontSize(14).alignment('left').end
          ).border([false]).end,
          new Cell(
            new Txt(`( ) Autorização Para Transferência`)
              .fontSize(14)
              .alignment('left').end
          )
            .colSpan(2)
            .border([false, false]).end,
        ],
      ])
        .alignment('center')
        .margin([0, 15, 0, 0])
        .widths(['*', '*', '*', '*']).end
    );

    pdf.add(
      new Table([
        [
          new Cell(
            new Txt(`Tipo: ${aquisicao.modelo.especie.nome}`)
              .fontSize(12)
              .alignment('left').end
          ).border([false, false]).end,
          new Cell(
            new Txt(`Marca: ${aquisicao.modelo.fabricante.nome}`)
              .fontSize(11)
              .alignment('left')
              .margin([0, 0]).end
          ).border([false]).end,
          new Cell(
            new Txt(`Modelo: ${aquisicao.modelo.nome}`)
              .fontSize(12)
              .alignment('left')
              .margin([0, 0]).end
          ).border([false]).end,
          new Cell(
            new Txt(`Nº Série:`)
              .fontSize(12)
              .alignment('left')
              .margin([0, 0]).end
          ).border([false]).end,
        ],
        [
          new Cell(
            new Txt(`___________________________________`)
              .fontSize(12)
              .alignment('center').end
          )
            .colSpan(4)
            .margin([0, 20, 0, 0])
            .border([false, false]).end,
        ],
        [
          new Cell(
            new Txt(`Assinatura do Agente Recebedor`)
              .fontSize(12)
              .alignment('center').end
          )
            .colSpan(4)
            .margin(0)
            .border([false, false]).end,
        ],
      ])
        .alignment('center')
        .margin([0, 15, 0, 0])
        .widths(['*', '*', '*', '*']).end
    );
    pdf.add(
      new Canvas([new Rect([20, 10], [570, 380]).end]).absolutePosition(-10, 0)
        .end
    );
    pdf.create().download(`Requerimento: ${aquisicao.cliente.nome}`);
  }

  async gerarRequerimentoBombeiro(cliente: Cliente) {
    const pdf = new PdfMakeWrapper();
    pdf.defaultStyle({
      fontSize: 12,
    });
    let tabelaServidor: Table = new Table([
      [new Cell(new Txt('Servidor').bold().end).colSpan(4).end],
      [
        new Cell(new Txt('Ativa').end).end,
        new Cell(new Txt('X').end).end,
        new Cell(new Txt('Inativo').end).end,
        new Cell(new Txt('').end).end,
      ],
    ]);
    let campoNome: ICell = new Cell(
      new Stack([
        new Txt('Nome Completo').bold().end,
        new Txt(cliente.nome).margin([0, 10]).end,
      ]).end
    ).colSpan(2).end;

    pdf.pageMargins([80, 20, 40, 20]);
    pdf.images({
      cabecalho: await new Img('assets/imgs/cabecalho-bombeiros.png').build(),
    });
    pdf.add(
      await new Img('assets/imgs/cabecalho-bombeiros.png')
        .alignment('center')
        .width(500)
        .build()
    );
    pdf.create().download(`Requerimento bombeiro: ${cliente.nome}`);
    pdf.add(
      new Table([
        /*Primeira linha*/
        [
          new Cell(
            new Txt('IDENTIFICAÇÃO DO REQUERENTE')
              .alignment('center')
              .fontSize(12)
              .bold().end
          ).colSpan(5).end,
        ],
        [
          new Cell(
            new Txt('IDENTIFICAÇÃO DO REQUERENTE')
              .alignment('center')
              .fontSize(12)
              .bold().end
          ).colSpan(1).end,
          new Cell(
            new Txt('IDENTIFICAÇÃO DO REQUERENTE')
              .alignment('center')
              .fontSize(12)
              .bold().end
          ).colSpan(4).end,
        ],
        /*Segunda  linha*/

        //[campoNome, new Cell(new Txt('teste').end).end, new Cell(new Txt('teste').end).colSpan(2).end,]
        //[new Cell(new Stack([new Txt('Nome Completo').bold().end, new Txt(cliente.nome).margin([0, 10]).end]).end).border([true, true, true, true]).end, new Cell(new Txt('teste').end).end],
      ]).widths(['*', '*', '*', '*', '*']).end
    );
  }

  gerarProcuracao(cliente: Cliente, outorgado: string) {
    console.log(cliente);
    const pdf = new PdfMakeWrapper();
    let outorgadoMacena = outorgado == 'ambos' || outorgado == 'ednildo';
    let outorgadoRaniere = outorgado == 'ambos' || outorgado == 'mathews';

    pdf.defaultStyle({
      fontSize: 12,
    });
    if (outorgado == 'mathews') {
      outorgado =
        'Mathews Raniere Gomes Isidoro da Silva, brasileiro, alagoano, solteiro, filho de Edivaldo Isidoro da Silva e Maria Eugenia Gomes Isidoro da Silva, Administrador, Portador do CPF: 098.947.394-58, RG 34040463 SEDES/AL.'.toUpperCase();
    } else if (outorgado == 'ednildo') {
      outorgado =
        'Ednildo Macena da Silva, brasileiro, alagoano, casado, servidor público estadual, filho de Euclides Macena de Lemos e Inez Macena da Silva, portador de CPF n* 438.798.704-97, RG nº 99001248650 SSP/AL.'.toUpperCase();
    } else {
      outorgado =
        'Ednildo Macena da Silva, brasileiro, alagoano, casado, servidor público estadual, filho de Euclides Macena de Lemos e Inez Macena da Silva, portador de CPF n* 438.798.704-97, RG nº 99001248650 SSP/AL e Mathews Raniere Gomes Isidoro da Silva, brasileiro, alagoano, solteiro, filho de Edivaldo Isidoro da Silva e Maria Eugenia Gomes Isidoro da Silva, Administrador, Portador do CPF: 098.947.394-58, RG 34040463 SEDES/AL.'.toUpperCase();
    }

    pdf.pageMargins([80, 40, 40, 20]);
    pdf.add(
      new Txt('PROCURAÇÃO - INSTRUMENTO PARTICULAR DE PROCURAÇÃO.')
        .decoration('underline')
        .alignment('center').end
    );
    pdf.add(
      new Txt('OUTORGANTE:').alignment('justify').margin([0, 20, 0, 3]).end
    );
    pdf.add(
      new Txt(
        `${cliente.nome.toLocaleUpperCase()}, brasileiro, natural de ${
          cliente.naturalidade
        }/${cliente.uf.toUpperCase()}, nascido em ${
          cliente.dataNascimentoFormatada
        }, filho de ${cliente.nomePai ? cliente.nomePai + ' e ' : ''}${
          cliente.nomeMae
        }, portador do CPF: ${cliente.cpfFormatado}, Identidade: ${
          cliente.identidade
        } ${cliente.orgaoIdentidade.toUpperCase()}, ${
          cliente.profissao
        }, residente no(a) ${cliente.endereco1.logradouro}, ${
          cliente.endereco1.numero
        }, ${cliente.endereco1.bairro}, ${
          cliente.endereco1.complemento
            ? cliente.endereco1.complemento + ', '
            : ''
        } ${
          cliente.endereco1.cidade
        }/${cliente.endereco1.estado.toUpperCase()}, CEP: ${
          cliente.endereco1.cep
        }`.toUpperCase()
      ).end
    );
    if (outorgadoMacena) {
      pdf.add(
        new Txt('OUTORGADO:').alignment('justify').margin([0, 30, 0, 3]).end
      );
      pdf.add(
        new Txt(
          'Ednildo Macena da Silva, brasileiro, alagoano, casado, servidor público estadual, filho de Euclides Macena de Lemos e Inez Macena da Silva, portador de CPF: 438.798.704-97, RG nº 99001248650 SSP/AL.'.toUpperCase()
        ).alignment('justify').end
      );
    }

    if (outorgadoRaniere) {
      pdf.add(
        new Txt('OUTORGADO:').alignment('justify').margin([0, 30, 0, 3]).end
      );
      pdf.add(
        new Txt(
          'Mathews Raniere Gomes Isidoro da Silva, brasileiro, alagoano, solteiro, filho de Edivaldo Isidoro da Silva e Maria Eugenia Gomes Isidoro da Silva, Administrador, Portador do CPF: 098.947.394-58, RG 34040463 SEDES/AL.'.toUpperCase()
        ).alignment('justify').end
      );
    }
    pdf.add(new Txt('PODERES:').alignment('justify').margin([0, 40, 0, 3]).end);
    pdf.add(
      new Txt(
        `OUTORGANDO-LHE(S) OS PODERES NECESSÁRIOS PARA REPRESENTAR O OUTORGANTE JUNTO A POLICIA FEDERAL, EXÉRCITO BRASILEIRO – DIRETORIA DE FISCALIZAÇÃO DE PRODUTOS CONTROLADOS E SUA REDE DE FISCALIZAÇÃO EM TODO O TERRITÓRIO NACIONAL, JUSTIÇA ESTADUAL, JUSTIÇA FEDERAL, JUSTIÇA MILITAR, JUSTIÇA ELEITORAL E DEMAIS REPARTIÇÕES PÚBLICAS FEDERAIS E ESTADUAIS, A FIM DE ATENDER DISPOSIÇÕES CONTIDAS NA LEGISLAÇÃO EM VIGOR, CONFERINDO, AINDA, AO DITO OUTORGADO PODERES ESPECIAIS PARA, EM NOME DO(A) OUTORGANTE, REQUERER, ASSINAR E ENCAMINHAR ATESTADO DE ANTECEDENTES CRIMINAIS, CERTIDÕES, E DOCUMENTOS REFERENTE A CONCESSÃO DE CR, RENOVAÇÃO DE CR, CANCELAMENTO DE CR, TRANSFERÊNCIA ENTRE ACERVOS, TRANSFERÊNCIAS DE ARMAS, APOSTILAMENTOS, CRAFS, GUIA(S) DE TRÁFEGO, AQUISIÇÃO DE ARMAS, AQUISIÇÃO DE PRODUTOS CONTROLADOS, RENOVAÇÃO DE REGISTRO(S) DE ARMA(S), PEDIDO DE ANDAMENTO OU REANÁLISE DE PROCESSO, RETIRAR DOCUMENTOS, DESARQUIVAR PROCESSOS, DESISTIR DO REQUERIMENTO, TRANSFERÊNCIA(S) - ATENDER E RECEBER OFÍCIOS E INTIMAÇÕES, ACOMPANHAR A FISCALIZAÇÃO PARA VISTORIA TÉCNICA MILITAR, PEDIR, ASSINAR E REQUERER VISTAS  DE PROCESSOS, RETIRAR OS MENCIONADOS DOCUMENTOS QUANDO DA EXPEDIÇÃO, BEM COMO ENVIAR COBRANÇA REFERENTE AOS SERVIÇOS ACIMA SOLICITADOS.`.toUpperCase()
      ).alignment('justify').end
    );

    pdf.add(
      new Txt(`MACEIÓ/AL, ${this.utilService.dataAtualPorExtenso().toUpperCase()}.`)
        .alignment('justify')
        .margin([0,5, 0, 5]).end
    );
    pdf.add(
      new Txt('___________________________')
        .alignment('center')
        .margin([0, 30, 0, 3]).end
    );
    pdf.add(
      new Txt(`${cliente.nome.toLocaleUpperCase()}`).alignment('center').end
    );
    pdf.add(new Txt(`CPF: ${cliente.cpfFormatado}`).alignment('center').end);
    pdf.create().download(`Procuração: ${cliente.nome}`);
  }

  gerarDeclaracaoResidencia(
    cliente: Cliente,
    nome: string,
    endereco: string,
    nacionalidade: string,
    naturalidade: string,
    parentesco: string,
    identidade: string,
    orgaoIdentidade: string,
    cpf: string,
    orgaoResidencia: string
  ) {
    const pdf = new PdfMakeWrapper();
    pdf.defaultStyle({
      fontSize: 13,
    });
    let primeiroParagrafo: string = `Eu, ${nome}, ${nacionalidade}, natural de  ${naturalidade}/${cliente.uf.toUpperCase()}, portador do RG nº ${identidade} ${orgaoIdentidade}, CPF nº ${cpf}, declaro para os devidos fins de comprovação ${
      orgaoResidencia == 'pf' ? 'junto à Polícia Federal ' : ''
    } que o Sr. ${cliente.nome}, portador da cédula de identidade nº ${
      cliente.identidade
    } ${cliente.orgaoIdentidade}, CPF ${cliente.cpfFormatado}, ${
      cliente.filiacao
    }, ${
      parentesco ? 'meu ' + parentesco.toLocaleLowerCase() + ',' : ''
    } reside no endereço de minha propriedade em ${endereco}${
      orgaoResidencia == 'exercito'
        ? ' e está autorizado a guardar seu acervo de atirador no respectivo endereço.'
        : '.'
    }`;
    let segundoParagrafo: string =
      'Por ser verdade, dato e assino o presente documento, declarando estar ciente de que responderei criminalmente em caso de falsidade das informações aqui prestadas.';
    pdf.pageMargins([80, 80, 40, 20]);
    pdf.add(new Txt('DECLARAÇÃO').fontSize(16).bold().alignment('center').end);
    pdf.add(
      new Txt(primeiroParagrafo).alignment('justify').margin([0, 40, 0, 0]).end
    );
    pdf.add(
      new Txt(segundoParagrafo).alignment('justify').margin([0, 10, 0, 3]).end
    );
    pdf.add(
      new Txt(`Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`)
        .alignment('center')
        .margin([0, 40, 0, 15]).end
    );
    pdf.add(
      new Txt('___________________________________________')
        .alignment('center')
        .margin([0, 40, 0, 3]).end
    );
    pdf.add(new Txt(`${nome.toLocaleUpperCase()}`).alignment('center').end);
    pdf.add(
      new Txt(`CPF nº ${this.utilService.formatarCPF(cpf)}`).alignment('center')
        .end
    );
    pdf.create().download(`Declaração residência: ${cliente.nome}`);
  }

  /**TRANSFERÊNCIAS */

  anexoK(transferencia: Transferencia) {
    let acervo: string;
    if (transferencia.recebedor.modalidade.toLowerCase() == 'cac') {
      acervo = 'ATIRADOR DESPORTIVO';
    } else {
      acervo = 'ACERVO PESSOAL';
    }

    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'Anexo K - REQUERIMENTO PARA TRANSFERÊNCIA DE PROPRIEDADE DE ARMA DE FOGO – SIGMA PARA SIGMA (colecionador, atirador desportivo, caçador e entidade de tiro)',
          style: 'cabecalho',
          alignment: 'center',
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ADQUIRENTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' +
                            transferencia.recebedor.identidadeFormatada,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.recebedor.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'CR: ' + transferencia.recebedor.cr ?? '',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Telefone: ' +
                            transferencia.recebedor.telefoneFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Email: ' + transferencia.recebedor.email,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ALIENANTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' +
                            transferencia.doador.identidadeFormatada,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.doador.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'CR: ' + transferencia.doador.cr,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Telefone: ' +
                            transferencia.doador.telefoneFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Email: ' + transferencia.doador.email,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DA ARMA',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Tipo: ' + transferencia.arma.modelo.especie.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Número de série: ' + transferencia.arma.numero,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Marca: ' +
                            transferencia.arma.modelo.fabricante.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Nº SIGMA: ' + transferencia.arma.sigma,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Modelo: ' + transferencia.arma.modelo.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Outras especificações: (quando for o caso)',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Calibre: ' + transferencia.arma.calibre.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Acessórios e/ou sobressalentes: (quando for o caso) ',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          table: {
            widths: '*',
            body: [
              [
                {
                  margin: [0, 10, 0, 5],
                  text: 'ACERVO DE DESTINO: ' + acervo,
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'ANEXOS',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: '( X ) comprovante de taxa de aquisição',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) cópia de identificação do alienante',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) cópia de identificações do adquirente',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) cópia do CRAF da arma objeto de transferência',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) comprovante de taxa de apostilamento - adquirente',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) comprovante de taxa de apostilamento - alienante',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) comprovante de taxa de registro',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) comprovante de taxa de registro',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: 'Declaro estar de acordo com a transferência de propriedade da arma objeto da presente transação.',
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          text:
                            'Maceió, ' + this.utilService.dataAtualPorExtenso(),
                          alignment: 'center',
                          margin: 5,
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          lineHeight: 1.2,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ) +
                            `\nAdquirente`,
                          alignment: 'center',
                          margin: 10,
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                        {
                          lineHeight: 1.2,
                          text:
                            '_________________________\n' +
                            transferencia.doador.nome.toUpperCase() +
                            `\nAlienante`,
                          alignment: 'center',
                          margin: 10,
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'DESPACHO DA OM DO SISFPC',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*'],
                    body: [
                      [
                        {
                          text: '(	) DEFERIDO - Autorizo a transferência da arma de fogo em questão.  Publique-se.',
                          lineHeight: 1.1,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.5,
                          text: '(	) INDEFERIDO\n________________________________________________________________________________________\n________________________________________________________________________________________',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text: '__________________________________________\nLocal e data',
                          alignment: 'center',
                          margin: [25, 0, 0, 30],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text: '_________________________\nNome completo e cargo',
                          alignment: 'center',
                          margin: [10, 5, 0, 25],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
        },
        header: {
          fontSize: 11,
          bold: true,
        },
        cabecalho: {
          fontSize: 11,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Anexo K');
  }

  async anexoF1(transferencia: Transferencia) {
    console.log(transferencia);
    const comprimentoCano = transferencia.arma.comprimentoCano;
    let modelo = await this.fireService.getModeloById(
      transferencia.arma.modelo.id
    );
    transferencia.arma.modelo = await this.fireService.getModeloById(
      transferencia.arma.modelo.id
    );
    console.log(modelo);
    let pdf: TDocumentDefinitions = {
      content: [
        { text: 'Anexo F1', style: 'cabecalho', alignment: 'center' },
        {
          text: 'FICHA CADASTRO DE ARMA DE FOGO NO SIGMA',
          style: 'cabecalho',
          alignment: 'center',
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*', '*', '*'],
            body: [
              [
                { text: 'Nº SÉRIE DA ARMA', style: 'tableContent' },
                { text: `${transferencia.arma.numero}`, style: 'tableContent' },
                { text: 'MARCA', style: 'tableContent' },
                {
                  style: 'tableContent',
                  text: `${transferencia.arma.modelo.fabricante.nome.toUpperCase()}`,
                },
              ],
              [
                { text: 'MODELO', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.nome}`,
                  style: 'tableContent',
                },
                { text: 'PAÍS DE FABRICAÇÃO', style: 'tableContent' },
                { text: `${transferencia.arma.pais}`, style: 'tableContent' },
              ],
              [
                { text: 'TIPO DE FUNCIONAMENTO ', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.funcionamentoFormatado.toUpperCase()}`,
                  style: 'tableContent',
                },
                { text: 'ESPÉCIE', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.especie.nome.toUpperCase()}`,
                  style: 'tableContent',
                },
              ],
              [
                { text: 'CALIBRE', style: 'tableContent' },
                {
                  text: `${transferencia.arma.calibre.nome.toUpperCase()}`,
                  style: 'tableContent',
                  colSpan: 3,
                },
              ],
              [
                { text: 'ACABAMENTO', style: 'tableContent' },
                {
                  text: `${transferencia.arma.acabamento.toUpperCase()}`,
                  style: 'tableContent',
                  colSpan: 3,
                },
              ],
              [
                { text: 'QUANTIDADE DE CANOS', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.numeroCanos}`,
                  style: 'tableContent',
                },
                { text: 'COMPRIMENTO DO CANO', style: 'tableContent' },
                {
                  text: comprimentoCano,
                  style: 'tableContent',
                },
              ],
              [
                { text: 'TIPO DE ALMA', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.tipoAlma.toUpperCase()}`,
                  style: 'tableContent',
                },
                { text: 'Nº DE RAIAS', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.quantidadeRaias}`,
                  style: 'tableContent',
                },
              ],
              [
                { text: 'CAPACIDADE CARREGAMENTO', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.capacidadeTiro}`,
                  style: 'tableContent',
                },
                { text: 'SENTIDO DA RAIA', style: 'tableContent' },
                {
                  text: `${transferencia.arma.modelo.sentidoRaias.toUpperCase()}`,
                  style: 'tableContent',
                },
              ],
            ],
          },
        },

        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          /* style: 'cabecalho', */
          alignment: 'center',
          margin: [0, 30, 0, 10],
        },
        {
          text: '___________________________________',
          alignment: 'center',
          margin: [0, 40, 0, 10],
        },
        {
          text: `${this.utilService.firstLetterUppercase(
            transferencia.recebedor.nome
          )}\nREQUERENTE`,
          alignment: 'center',
          margin: [0, 0, 0, 10],
        },
      ],
      styles: {
        tableContent: {
          fontSize: 11,
          alignment: 'center',
          margin: [0, 10],
        },
        cabecalho: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 10],
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5],
        },
        tableExample: {
          margin: [0, 15, 0, 15],
        },
        tableHeader: {
          bold: true,
          fontSize: 15,
          color: 'black',
        },
      },
    };
    pdfmake.createPdf(pdf).download('Anexo F1');
  }

  anexoH(transferencia: Transferencia) {
    console.log(transferencia.recebedor);
    let acervo: string;
    if (transferencia.recebedor.modalidade.toLowerCase() == 'cac') {
      acervo = 'ATIRADOR DESPORTIVO';
    } else {
      acervo = 'ACERVO PESSOAL';
    }

    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'Anexo H',
          style: 'cabecalho',
          alignment: 'center',
        },
        {
          text: 'REQUERIMENTO PARA TRANSFERÊNCIA DE ARMA DE FOGO - SINARM para SIGMA (colecionador, atirador desportivo e caçador e entidade de tiro)',
          style: 'cabecalho',
          alignment: 'center',
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ADQUIRENTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['auto', '*', '*'],
                    body: [
                      [
                        {
                          text: 'Atividade: Tiro Desportivo',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ),
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' + transferencia.recebedor.identidade,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.recebedor.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'OM do SisFPC de vinculação: 7ªRM',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'CR: ' + transferencia.recebedor.cr,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ALIENANTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' +
                            transferencia.doador.identidadeFormatada,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.doador.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Endereço Completo: ' +
                            transferencia.doador.endereco1Formatado +
                            ', CEP: ' +
                            transferencia.doador.endereco1.cep,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DA ARMA OBJETO DA AQUISIÇÃO',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Tipo: ' + transferencia.arma.modelo.especie.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Número de série: ' + transferencia.arma.numero,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Marca: ' +
                            transferencia.arma.modelo.fabricante.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Nº SINARM: ' + transferencia.arma.sinarmFormatado,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Modelo: ' + transferencia.arma.modelo.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Outras especificações: (quando for o caso): ',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Calibre: ' + transferencia.arma.calibre.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Acessórios e/ou sobressalentes: (quando for o caso): ',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          table: {
            widths: '*',
            body: [
              [
                {
                  margin: [0, 5, 0, 5],
                  text: `ACERVO DE DESTINO: ATIRADOR DESPORTIVO`,
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'ANEXOS',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: '( X ) cópia de documento de identificação (alienante)',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X )  ficha cadastro de arma de fogo no SIGMA',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) cópia de documento de identificação (adquirente)',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) comprovante pgto da taxa de aquisição de PCE',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) cópia do CRAF da arma',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) anuência do SINARM',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) comprovante pgto taxa de registro',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) comprovante pgto taxa de apostilamento',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: 'Declaro estar de acordo com a transferência de propriedade da arma objeto da presente transação.',
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          text:
                            'Maceió, ' + this.utilService.dataAtualPorExtenso(),
                          alignment: 'center',
                          margin: 4,
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ) +
                            `\nAlienante`,
                          alignment: 'center',
                          margin: [15, 15, 15, 5],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ) +
                            `\nAdquirente`,
                          alignment: 'center',
                          margin: [15, 15, 15, 5],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'DESPACHO DA OM SISFPC',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*'],
                    body: [
                      [
                        {
                          text: '(	) DEFERIDO - Autorizo a transferência da arma de fogo em questão.  Publique-se.',
                          lineHeight: 1.3,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) INDEFERIDO',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Arma e/ou calibre não previstos na Portaria  nº ______-COLOG/2019.',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Quantitativo de armas de fogo já atingido.',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Outros motivos:',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '__________________________________________________________________________________________\n__________________________________________________________________________________________',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text: '_________________________\nNome completo e cargo\nOM SisFPC',
                          alignment: 'center',
                          margin: [25, 25, 0, 30],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
        },
        header: {
          fontSize: 12,
          bold: true,
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Anexo H');
  }

  anexoL(transferencia: Transferencia) {
    let tableCell: TableCell[];
    if (
      transferencia.recebedor.modalidade == 'pm' ||
      transferencia.recebedor.modalidade == 'bm'
    ) {
      tableCell = [
        {
          text: 'Número de ordem: ' + transferencia.recebedor.ordem,
          border: [false, false, false, false],
        },
        {
          text: transferencia.recebedor.matricula
            ? 'Matrícula: ' + transferencia.recebedor.matricula
            : '',
          border: [false, false, false, false],
        },
      ];
    } else
      tableCell = [
        { text: '', colSpan: 2, border: [false, false, false, false] },
      ];
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'Anexo L',
          style: 'cabecalho',
          alignment: 'center',
        },
        {
          text: 'REQUERIMENTO PARA TRANSFERÊNCIA DE ARMA DE FOGO – SIGMA PARA SIGMA (PM/CBM, ABIN e GSI)',
          style: 'cabecalho',
          alignment: 'center',
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ADQUIRENTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Posto/grad/função: ' +
                            transferencia.recebedor.posto,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ),
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Identidade: ' +
                            transferencia.recebedor.identidadeFormatada,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Telefone: ' +
                            transferencia.recebedor.telefoneFormatado,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Email: ' + transferencia.recebedor.email,
                          border: [false, false, false, false],
                        },
                        {
                          text: 'CPF: ' + transferencia.recebedor.cpfFormatado,
                          border: [false, false, false, false],
                        },
                      ],
                      tableCell,
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ALIENANTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' +
                            transferencia.doador.identidadeFormatada,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.doador.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'CR: ' + transferencia.doador.cr,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Telefone: ' +
                            transferencia.doador.telefoneFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Email: ' + transferencia.doador.email,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DA ARMA OBJETO DA AQUISIÇÃO',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Tipo: ' + transferencia.arma.modelo.especie.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Número de série: ' + transferencia.arma.numero,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Marca: ' +
                            transferencia.arma.modelo.fabricante.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Nº SIGMA: ' + transferencia.arma.sigma,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Modelo: ' + transferencia.arma.modelo.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Outras especificações: (quando for o caso): ',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Calibre: ' + transferencia.arma.calibre.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Acessórios e/ou sobressalentes: (quando for o caso): ',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          table: {
            widths: '*',
            body: [
              [
                {
                  margin: [0, 10, 0, 10],
                  text:
                    'ACERVO DE DESTINO DA ARMA DE FOGO: Acervo pessoal ' +
                    transferencia.recebedor.endereco1Formatado.toUpperCase(),
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'ANEXOS',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: '( X ) Documento de identificação do adquirente',
                          border: [false, false, false, false],
                        },
                        {
                          text: '( X ) Comprovante de residência do adquirente',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) Documento de identificação do alienante',
                          border: [false, false, false, false],
                        },
                        {
                          text: '( X ) Comprovante de residência do alienante',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) Comprovante de taxa de aquisição',
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                      ],
                      //[{ text: '( X ) Cópia da autorização para aquisição por transferência do órgão de vinculação', border: [false, false, false, false], }],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: 'Declaro estar de acordo com a transferência de propriedade da arma objeto da presente transação.',
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          text:
                            'Maceió, ' + this.utilService.dataAtualPorExtenso(),
                          alignment: 'center',
                          margin: 4,
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ) +
                            '\nAlienante',
                          alignment: 'center',
                          margin: [15, 15, 15, 5],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ) +
                            '\nAdquirente',
                          alignment: 'center',
                          margin: [15, 15, 15, 5],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'DESPACHO DA OM SISFPC',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*'],
                    body: [
                      [
                        {
                          text: '(	) DEFERIDO\nAutorizo a transferência da arma de fogo em questão.  Publique-se.',
                          lineHeight: 1.3,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) INDEFERIDO',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '__________________________________________________________________________________________\n__________________________________________________________________________________________',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text: '_________________________',
                          alignment: 'center',
                          margin: [25, 25, 0, 40],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
        },
        header: {
          fontSize: 12,
          bold: true,
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Anexo L');
  }

  anexoG(transferencia: Transferencia) {
    let comprovantePsic: TableCell[];
    let orgao: string;
    switch (transferencia.recebedor.modalidade) {
      case 'pm':
        orgao = 'Polícia Militar de Alagoas';
        break;
      case 'bm':
        orgao = 'Corpo de Bombeiros Militar de Alagoas';
        break;
      default:
        orgao = '';
        break;
    }
    //Checa se o cliente é pm aposentando há mais de cinco anos. Se for, exibe o anexo correspondente
    if (transferencia.recebedor.aposentadoAMaisdeCincoAnos)
      comprovantePsic = [
        {
          text: '(x) comprovante de aptidão psicológica e capacidade técnica',
          style: { fontSize: 10 },
          border: [false, false, true, false],
        },
        { text: '', border: [false, false, false, false] },
      ];

    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'Anexo G\nREQUERIMENTO PARA TRANSFERÊNCIA DE ARMA DE FOGO - SINARM para SIGMA (PM/CBM)',
          style: 'cabecalho',
          alignment: 'center',
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ADQUIRENTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: 'Posto/grad: ' + transferencia.recebedor.posto,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Nº de ordem: ' + transferencia.recebedor.ordem,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' + transferencia.recebedor.identidade,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.recebedor.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Órgão de vinculação: ' + orgao,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Email: ' + transferencia.recebedor.email,
                          style: 'semBordas',
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ALIENANTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: [200, '*'],
                    body: [
                      [
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: `Identidade: ${transferencia.doador.identidade} ${transferencia.doador.orgaoIdentidade}`,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.doador.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Endereço completo: ' +
                            transferencia.doador.endereco1Formatado +
                            ' CEP: ' +
                            transferencia.doador.endereco1.cep,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DA ARMA',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Tipo: ' + transferencia.arma.modelo.especie.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Número de série: ' + transferencia.arma.numero,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Marca: ' +
                            transferencia.arma.modelo.fabricante.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Nº SINARM: ' + transferencia.arma.sinarmFormatado,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Modelo: ' + transferencia.arma.modelo.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Outras especificações: (quando for o caso)',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Calibre: ' + transferencia.arma.calibre.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Acessórios e/ou sobressalentes: (quando for o caso) ',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        /* {
          table: {
            widths: '*',
            body: [[{ margin: [0, 10, 0, 10], text: 'ACERVO DE DESTINO DA ARMA DE FOGO: ' + transferencia.recebedor.endereco1Formatado.toUpperCase() }],]
          }
        }, */
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'ANEXOS',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: '(x) documento de identificação (alienante)',
                          border: [false, false, true, false],
                        },
                        {
                          text: '(x) documento de identificação (adquirente)',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(x) comprovante de residência (alienante)',
                          border: [false, false, true, false],
                        },
                        {
                          text: '(x) comprovante de residência (adquirente)',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(x) anuência do SINARM',
                          border: [false, false, true, false],
                        },
                        {
                          text: '(x) cópia do CRAF da arma',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(x) comprovante de pag. da taxa de aquisição de PCE',
                          border: [false, false, true, false],
                        },
                        { text: '', border: [false, false, false, false] },
                      ],
                      comprovantePsic ?? [
                        { text: '', border: [false, false, false, false] },
                        { text: '', border: [false, false, false, false] },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: 'Declaro estar de acordo com a transferência de propriedade da arma objeto da presente transação.',
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          text:
                            'Maceió, ' + this.utilService.dataAtualPorExtenso(),
                          alignment: 'center',
                          margin: 7,
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ) +
                            '\nAlienante',
                          alignment: 'center',
                          margin: 20,
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ) +
                            '\nAdquirente',
                          alignment: 'center',
                          margin: 20,
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'DESPACHO DA OM DO SISFPC',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*'],
                    body: [
                      [
                        {
                          text: '(	) DEFERIDO - Autorizo a transferência da arma de fogo em questão.  Publique-se.',
                          lineHeight: 1.3,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) INDEFERIDO',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Arma e/ou calibre não previstos na Portaria  nº ______-COLOG/2019.',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Quantitativo de armas de fogo já atingido.',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Outros motivos:',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '__________________________________________________________________________________________\n__________________________________________________________________________________________',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text: '_________________________',
                          alignment: 'center',
                          margin: [25, 25, 0, 40],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
        },
        header: {
          fontSize: 11,
          bold: true,
        },
        cabecalho: {
          fontSize: 11,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Anexo G');
  }

  async anexoI(transferencia: Transferencia) {
    console.log(transferencia.doador);
    let textoAtividade: string;
    let om: string;
    if (transferencia.doador.modalidade == 'pm') {
      om = 'PMAL';
      textoAtividade = 'Posto/grad: ' + transferencia.doador.posto;
    } else if (transferencia.doador.modalidade == 'bm') {
      om = 'CBMAL';
      textoAtividade = 'Posto/grad: ' + transferencia.doador.posto;
    } else if (
      transferencia.doador.modalidade == 'pc' ||
      transferencia.doador.modalidade == 'gm' ||
      transferencia.doador.modalidade == 'pp' ||
      transferencia.doador.modalidade == 'prf' ||
      transferencia.doador.modalidade == 'pf'
    ) {
      om = '59 BIMTz/7RM';
      textoAtividade =
        'Cargo/função: ' +
        this.utilService.getModalidadeById(transferencia.doador.modalidade)
          .nome;
    } else {
      om = '59 BIMTz/7RM';
      textoAtividade = 'Atividade: Tiro Desportivo';
    }

    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'Anexo I',
          style: 'cabecalho',
          alignment: 'center',
        },
        {
          text: 'REQUERIMENTO PARA TRANSFERÊNCIA DE ARMA DE FOGO - SIGMA PARA SINARM (todos)',
          style: 'cabecalho',
          alignment: 'center',
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ALIENANTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['auto', '*', 'auto'],
                    body: [
                      [
                        {
                          text: textoAtividade,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ),
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' +
                            transferencia.doador.identidadeFormatada,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.doador.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'OM do SisFPC: ' + om,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'CR: ' + transferencia.doador.cr ?? '',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DO ADQUIRENTE',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['auto', '*', 'auto'],
                    body: [
                      [
                        {
                          text:
                            'Prerrogativa: ' +
                            transferencia.recebedor.profissaoFormatada,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Nome: ' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ),
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Identidade: ' + transferencia.recebedor.identidade,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'CPF: ' + transferencia.recebedor.cpfFormatado,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text:
                            'Endereço Completo: ' +
                            transferencia.recebedor.endereco1Formatado,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'IDENTIFICAÇÃO DA ARMA OBJETO DA AQUISIÇÃO',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text:
                            'Tipo: ' + transferencia.arma.modelo.especie.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Número de série: ' + transferencia.arma.numero,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text:
                            'Marca: ' +
                            transferencia.arma.modelo.fabricante.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Nº SIGMA: ' + transferencia.arma.sigma,
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Modelo: ' + transferencia.arma.modelo.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Outras especificações: (quando for o caso): ',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'Calibre: ' + transferencia.arma.calibre.nome,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                        {
                          text: 'Acessórios e/ou sobressalentes: (quando for o caso): ',
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'ANEXOS',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: '( X ) Documento de identificação (alienante)',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) Comprovante de residência (alienante)',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) Documento de identificação (adquirente)',
                          border: [false, false, true, false],
                        },
                        {
                          text: '( X ) Comprovante de residência (adquirente)',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '( X ) Cópia do CRAF da arma',
                          border: [false, false, true, false],
                        },
                        { text: '', border: [false, false, false, false] },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    //border: [false, false, false, false],
                    widths: ['*', '*'],
                    body: [
                      [
                        {
                          text: 'Declaro estar de acordo com a transferência de propriedade da arma objeto da presente transação.',
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          text:
                            'Maceió, ' + this.utilService.dataAtualPorExtenso(),
                          alignment: 'center',
                          margin: 4,
                          fontSize: 11,
                          border: [false, false, false, false],
                          colSpan: 2,
                        },
                        {},
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.doador.nome
                            ) +
                            '\nAlienante',
                          alignment: 'center',
                          margin: [15, 15, 15, 0],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                        {
                          lineHeight: 1.3,
                          text:
                            '_________________________\n' +
                            this.utilService.firstLetterUppercase(
                              transferencia.recebedor.nome
                            ) +
                            '\nAdquirente',
                          alignment: 'center',
                          margin: [15, 15, 15, 5],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'DESPACHO DA OM SISFPC',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                },
                {},
              ],
              [
                {
                  colSpan: 2,
                  style: 'tableDados',
                  table: {
                    border: [false, false, false, false],
                    widths: ['*'],
                    body: [
                      [
                        {
                          image: await this.utilService.assetToBase64(
                            'assets/imgs/logo-federal.png'
                          ),
                          height: 30,
                          width: 30,
                          alignment: 'center',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: 'MINISTÉRIO DA DEFESA\nEXÉRCITO BRASILEIRO',
                          lineHeight: 1.1,
                          style: 'semBordas',
                          alignment: 'center',
                          margin: [10, 0, 0, 5],
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) DEFERIDO - Autorizo a transferência da arma de fogo para o SINARM.  Publique-se.  Aguardar comunicação do SINARM para atualização do cadastro no SIGMA.',
                          lineHeight: 1.1,
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) INDEFERIDO',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Arma e/ou calibre não previstos na Portaria  nº ______-COLOG/2019.',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '(	) Outros motivos:',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          text: '__________________________________________________________________________________________\n__________________________________________________________________________________________',
                          style: 'semBordas',
                          border: [false, false, false, false],
                        },
                      ],
                      [
                        {
                          lineHeight: 1.3,
                          text: '_________________________\nNome completo e cargo\nOM do SisFPC',
                          alignment: 'center',
                          margin: [25, 15, 0, 5],
                          fontSize: 11,
                          border: [false, false, false, false],
                        },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
        },
        header: {
          fontSize: 12,
          bold: true,
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Anexo I');
  }

  termoDeTransferencia(transferencia: Transferencia) {
    console.log(transferencia);
    let textoAutorizacao: string;
    let cellSinarm: TableCell;
    let linhaAdquirente: any;
    let mesmaPessoa = transferencia.recebedor.id == transferencia.doador.id;
    if (transferencia.arma.isSinarm) {
      cellSinarm = {
        text: 'SINARM nº: ' + transferencia.arma.sinarmFormatado,
        style: 'tableHeader',
        alignment: 'left',
      };
    } else
      cellSinarm = {
        text: 'SIGMA nº: ' + transferencia.arma.sigma,
        style: 'tableHeader',
        alignment: 'left',
      };
    if (mesmaPessoa) {
      textoAutorizacao = `EU, ${this.utilService.firstLetterUppercase(
        transferencia.doador.nome
      )}, portador${
        transferencia.doador.sexo == 'feminino' ? 'a' : ''
      } da cédula de identidade nº ${
        transferencia.doador.identidadeFormatada
      }, CPF Nº ${transferencia.doador.cpfFormatado}, ${
        transferencia.doador.profissaoFormatada
      }, ${transferencia.doador.filiacao}, RESIDENTE NO(A) ${
        transferencia.doador.endereco1Formatado
      }, AUTORIZO A TRANSFERÊNCIA DA ARMA DE FOGO ABAIXO DESCRITA, DO SISTEMA SINARM PARA O SISTEMA SIGMA, EM RAZÃO DESTE SIGNATÁRIO SER ${transferencia.recebedor.profissaoFormatada.toUpperCase()} DO ESTADO DE ALAGOAS, CONFORME CÓPIA DE IDENTIDADE FUNCIONAL EM ANEXO:`.toUpperCase();
      linhaAdquirente = {};
    } else {
      textoAutorizacao =
        'da arma de sua propriedade, com as seguintes características:';
      linhaAdquirente = {
        text: `_________________________________\n${this.utilService.firstLetterUppercase(
          transferencia.recebedor.nome
        )}\nADQUIRENTE`.toUpperCase(),
        margin: 12,
        alignment: 'center',
        lineHeight: 1.5,
        fontSize: 12,
      };
    }
    let pdf: TDocumentDefinitions = {
      content: [
        {
          margin: [0, 5, 0, 5],
          text: 'TERMO DE TRANSFERÊNCIA',
          style: 'header',
          alignment: 'center',
        },
        {
          text: mesmaPessoa
            ? textoAutorizacao
            : `
          ${this.utilService.firstLetterUppercase(
            transferencia.doador.nome
          )}, portador${
                transferencia.doador.sexo == 'feminino' ? 'a' : ''
              } da cédula de identidade nº ${
                transferencia.doador.identidadeFormatada
              }, CPF Nº ${transferencia.doador.cpfFormatado}, ${
                transferencia.doador.profissaoFormatada
              }, ${transferencia.doador.filiacao}, residente no(a) ${
                transferencia.doador.endereco1Formatado
              }, AUTORIZA A TRANSFERÊNCIA ao Sr. ${this.utilService.firstLetterUppercase(
                transferencia.recebedor.nome
              )}, portador da cédula de identidade ${
                transferencia.recebedor.identidade
              } ${transferencia.recebedor.orgaoIdentidade}, CPF Nº ${
                transferencia.recebedor.cpfFormatado
              }, ${transferencia.recebedor.profissaoFormatada}, ${
                transferencia.recebedor.filiacao
              }, residente no(a) ${
                transferencia.recebedor.endereco1Formatado
              }, ${textoAutorizacao}`.toUpperCase(),
          alignment: 'justify',
          lineHeight: 1.5,
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            heights: [15],
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'DADOS DA ARMA',
                  style: 'tableHeader',
                  colSpan: 2,
                  alignment: 'center',
                  bold: true,
                },
                {},
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [25],
            widths: ['*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Nota fiscal: ',
                  style: 'tableHeader',
                  alignment: 'left',
                },
                { text: 'Data: ', style: 'tableHeader', alignment: 'left' },
                {
                  text: 'Pessoa Jurídica: ',
                  style: 'tableHeader',
                  alignment: 'left',
                },
                { text: 'CNPJ: ', style: 'tableHeader', alignment: 'left' },
              ],
            ],
          },
        },

        {
          style: 'tableExample',
          table: {
            heights: [25],
            widths: ['*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Número da arma: ' + transferencia.arma.numero,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text:
                    'Registro federal: ' + transferencia.arma.registroFederal,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text: 'Órgão expedidor: ' + transferencia.arma.expedidor,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text:
                    'Data de emissão: ' +
                    this.utilService.formatarData(
                      transferencia.arma.dataEmissao
                    ),
                  style: 'tableHeader',
                  alignment: 'left',
                },
              ],
            ],
          },
        },

        {
          style: 'tableExample',
          table: {
            heights: [25],
            widths: ['*', '*', '*', '*'],
            body: [
              [
                cellSinarm,
                {
                  text: 'Espécie: ' + transferencia.arma.modelo.especie.nome,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text: 'Marca: ' + transferencia.arma.modelo.fabricante.nome,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text: 'Modelo: ' + transferencia.arma.modelo.nome,
                  style: 'tableHeader',
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [25],
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Calibre: ' + transferencia.arma.calibre.nome,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text: 'País de fabricação: Brasil ',
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text:
                    'Capacidade de Tiros: ' +
                      transferencia.arma.modelo.capacidadeTiro ?? '',
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text: 'Nº de Canos: ' + transferencia.arma.modelo.numeroCanos,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text:
                    'Comprimento do cano: ' +
                    transferencia.arma.comprimentoCano,
                  style: 'tableHeader',
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [25],
            widths: ['*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Tipo de alma: ' + transferencia.arma.modelo.tipoAlma,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text:
                    'Quantidade de raias: ' +
                    transferencia.arma.modelo.quantidadeRaias,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text: 'Marca: ' + transferencia.arma.modelo.fabricante.nome,
                  style: 'tableHeader',
                  alignment: 'left',
                },
                {
                  text:
                    'Funcionamento: ' +
                    transferencia.arma.modelo.funcionamentoFormatado,
                  style: 'tableHeader',
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [20],
            widths: ['*'],
            body: [
              [
                {
                  text:
                    'Acabamento: ' +
                    transferencia.arma.acabamento.toUpperCase(),
                  style: 'tableHeader',
                  alignment: 'left',
                  height: 60,
                },
              ],
            ],
          },
        },

        {
          text: `MACEIÓ/AL, ${this.utilService.dataAtualPorExtenso()}`,
          margin: 12,
          alignment: 'center',
          lineHeight: 1.3,
          fontSize: 12,
        },

        {
          text: `_________________________________\n${this.utilService.firstLetterUppercase(
            transferencia.doador.nome
          )}\nALIENANTE`.toUpperCase(),
          margin: 15,
          alignment: 'center',
          lineHeight: 1.3,
          fontSize: 12,
        },
        linhaAdquirente,
      ],
      styles: {
        tableExample: {
          fontSize: 11,
          margin: 5,
        },
        header: {
          fontSize: 14,
          bold: true,
          lineHeight: 2.0,
          alignment: 'center',
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Termo de transferência');
  }

  async requerimentoPF(
    transferencia: Transferencia,
    nomeEmpresa: string,
    municipioTrabalho: string,
    enderecoTrabalho: string,
    bairroTrabalho: string,
    ufTrabalho: string,
    cepTrabalho: string,
    telefoneTrabalho: string,
    cnpjTrabalho: string
  ) {
    let uf = /* prompt("Digite a UF do recebedor") ??  */ '';
    let cliente: Cliente;
    console.log(transferencia);
    let tipoRequerimento: string;
    let categoria: string;
    let porte: string;
    let celulaNumSinarmSigma;
    transferencia.doador = await this.fireService.getClienteById(
      transferencia.doador.id
    );
    transferencia.recebedor = await this.fireService.getClienteById(
      transferencia.recebedor.id
    );

    transferencia.arma = await this.fireService.getArmaById(
      transferencia.arma.id
    );
    console.log(transferencia.arma);
    if (transferencia.arma.isSigma) {
      celulaNumSinarmSigma = {
        text: 'Cadastro Sigma:' + transferencia.arma.sigma,
        fontSize: 12,
        style: 'semTopoBorder',
        border: [true, true, true, true],
        margin: [0, 2],
        alignment: 'left',
      };
    } else
      celulaNumSinarmSigma = {
        text: 'Cadastro Sinarm:' + transferencia.arma.sinarmFormatado,
        fontSize: 12,
        style: 'semTopoBorder',
        border: [true, true, true, true],
        margin: [0, 2],
        alignment: 'left',
      };
    if (transferencia.modalidade == 'sigmaXsinarm') {
      tipoRequerimento = 'TRANSFERÊNCIA DO SIGMA PARA O SINARM';
      cliente = transferencia.recebedor;
    } else {
      tipoRequerimento = 'TRANSFERÊNCIA DO SINARM PARA O SIGMA';
      cliente = transferencia.doador;
    }

    if (cliente.modalidade == 'cidadao' || cliente.modalidade == 'cac') {
      categoria = 'Cidadão';
    } else categoria = 'Servidor Público – Porte por prerrogativa de função ';
    if (cliente.modalidade == 'cac') porte = 'Atirador deportivo';
    else porte = 'Defesa Pessoal';
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'ANEXO I\nREQUERIMENTO PADRÃO',
          style: 'header',
          margin: [10, 0, 10, 15],
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*', 100],
            body: [
              [
                {
                  image: await this.utilService.assetToBase64(
                    'assets/imgs/logo-federal.png'
                  ),
                  alignment: 'center',
                  height: 70,
                  width: 70,
                  border: [true, true, true, false],
                },
                cliente.foto //teste
                  ? {
                      image: await this.utilService.assetToBase64(cliente.foto),
                      alignment: 'center',
                      margin: [0, 5],
                      border: [false, true, true, false],
                      height: 120,
                      width: 92,
                      rowSpan: 2,
                    }
                  : {
                      text: 'FOTO 3X4',
                      alignment: 'center',
                      margin: [0, 20],
                      border: [false, true, true, false],
                    },
              ],
              [
                {
                  text: 'SERVIÇO PÚBLICO FEDERAL\nMINISTÉRIO DA JUSTIÇA E SEGURANÇA PÚBLICA\nPOLÍCIA FEDERAL',
                  border: [true, false, true, false],
                  margin: [0, 5, 0, 0],
                  style: 'tableHeader',
                  alignment: 'center',
                  bold: true,
                },
                { text: '', border: [true, false, true, false] },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*'],
            body: [
              [
                {
                  text: '1 – TIPO DE REQUERIMENTO',
                  bold: true,
                  fontSize: 13,
                  style: 'semTopoBorder',
                  margin: [0, 10],
                  alignment: 'left',
                },
              ],
              [
                {
                  text: tipoRequerimento,
                  fontSize: 12,
                  bold: false,
                  style: 'semTopoBorder',
                  margin: [0, 10],
                  lineHeight: 1.2,
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*'],
            body: [
              [
                {
                  text: '2 – DADOS DA PESSOA FÍSICA OU JURÍDICA',
                  bold: true,
                  fontSize: 13,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 10],
                  colSpan: 2,
                  alignment: 'left',
                },
                {},
              ],
              [
                {
                  text:
                    'Nome Completo:\n' +
                    this.utilService.firstLetterUppercase(cliente.nome),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'CPF:\n' + cliente.cpfFormatado,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: [/* 'auto', 'auto',  */ '*'],
            body: [
              [
                {
                  text: 'Categoria: ' + categoria,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                /* { text: '1-Cidadão (   )\n2-Caçador de Subsistência (   )\n3-Servidor Público – Porte por prerrogativa de função (   )\n4-Outras Categorias ', fontSize: 10, style: 'semTopoBorder', border: [true, false, true, true], margin: [0, 2], alignment: 'left', },
            { text: '5-Órgão Público com taxa (   )\n6-Órgão Público sem taxa (   )\n7-Empresa de Segurança Privada (   )\n8-Empresa com Segurança Orgânica (  )\n9-Empresa Comercial (   )\n10-Fabricante/Importador (   )\n11-Revendedor (   )', fontSize: 10, style: 'semTopoBorder', border: [true, false, true, true], margin: [0, 2], alignment: 'left', },*/
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*'],
            body: [
              [
                {
                  text:
                    'Nome do Pai:\n' +
                    this.utilService.firstLetterUppercase(cliente.nomePai),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Nome da Mãe:\n' +
                    this.utilService.firstLetterUppercase(cliente.nomeMae),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*', '*', '*', 50],
            body: [
              [
                {
                  text:
                    'Data de nascimento:\n' +
                    this.utilService.formatarData(cliente.dataNascimento),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Sexo:\n' + cliente.sexo,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'País de Nascimento: Brasil',
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: `Município de Nasc.\n
                    ${this.utilService.firstLetterUppercase(
                      cliente.naturalidade
                    )}/${cliente.uf.toUpperCase()}`,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'UF:\n' + cliente.uf,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*'],
            body: [
              //[{ lineHeight: 1.5, text: 'Estado Civil:  1-Solteiro (   )      2-Casado (   )      3-Viúvo (   )            4-Separado Jud. (   )              5-Divorciado (   )\n                    6-União Estável (   )    7-União Homoafetiva (   )       8-Outros (   )   ', fontSize: 10, style: 'semTopoBorder', border: [true, false, true, true], margin: [0, 2], alignment: 'left', colsSpan: 2 }],
              [
                {
                  lineHeight: 1.5,
                  text: 'Estado Civil: ' + cliente.estadoCivil,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                  colsSpan: 2,
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*', '*', 40, 120],
            body: [
              [
                {
                  text: 'Identidade:\n' + cliente.identidade,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Emissão:\n' +this.utilService.formatarData( cliente.expedicao),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Órgão Expedidor:\n' + cliente.orgaoIdentidade,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'UF:\n' + cliente.ufIdentidade?.toUpperCase(),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Título de Eleitor:',
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', 100],
            body: [
              [
                {
                  text:
                    'Endereço Residencial:\n' +
                    `${cliente.endereco1.logradouro}, ${cliente.endereco1.numero} ${cliente.endereco1.complemento}`,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Distrito/Bairro:\n' + cliente.endereco1.bairro,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: [180, 50, '*', '*'],
            body: [
              [
                {
                  text: 'Município: ' + cliente.endereco1.cidade,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'UF: ' + cliente.endereco1.estado,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'CEP: ' + cliente.endereco1.cep,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Telefone Fixo: ',
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: [180, 180, '*'],
            body: [
              [
                {
                  text: 'Cargo/Ocupação\n' + cliente.profissaoFormatada,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'E-mail\n' + cliente.email,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Telefone Celular\n' + cliente.telefoneFormatado,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            dontBreakRows: true,
            heights: [30],
            widths: [360, '*'],
            body: [
              [
                {
                  text: 'Empresa/Órgão de Trabalho\n' + nomeEmpresa,
                  fontSize: 11,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'CNPJ\n' + cnpjTrabalho,
                  fontSize: 11,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          margin: [0, 0, 0, 0],
          style: 'tableExample',
          table: {
            heights: [30],
            widths: [360, '*'],
            body: [
              [
                {
                  text: 'Endereço Comercial:\n' + enderecoTrabalho,
                  fontSize: 12,
                  border: [true, true, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Distrito/Bairro\n' + bairroTrabalho,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, true, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            dontBreakRows: true,
            heights: [30],
            widths: [180, 50, '*', '*'],
            body: [
              [
                {
                  text: 'Município\n' + municipioTrabalho,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'UF\n' + ufTrabalho,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'CEP\n' + cepTrabalho,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Telefone Comercial\n' + telefoneTrabalho,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            dontBreakRows: true,
            heights: [30],
            widths: ['*'],
            body: [
              [
                {
                  text: '3 – DADOS DA ARMA',
                  bold: true,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  margin: [0, 10],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            dontBreakRows: true,
            heights: [30],
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Número da Arma:\n' + transferencia.arma.numero,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Registro Federal:\n' + transferencia.arma.registroFederal,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Órgão Expedidor:\n ' + transferencia.arma.expedidor,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'UF: \n' + transferencia.arma.uf,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Data de Emissão:\n' +
                    this.utilService.formatarData(
                      transferencia.arma.dataEmissao
                    ),
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            dontBreakRows: true,
            heights: [30],
            widths: ['*', '*', '*'],
            body: [
              [
                celulaNumSinarmSigma,
                {
                  text: 'Espécie: ' + transferencia.arma.modelo.especie.nome,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, true, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Marca: ' + transferencia.arma.modelo.fabricante.nome,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, true, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Modelo: ' + transferencia.arma.modelo.nome,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Calibre: ' + transferencia.arma.calibre.nome,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'País de fabricação: ' + transferencia.arma.pais,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Capacidade de Tiros: ' +
                      transferencia.arma.modelo.capacidadeTiro ?? '',
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Número de canos: ' + transferencia.arma.modelo.numeroCanos,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Alma: ' + transferencia.arma.modelo.tipoAlma,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Nº de raias: ' + transferencia.arma.modelo.quantidadeRaias,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text: 'Sentido: ' + transferencia.arma.modelo.sentidoRaias,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Compr. do Cano (mm): ' +
                    transferencia.arma.comprimentoCano,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'Acabamento: ' + transferencia.arma.acabamento,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                {
                  text:
                    'Funcionamento: ' +
                    transferencia.arma.modelo.funcionamentoFormatado,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: [/* 'auto', 'auto',  */ '*'],
            body: [
              [
                {
                  text: 'Porte: ' + porte,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  border: [true, false, true, true],
                  margin: [0, 2],
                  alignment: 'left',
                },
                /* { text: '1-Defesa Pessoal (   )\n2-Segurança de Dignitários (   )', fontSize: 10, style: 'semTopoBorder', border: [false, false, false, true], margin: [0, 2, 20, 0], alignment: 'left', },
                { text: '2-Funcional (   )\n4-Caçador de Subsistência (   ) ', fontSize: 10, style: 'semTopoBorder', border: [false, false, true, true], margin: [0, 2], alignment: 'left', } */
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            heights: [30],
            widths: ['*'],
            body: [
              [
                {
                  text: '4 - TERMO DE RESPONSABILIDADE ',
                  bold: true,
                  fontSize: 12,
                  style: 'semTopoBorder',
                  margin: [0, 10],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            //heights: [30],

            widths: ['*'],
            body: [
              [
                {
                  text: '( X ) Declaro que não estou respondendo a inquérito policial ou a processo criminal.',
                  margin: [0, 10, 0, 0],
                  border: [true, true, true, false],
                  bold: true,
                  fontSize: 10,
                  style: 'semTopoBorder',
                  alignment: 'left',
                },
              ],
              [
                {
                  text: '( X ) Declaro serem verdadeiras as informações consignadas neste formulário.',
                  border: [true, false, true, false],
                  bold: true,
                  fontSize: 10,
                  style: 'semTopoBorder',
                  margin: [0, 0],
                  alignment: 'left',
                },
              ],
            ],
          },
        },
        /* { text: '(   ) Declaro que não estou respondendo a inquérito policial ou a processo criminal.', bold: true, fontSize: 12, style: 'semTopoBorder', margin: [0, 10], alignment: 'left', },
       { text: '(   ) Declaro serem verdadeiras as informações consignadas neste formulário.', bold: true, fontSize: 12, style: 'semTopoBorder', margin: [0, 10], alignment: 'left', }, */
        {
          style: 'tableExample',
          table: {
            //heights: [25],
            widths: ['*'],
            body: [
              [
                {
                  text: 'Maceió, ' + this.utilService.dataAtualPorExtenso(),
                  bold: false,
                  fontSize: 12,
                  border: [true, false, true, false],
                  margin: [0, 50, 0, 10],
                  alignment: 'center',
                },
              ],
            ],
          },
        },
        {
          style: 'tableExample',
          table: {
            //heights: [25],
            widths: ['*'],
            body: [
              [
                {
                  text: `___________________________________\n${cliente.nome}\nREQUERENTE`,
                  fontSize: 12,
                  border: [true, false, true, true],
                  lineHeight: 1.3,
                  margin: [0, 40, 0, 40],
                  alignment: 'center',
                },
              ],
            ],
          },
        },
        //{ text: '_____________/_________, _______de _______________ de _________.', bold: false, fontSize: 12, style: 'semTopoBorder', margin: [50, 10], alignment: 'center', },
        /* fromltrb
        {
          style: 'tableExample',
          table: {

            heights: [25],
            widths: ['*','*'],
            body: [
              [{text: 'Acabamento:  1-Oxidado (   )      3-Aço Inox (   )\n2-Niquelado (   )            4-Outros (   )', fontSize: 10, style: 'semTopoBorder', border: [true, false, true, true], margin: [0, 2], alignment: 'left', },
              {text: 'Funcionamento:  1-Repetição (   )      3-Automático (   )\n2-Semiautomático (   )            4-Outros (   )', fontSize: 10, style: 'semTopoBorder', border: [true, false, true, true], margin: [0, 2], alignment: 'left', }
              ],
            ]
          }
        }, */
      ],

      styles: {
        tableExample: {
          fontSize: 12,
        },
        header: {
          fontSize: 12,
          bold: false,
          lineHeight: 1.5,
          alignment: 'center',
        },
      },
    };
    pdfmake.createPdf(pdf).download('Requerimento padrão');
  }

  async declaracaoLocalSeguro(cliente: Cliente) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE EXISTÊNCIA DE LOCAL SEGURO PARA GUARDA DE ARMA DE FOGO',
          style: 'header',
          alignment: 'center',
          margin: [0, 0, 0, 100],
        },
        {
          text: `Eu, ${cliente.nome}, portador da cédula de identidade nº ${cliente.identidadeFormatada}, CPF Nº ${cliente.cpfFormatado}, ${cliente.profissaoFormatada}, ${cliente.filiacao}, residente no(a) ${cliente.endereco1Formatado}, declaro que possuo lugar seguro com tranca para armazenamento das armas de fogo das quais sou/serei proprietário, bem como adoto todas as medidas necessárias para impedir que menor de dezoito anos de idade ou pessoa com deficiência mental se apodere de arma de fogo que esteja sob minha posse ou que seja de minha propriedade nos termos do disposto no art. 13 da Lei n° 10.826, de 2003.`,
          alignment: 'justify',
          lineHeight: 1.5,
        },

        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
          margin: [0, 0, 0, 10],
        },
        {
          text: `_________________________________\n${cliente.nome}\nDECLARANTE`,
          margin: 25,
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
          margin: 5,
        },
        header: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 15],
          lineHeight: 1.3,
          alignment: 'center',
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Declaracao Local Seguro');
  }

  async declaracaoProcesso(cliente: Cliente) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE NÃO ESTAR RESPONDENDO A INQUÉRITO POLICIAL OU A PROCESSO CRIMINAL',
          style: 'header',
          alignment: 'center',
          margin: [0, 0, 0, 100],
        },
        {
          text: `Eu, ${cliente.nome}, portador da cédula de identidade nº ${cliente.identidadeFormatada}, CPF Nº ${cliente.cpfFormatado}, ${cliente.profissaoFormatada}, ${cliente.filiacao}, residente no(a) ${cliente.endereco1Formatado}, declaro sob as penas da lei, que não respondo processo criminal e/ou inquérito policial, tanto no Estado de domicílio quanto nos demais entes federativos, e estou ciente de que, em caso de falsidade ideológica, ficarei sujeito às sanções prescritas no Código Penal e às demais cominações legais aplicáveis.`,
          alignment: 'justify',
          lineHeight: 1.5,
          margin: [0, 0, 0, 10],
        },

        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },
        {
          text: `_________________________________\n${cliente.nome}\nDECLARANTE`,
          margin: 25,
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
          margin: 5,
        },
        header: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 15],
          lineHeight: 1.3,
          alignment: 'center',
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Declaracao processos');
  }

  async declaracaoEfetivaNecessidade(cliente: Cliente) {
    let motivacao;
    switch (cliente.modalidade) {
      case 'pc':
        motivacao =
          'em razão do exercício profissional como Policial Civil, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      case 'gm':
        motivacao =
          'em razão do exercício profissional como Guarda Municipal, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      case 'pf':
        motivacao =
          'em razão do exercício profissional como Policial Federal, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      case 'prf':
        motivacao =
          'em razão do exercício profissional como Policial Rodoviário Federal, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      default:
        motivacao =
          'para defesa da minha vida, de meus familiares e de meu patrimônio em eventual tentativa de roubo ou invasão criminosa à minha residência.';
        break;
    }
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE EFETIVA NECESSIDADE',
          style: 'header',
          alignment: 'center',
          margin: [0, 0, 0, 100],
        },
        {
          text: `Eu, ${cliente.nome}, portador da cédula de identidade nº ${cliente.identidadeFormatada}, CPF Nº ${cliente.cpfFormatado}, ${cliente.profissaoFormatada}, ${cliente.filiacao}, residente no(a) ${cliente.endereco1Formatado}, declaro nos termos do Art. 3º, § 1º do DECRETO Nº 9.845, DE 25 DE JUNHO DE 2019 a efetiva necessidade em possuir armas de fogo ${motivacao}`,
          alignment: 'justify',
          lineHeight: 1.5,
          margin: [0, 0, 0, 20],
        },

        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          margin: [0, 0, 0, 20],
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },
        {
          text: `_________________________________\n${cliente.nome}\nDECLARANTE`,
          margin: 25,
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
          margin: 5,
        },
        header: {
          fontSize: 14,
          margin: [0, 0, 0, 15],
          bold: true,
          lineHeight: 1.3,
          alignment: 'center',
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Declaracao Efetiva Necessidade');
  }

  async declaracoesConjuntas(cliente: Cliente) {
    let motivacao;
    switch (cliente.modalidade) {
      case 'pc':
        motivacao =
          'em razão do exercício profissional como Policial Civil, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      case 'gm':
        motivacao =
          'em razão do exercício profissional como Guarda Municipal, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      case 'pf':
        motivacao =
          'em razão do exercício profissional como Policial Federal, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      case 'prf':
        motivacao =
          'em razão do exercício profissional como Policial Rodoviário Federal, sendo a arma de fogo imprescindível para à preservação da minha integridade física e familiar devido aos riscos inerentes à minha profissão.';
        break;
      default:
        motivacao =
          'para defesa da minha vida, de meus familiares e de meu patrimônio em eventual tentativa de roubo ou invasão criminosa à minha residência.';
        break;
    }

    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE EFETIVA NECESSIDADE',
          style: 'header',
          alignment: 'center',
          margin: [0, 0, 0, 20],
        },
        {
          text: `Eu, ${cliente.nome}, portador da cédula de identidade nº ${cliente.identidadeFormatada}, CPF Nº ${cliente.cpfFormatado}, ${cliente.profissaoFormatada}, ${cliente.filiacao}, residente no(a) ${cliente.endereco1Formatado}, declaro nos termos do Art. 3º, § 1º do DECRETO Nº 9.845, DE 25 DE JUNHO DE 2019 a efetiva necessidade em possuir armas de fogo ${motivacao}`,
          alignment: 'justify',
          lineHeight: 1.5,
          margin: [0, 0, 0, 20],
        },

        {
          text: 'DECLARAÇÃO DE EXISTÊNCIA DE LOCAL SEGURO PARA GUARDA DE ARMA DE FOGO',
          style: 'header',
          alignment: 'center',
          margin: [0, 0, 0, 20],
        },
        {
          text: `Eu, ${cliente.nome}, portador da cédula de identidade nº ${cliente.identidadeFormatada}, CPF Nº ${cliente.cpfFormatado}, ${cliente.profissaoFormatada}, ${cliente.filiacao}, residente no(a) ${cliente.endereco1Formatado}, declaro que possuo lugar seguro com tranca para armazenamento das armas de fogo das quais sou/serei proprietário, bem como adoto todas as medidas necessárias para impedir que menor de dezoito anos de idade ou pessoa com deficiência mental se apodere de arma de fogo que esteja sob minha posse ou que seja de minha propriedade nos termos do disposto no art. 13 da Lei n° 10.826, de 2003.`,
          alignment: 'justify',
          lineHeight: 1.5,
        },
        {
          text: 'DECLARAÇÃO DE NÃO ESTAR RESPONDENDO A INQUÉRITO POLICIAL OU A PROCESSO CRIMINAL',
          style: 'header',
          alignment: 'center',
          margin: [0, 20, 0, 20],
        },
        {
          text: `Eu, ${cliente.nome}, portador da cédula de identidade nº ${cliente.identidadeFormatada}, CPF Nº ${cliente.cpfFormatado}, ${cliente.profissaoFormatada}, ${cliente.filiacao}, residente no(a) ${cliente.endereco1Formatado}, declaro sob as penas da lei, que não respondo processo criminal e/ou inquérito policial, tanto no Estado de domicílio quanto nos demais entes federativos, e estou ciente de que, em caso de falsidade ideológica, ficarei sujeito às sanções prescritas no Código Penal e às demais cominações legais aplicáveis.`,
          alignment: 'justify',
          lineHeight: 1.5,
          margin: [0, 0, 0, 10],
        },
        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          margin: [0, 30, 0, 20],
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },

        {
          text: `_________________________________\n${cliente.nome}\nDECLARANTE`,
          margin: 25,
          alignment: 'center',
          lineHeight: 1.5,
          fontSize: 12,
        },
      ],
      styles: {
        tableExample: {
          fontSize: 11,
          margin: 5,
        },
        header: {
          fontSize: 14,
          margin: [0, 0, 0, 15],
          bold: true,
          lineHeight: 1.3,
          alignment: 'center',
        },
        cabecalho: {
          fontSize: 12,
          bold: false,
        },
        subheader: {
          fontSize: 15,
          bold: true,
        },
        quote: {
          italics: true,
        },
        small: {
          fontSize: 8,
        },
      },
    };
    pdfmake.createPdf(pdf).download('Declaracao Efetiva Necessidade');
  }
}
