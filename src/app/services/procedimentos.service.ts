import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  DocumentReference,
  DocumentSnapshot,
  QuerySnapshot,
} from '@angular/fire/firestore';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { Calibre } from '../model/calibre.model';
import { Cliente } from '../model/cliente.model';
import { Fornecedor } from '../model/fornecedor.model';
import { Modelo } from '../model/modelo.model';
import { FireService } from './fire.service';
import { UtilService } from './util.service';
import * as firebase from 'firebase';
import { Aquisicao } from '../model/aquisicao.model';
import * as aquisicaoFactory from '../model/aquisicao.model';
import { first } from 'rxjs/operators';
import { Pendencia } from '../model/pendencia.model';
import { Arquivo } from '../model/arquivo.model';
import { Arma } from '../model/arma.model';
import { Transferencia } from '../model/transferencia.model';
import * as transferenciaFactory from '../model/transferencia.model';

@Injectable({
  providedIn: 'root',
})
export class ProcedimentosService {
  constructor(
    private firestore: AngularFirestore,
    public auth: AngularFireAuth,
    public storage: AngularFireStorage,
    private utilService: UtilService,
    private fireService: FireService
  ) {}

  /**AQUISIÇÃO */
  async cadastrarAquisicao(
    modelo: Modelo,
    cliente: Cliente,
    fornecedor: Fornecedor,
    calibre: Calibre,
    acabamento: string,
    formaPagamento: string,
    email: string,
    observacao: string,
    local: string,
    modalidade: string
  ): Promise<Aquisicao> {
    let ref: DocumentReference<unknown> = await this.firestore
      .collection('aquisicoes')
      .add({
        modelo: modelo.asMap,
        modeloId: modelo.id,
        modeloNome: modelo.nome,
        cliente: cliente.asMap,
        clienteId: cliente.id,
        clienteNome: cliente.nome,
        fornecedor: fornecedor.asMap,
        fornecedorNome: fornecedor.nome,
        fornecedorId: fornecedor.id,
        calibre: calibre.asMap,
        calibreNome: calibre.nome,
        calibreId: calibre.id,
        acabamento: acabamento,
        email: email,
        observacao: observacao,
        local: local,
        formaPagamento: formaPagamento,
        pendenciasResolvidas: [],
        excluido: false,
        modalidade: modalidade,
        timestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
      });
    await this.firestore
      .collection('clientes')
      .doc(cliente.id)
      .collection('procedimentos')
      .add({
        tipo: 'aquisicao',
        procedimentoId: ref.id,
        modelo: modelo.asMap,
        modeloId: modelo.id,
        modeloNome: modelo.nome,
        fornecedor: fornecedor.asMap,
        fornecedorNome: fornecedor.nome,
        fornecedorId: fornecedor.id,
      });
    let documentSnapshot: firebase.default.firestore.DocumentSnapshot<unknown> =
      await ref.get();
    return aquisicaoFactory.fromFirestore(documentSnapshot);
  }

  async getAquisicoes(): Promise<Aquisicao[]> {
    let querySnapshot: QuerySnapshot<any> = await this.firestore
      .collection('aquisicoes', (ref) =>
        ref.where('excluido', '==', false).orderBy('timestamp', 'desc')
      )
      .get()
      .pipe(first())
      .toPromise();
    let promises: Promise<Aquisicao>[];
    promises = querySnapshot.docs.map((s) => aquisicaoFactory.fromFirestore(s));
    return Promise.all(promises);
  }

  async getAquisicaoById(id: string): Promise<Aquisicao> {
    return aquisicaoFactory.fromFirestore(
      await this.firestore
        .collection('aquisicoes')
        .doc(id)
        .get()
        .pipe(first())
        .toPromise()
    );
  }

  async getAquisicaoTeste(): Promise<Aquisicao> {
    let aquisicoes = await this.getAquisicoes();
    return aquisicoes[1];
  }

  async addPendenciaResolvida(
    aquisicao: Aquisicao,
    pendencia: Pendencia
  ): Promise<Aquisicao> {
    pendencia = new Pendencia(pendencia.descricao, true, new Date());
    aquisicao.pendenciasResolvidas.push(pendencia);
    await this.firestore
      .collection('aquisicoes')
      .doc(aquisicao.id)
      .update({
        ultimaPendencia: pendencia.toMap,
        ultimaAtividade: pendencia.data,
        pendenciasResolvidas: aquisicao.pendenciasResolvidas.map((p) => {
          return p.toMap;
        }),
      });
    return this.getAquisicaoById(aquisicao.id);
  }

  async removerPendenciaResolvida(
    aquisicao: Aquisicao,
    pendencia: Pendencia
  ): Promise<Aquisicao> {
    let pendencias = aquisicao.pendenciasResolvidas.filter((p) => {
      return pendencia.descricao != p.descricao;
    });
    let ultimaPendencia =
      aquisicao.pendenciasResolvidas[aquisicao.pendenciasResolvidas.length - 1];
    await this.firestore
      .collection('aquisicoes')
      .doc(aquisicao.id)
      .update({
        ultimaPendencia: ultimaPendencia.toMap,
        ultimaAtividade: ultimaPendencia.data,
        pendenciasResolvidas: pendencias.map((p) => {
          return p.toMap;
        }),
      });
    return this.getAquisicaoById(aquisicao.id);
  }

  async cadastrarArquivoAquisicao(
    descricao: string,
    arquivo: File,
    aquisicao: Aquisicao
  ): Promise<Aquisicao> {
    let task: AngularFireUploadTask = this.storage.upload(
      `aquisicoes/${aquisicao.id}/${arquivo.name}`,
      arquivo
    );
    let url: string = await (await task).ref.getDownloadURL();
    let path: string = (await task).ref.fullPath;

    await this.firestore
      .collection('aquisicoes')
      .doc(aquisicao.id)
      .collection('arquivos')
      .add({
        url: url,
        path: path,
        timestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
        descricao: descricao,
      });
    return this.getAquisicaoById(aquisicao.id);
  }

  async excluirArquivoAquisicao(
    aquisicao: Aquisicao,
    arquivo: Arquivo
  ): Promise<Aquisicao> {
    await this.storage.ref(arquivo.path).delete().pipe(first()).toPromise();
    await this.firestore
      .collection('aquisicoes')
      .doc(aquisicao.id)
      .collection('arquivos')
      .doc(arquivo.id)
      .delete();
    return this.getAquisicaoById(aquisicao.id);
  }

  async excluirAquisicao(aquisicao: Aquisicao): Promise<any> {
    let promises: Promise<any>[];
    promises = aquisicao.arquivos.map((a) => {
      return this.excluirArquivoAquisicao(aquisicao, a);
    });
    promises.push(
      this.firestore
        .collection('aquisicoes')
        .doc(aquisicao.id)
        .update({ excluido: true })
    );
    return Promise.all(promises);
  }

  get pendencias(): Pendencia[] {
    return [
      new Pendencia('PAGAMENTO SERVIÇO DESPACHANTE'),
      new Pendencia('CONFIRMAR SITUAÇÃO NO SIGMA'),
      new Pendencia('FUNCIONAL - RGPM'),
      new Pendencia('COMPROVANTE DE RESIDÊNCIA'),
      new Pendencia('TESTE PSICOLÓGICO (PARA REFORMADOS + DE 10 anos)'),
      new Pendencia('PAGAMENTO DE GRU'),
      new Pendencia('REQUERIMENTO AQUISIÇÃO - ANEXO C'),
      new Pendencia('PROTOCOLO NA 2ª SEÇÃO '),
      new Pendencia('AUTORIZAÇÃO PARA AQUISIÇÃO - PMAL'),
      new Pendencia('PAGAMENTO DA ARMA'),
      new Pendencia('ENVIO DE AUTORIZAÇÃO À TAURUS'),
      new Pendencia('AGUARDAR NOTA FISCAL'),
      new Pendencia('ENCAMINHAR NOTA FISCAL À 2ª SEÇÃO'),
      new Pendencia('ENVIAR CRAF AO FORNECEDOR'),
      new Pendencia('ENTREGA DO CRAF AO CLIENTE'),
      new Pendencia('CONFIRMAR RECEBIMENTO DA ARMA'),
    ];
  }

  /**FIM AQUISIÇÃO */

  /**TRANSFERÊNCIA */

  async cadastrarTransferencia(
    doador: Cliente,
    recebedor: Cliente,
    arma: Arma,
    modalidade: string,
    observacoes: string
  ): Promise<Transferencia> {
    console.log(doador);
    let ref: DocumentReference<unknown> = await this.firestore
      .collection('transferencias')
      .add({
        doador: doador.asMap,
        doadorId: doador.id,
        recebedorId: recebedor.id,
        recebedor: recebedor.asMap,
        arma: arma.asMap,
        modalidade: modalidade,
        observacoes: observacoes,
        timestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
      });
    return transferenciaFactory.fromFirestore(await ref.get());
  }

  async getTransferenciaByCliente(cliente: Cliente): Promise<Transferencia[]> {
    let querySnapshot: QuerySnapshot<any> = await this.firestore
      .collection('transferencias', (ref) =>
        ref.where('doadorId', '==', cliente.id)
      )
      .get()
      .pipe(first())
      .toPromise();
    return querySnapshot.docs.map((s) => transferenciaFactory.fromFirestore(s));
  }

  async getTransferencias(): Promise<Transferencia[]> {
    let querySnapshot: QuerySnapshot<any> = await this.firestore
      .collection('transferencias', (ref) => ref.orderBy('timestamp', 'desc'))
      .get()
      .pipe(first())
      .toPromise();
    return querySnapshot.docs.map((s) => transferenciaFactory.fromFirestore(s));
  }
  /**FIM TRANSFERÊNCIA */
}
