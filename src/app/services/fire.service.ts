import { PreCadastro } from './../model/preCadastro.model';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  DocumentSnapshot,
  QueryDocumentSnapshot,
  QuerySnapshot,
} from '@angular/fire/firestore';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import * as firebase from 'firebase';
import { map, first } from 'rxjs/operators';

import { Fabricante } from '../model/fabricante.model';
import { Especie } from '../model/especie.model';
import { Calibre } from '../model/calibre.model';

import * as fabricanteFactory from '../model/fabricante.model';
import * as fornecedorFactory from '../model/fornecedor.model';
import * as especieFactory from '../model/especie.model';
import * as calibreFactory from '../model/calibre.model';
import * as modeloFactory from '../model/modelo.model';
import * as armaFactory from '../model/arma.model';
import * as clienteFactory from '../model/cliente.model';

import { FormGroup } from '@angular/forms';
import { Modelo } from '../model/modelo.model';
import { UtilService } from './util.service';
import { Arma } from '../model/arma.model';
import { Cliente } from '../model/cliente.model';
import { Endereco } from '../model/endereco.model';
import { Arquivo } from '../model/arquivo.model';
import { Fornecedor } from '../model/fornecedor.model';
import { UploadTaskSnapshot } from '@angular/fire/storage/interfaces';
import { unwatchFile } from 'fs';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FireService {
  fabricantes: Fabricante[] = [];
  especies: Especie[] = [];
  calibres: Calibre[] = [];
  modelos: Modelo[] = [];
  armas: Arma[] = [];
  fornecedores: Fornecedor[] = [];

  constructor(
    private firestore: AngularFirestore,
    public auth: AngularFireAuth,
    public storage: AngularFireStorage,
    private utilService: UtilService
  ) {
    this.initFabricantes();
    this.initEspecies();
    this.initCalibres();
    //this.initModelos();
  }

  initFabricantes() {
    this.firestore
      .collection('fabricantes')
      .snapshotChanges()
      .subscribe((snapshot) => {
        this.fabricantes = snapshot.map((s) =>
          fabricanteFactory.fromSnapshotFirestore(s)
        );
        console.log('Fabricantes: ', this.fabricantes);
      });
  }

  initEspecies() {
    this.firestore
      .collection('especies')
      .snapshotChanges()
      .subscribe((snapshot) => {
        this.especies = snapshot.map((s) =>
          especieFactory.fromSnapshotFirestore(s)
        );
        console.log('Fabricantes: ', this.fabricantes);
      });
  }

  initCalibres() {
    this.firestore
      .collection('calibres')
      .snapshotChanges()
      .subscribe((snapshot) => {
        this.calibres = snapshot.map((s) =>
          calibreFactory.fromSnapshotFirestore(s)
        );
        console.log('Calibres: ', this.calibres);
      });
  }

  /*  initModelos() {
    this.firestore
      .collection('modelos')
      .snapshotChanges()
      .subscribe((snapshot) => {
        this.modelos = snapshot.map((s) => {
          let fabricante: Fabricante;
          let especie: Especie;
          return modeloFactory.fromSnapshotFirestore(s);
        });
        console.log('Fabricantes: ', this.fabricantes);
      });
  } */

  async getAniversariantes(): Promise<Cliente[]> {
    let hoje: Date = new Date();
    let snapshot: QuerySnapshot<unknown> = await this.firestore.collection('clientes', ref => ref.where('aniversario', '==', `${hoje.getDate()}-${hoje.getMonth()}`)).get().pipe(first())
      .toPromise();
    return Promise.all(snapshot.docs.map((s) => clienteFactory.fromFirestore(s)));
  }

  /** FABRICANTES */

  async cadastrarFabricante(nome: string, cnpj: string): Promise<Fabricante[]> {
    if (await this.getFabricantePorCNPJ(cnpj)) {
      throw 'cnpj-ja-cadastrado';
    }

    await this.firestore
      .collection('fabricantes')
      .add({ nome: nome, cnpj: cnpj });
    return this.getFabricantes();
  }

  async getFabricantePorCNPJ(cnpj: string): Promise<Fabricante> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('fabricantes', (ref) => ref.where('cnpj', '==', cnpj))
      .get()
      .pipe(first())
      .toPromise();
    if (snapshot.docs.length <= 0) return null;
    return fabricanteFactory.fromFirestore(snapshot.docs[0]);
  }

  async getFabricantes(): Promise<Fabricante[]> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('fabricantes', (ref) => ref.orderBy('nome'))
      .get()
      .pipe(first())
      .toPromise();
    this.fabricantes = snapshot.docs.map((s) =>
      fabricanteFactory.fromFirestore(s)
    );
    return this.fabricantes;
  }

  async getFabricanteById(id: string): Promise<Fabricante> {
    let fabricante: Fabricante;
    console.log('getFabricanteById: ', id);
    this.fabricantes.forEach((e) => {
      if (e.id.includes(id)) fabricante = e;
    });
    if (!fabricante) {
      let snapshot: firebase.default.firestore.DocumentSnapshot<unknown> =
        await this.firestore
          .collection('fabricantes')
          .doc(id)
          .get()
          .pipe(first())
          .toPromise();
      fabricante = fabricanteFactory.fromFirestore(snapshot);
    }
    return fabricante;
  }

  async deleteFabricante(fabricante: Fabricante): Promise<Fabricante[]> {
    await this.firestore.doc(`fabricantes/${fabricante.id}`).delete();
    return this.getFabricantes();
  }

  /**FORNECEDOR */

  async cadastrarFornecedor(
    nome: string,
    cnpj: string,
    cr: string,
    endereco: string,
    cep: string,
    telefone: string
  ): Promise<Fornecedor[]> {
    if (await this.getFornecedorPorCNPJ(cnpj)) {
      throw 'cnpj-ja-cadastrado';
    }

    await this.firestore.collection('fornecedores').add({
      nome: nome,
      cnpj: cnpj,
      cr: cr,
      endereco: endereco,
      cep: cep,
      telefone: telefone,
    });
    return this.getFornecedores();
  }

  async getFornecedorPorCNPJ(cnpj: string): Promise<Fornecedor> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('fornecedores', (ref) => ref.where('cnpj', '==', cnpj))
      .get()
      .pipe(first())
      .toPromise();
    if (snapshot.docs.length <= 0) return null;
    return fornecedorFactory.fromFirestore(snapshot.docs[0]);
  }

  async getFornecedores(): Promise<Fornecedor[]> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('fornecedores', (ref) => ref.orderBy('nome'))
      .get()
      .pipe(first())
      .toPromise();
    this.fornecedores = snapshot.docs.map((s) =>
      fornecedorFactory.fromFirestore(s)
    );
    return this.fornecedores;
  }

  async getFornecedorById(id: string): Promise<Fornecedor> {
    let fornecedor: Fornecedor;
    this.fornecedores.forEach((e) => {
      if (e.id.includes(id)) fornecedor = e;
    });
    if (!fornecedor) {
      let snapshot: firebase.default.firestore.DocumentSnapshot<unknown> =
        await this.firestore
          .collection('fornecedores')
          .doc(id)
          .get()
          .pipe(first())
          .toPromise();
      fornecedor = fornecedorFactory.fromFirestore(snapshot);
    }
    return fornecedor;
  }

  async deleteFornecedor(fornecedor: Fornecedor): Promise<Fornecedor[]> {
    await this.firestore.doc(`fornecedores/${fornecedor.id}`).delete();
    return this.getFornecedores();
  }

  /** ESPÉCIES */

  async cadastrarEspecie(nome: string): Promise<Especie[]> {
    if (await this.getEspeciePorNome(nome)) {
      throw 'especie-ja-cadastrada';
    }

    await this.firestore.collection('especies').add({ nome: nome });
    return this.getEspecies();
  }

  async getEspeciePorNome(nome: string): Promise<Especie> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('especies', (ref) => ref.where('nome', '==', nome))
      .get()
      .pipe(first())
      .toPromise();
    if (snapshot.docs.length <= 0) return null;
    return especieFactory.fromFirestore(snapshot.docs[0]);
  }

  async getEspecies(): Promise<Especie[]> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('especies', (ref) => ref.orderBy('nome'))
      .get()
      .pipe(first())
      .toPromise();
    this.especies = snapshot.docs.map((s) => especieFactory.fromFirestore(s));
    return this.especies;
  }

  async getEspecieById(id: string): Promise<Especie> {
    let especie: Especie;
    this.especies.forEach((e) => {
      if (e.id.includes(id)) especie = e;
    });
    if (!especie) {
      let snapshot: firebase.default.firestore.DocumentSnapshot<unknown> =
        await this.firestore
          .collection('especies')
          .doc(id)
          .get()
          .pipe(first())
          .toPromise();
      especie = especieFactory.fromFirestore(snapshot);
    }
    return especie;
  }

  async deleteEspecie(especie: Especie): Promise<Especie[]> {
    await this.firestore.doc(`especies/${especie.id}`).delete();
    return this.getEspecies();
  }

  /** CALIBRES */

  async cadastrarCalibre(nome: string): Promise<Calibre[]> {
    if (await this.getCalibrePorNome(nome)) {
      throw 'calibre-ja-cadastrada';
    }

    await this.firestore.collection('calibres').add({ nome: nome });
    return this.getCalibres();
  }

  async getCalibrePorNome(nome: string): Promise<Calibre> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('calibres', (ref) => ref.where('nome', '==', nome))
      .get()
      .pipe(first())
      .toPromise();
    if (snapshot.docs.length <= 0) return null;
    return calibreFactory.fromFirestore(snapshot.docs[0]);
  }

  async getCalibres(): Promise<Calibre[]> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('calibres', (ref) => ref.orderBy('nome'))
      .get()
      .pipe(first())
      .toPromise();
    this.calibres = snapshot.docs.map((s) => calibreFactory.fromFirestore(s));
    return this.calibres;
  }

  async getCalibreById(id: string): Promise<Calibre> {
    let calibre: Calibre;
    this.calibres.forEach((c) => {
      if (c.id.includes(id)) calibre = c;
    });
    if (!calibre) {
      let snapshot: firebase.default.firestore.DocumentSnapshot<unknown> =
        await this.firestore
          .collection('calibres')
          .doc(id)
          .get()
          .pipe(first())
          .toPromise();
      calibre = calibreFactory.fromFirestore(snapshot);
    }

    return calibre;
  }

  async deleteCalibre(calibre: Calibre): Promise<Calibre[]> {
    await this.firestore.doc(`calibres/${calibre.id}`).delete();
    return this.getCalibres();
  }

  /**MODELOS */

  async cadastrarModelo(formulario: FormGroup): Promise<Modelo[]> {
    let fabricante: Fabricante;
    let especie: Especie;
    let calibre: Calibre;
    let data = formulario.value;
    if (formulario.invalid) {
      throw 'formulario-invalido';
    }
    this.fabricantes.forEach((f) => {
      if (f.id.includes(formulario.value['fabricante'])) {
        fabricante = f;
      }
    });

    this.especies.forEach((e) => {
      if (e.id.includes(formulario.value['especie'])) {
        especie = e;
      }
    });

    this.calibres.forEach((e) => {
      if (e.id.includes(formulario.value['calibre'])) {
        calibre = e;
      }
    });

    await this.firestore.collection('modelos').add({
      nome: data['nome'],
      capacidadeTiro: data['capacidadeTiro'],
      fabricanteId: fabricante.id,
      fabricanteNome: fabricante.nome,
      especieId: especie.id,
      especieNome: especie.nome,
      /*       'calibreId': calibre.id,
            'calibreNome': calibre.nome, */
      tipoAlma: data['tipoAlma'],
      quantidadeRaias: data['quantidadeRaias'],
      sentidoRaias: data['sentidoRaias'],
      numeroCanos: data['numeroCanos'],
      funcionamento: data['funcionamento'],
    });

    return this.getModelos();
  }

  async getModelos(): Promise<Modelo[]> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('modelos', (ref) => ref.orderBy('nome'))
      .get()
      .pipe(first())
      .toPromise();
    if (snapshot.docs.length == 0) return [];
    let modelosPromise = await snapshot.docs.map(async (s) => {
      let fabricante: Fabricante;
      try {
        fabricante = await this.getFabricanteById(s.data()['fabricanteId']);
      } catch (err) {
        console.error('Erro no modelo: ', s.data(), err);
      }
      let especie: Especie = await this.getEspecieById(s.data()['especieId']);
      return modeloFactory.fromFirestore(s, fabricante, especie);
    });
    this.modelos = await Promise.all(modelosPromise);
    return this.modelos;
  }

  async getModeloById(id: string): Promise<Modelo> {
    let modelo: Modelo;
    let fabricante: Fabricante;
    let especie: Especie;
    this.modelos.forEach((m) => {
      if (m.id.includes(id)) modelo = m;
    });
    if (!modelo) {
      let snapshot: firebase.default.firestore.DocumentSnapshot<unknown> =
        await this.firestore
          .collection('modelos')
          .doc(id)
          .get()
          .pipe(first())
          .toPromise();
      try {
        if (!snapshot.data()) {
          console.log('SNAPSHOT NAO ENCONTRADO: ', id, snapshot.data());
          return;
        }
        fabricante = await this.getFabricanteById(
          snapshot.data()['fabricanteId']
        );
        especie = await this.getEspecieById(snapshot.data()['especieId']);
      } catch (error) {
        console.error('Erro ao buscar modelo: ', error);
      }
      modelo = await modeloFactory.fromFirestore(snapshot, fabricante, especie);
    }

    return modelo;
  }

  async alterarModelo(
    modelo: Modelo,
    formulario: FormGroup
  ): Promise<Modelo[]> {
    console.log(formulario);
    let fabricante: Fabricante;
    let especie: Especie;
    let calibre: Calibre;
    let data = formulario.value;
    if (formulario.invalid) {
      throw 'formulario-invalido';
    }
    this.fabricantes.forEach((f) => {
      if (f.id.includes(formulario.value['fabricante'])) {
        fabricante = f;
      }
    });

    this.especies.forEach((e) => {
      if (e.id.includes(formulario.value['especie'])) {
        especie = e;
      }
    });

    this.calibres.forEach((e) => {
      if (e.id.includes(formulario.value['calibre'])) {
        calibre = e;
      }
    });

    await this.firestore.collection('modelos').doc(modelo.id).update({
      nome: data['nome'],
      fabricanteId: fabricante.id,
      fabricanteNome: fabricante.nome,
      especieId: especie.id,
      especieNome: especie.nome,
      tipoAlma: data['tipoAlma'],
      quantidadeRaias: data['quantidadeRaias'],
      sentidoRaias: data['sentidoRaias'],
      numeroCanos: data['numeroCanos'],
      capacidadeTiro: data['capacidadeTiro'],
      funcionamento: data['funcionamento'],
    });
    return this.getModelos();
  }

  async deleteModelo(modelo: Modelo): Promise<Modelo[]> {
    await this.firestore.collection('modelos').doc(modelo.id).delete();
    return this.getModelos();
  }

  /**CADASTRO DE ARMAS */

  async getArmaById(id: string) {
    let snapshot: firebase.default.firestore.DocumentSnapshot<any> =
      await this.firestore
        .collection('armas')
        .doc(id)
        .get()
        .pipe(first())
        .toPromise();
    let modelo: Modelo = await this.getModeloById(snapshot.data()['modeloId']);
    let calibre: Calibre = await this.getCalibreById(
      snapshot.data()['calibreId']
    );
    return armaFactory.fromFirestore(snapshot, calibre, modelo);
  }

  async getArmaByRegistro(registro: string): Promise<Arma> {
    let querySnapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('armas', (ref) => ref.where('numero', '==', registro))
      .get()
      .pipe(first())
      .toPromise();
    if (querySnapshot.docs.length == 0) throw 'nao-encontrado';
    if (querySnapshot.docs.length > 1) throw 'multiplos-cadastros';
    let snapshot = querySnapshot.docs[0];
    let modelo: Modelo = await this.getModeloById(snapshot.data()['modeloId']);
    let calibre: Calibre = await this.getCalibreById(
      snapshot.data()['calibreId']
    );
    return armaFactory.fromFirestore(snapshot, calibre, modelo);
  }

  async checkArmaCadastrada(numero: string): Promise<boolean> {
    let snapshot = await this.firestore
      .collection('armas', (ref) => ref.where('numero', '==', numero))
      .snapshotChanges()
      .pipe(first())
      .toPromise();
    return snapshot.length > 0;
  }

  async cadastrarArma(formulario: FormGroup): Promise<Arma> {
    let data = formulario.value;
    let calibre: Calibre = await this.getCalibreById(data['calibre']);
    let modelo: Modelo = await this.getModeloById(data['modelo']);

    let snapshot = await this.firestore
      .collection('armas', (ref) => ref.where('numero', '==', data['numero']))
      .snapshotChanges()
      .pipe(first())
      .toPromise();
    if (snapshot.length > 0) throw 'arma já cadastrada';
    let ref = await this.firestore.collection('armas').add({
      modeloId: data['modelo'],
      modeloNome: data['modeloNome'],
      modelo: modelo.asMap,
      notaFiscal: data['notaFiscal'],
      dataNotaFiscal: data['dataNotaFiscal'],
      numero: data['numero'],
      registroFederal: data['registroFederal'] ?? '',
      expedidor: data['expedidor'],
      uf: data['uf'],
      dataEmissao: data['dataEmissao'],
      pais: data['pais'],
      sinarm: data['sinarm'],
      sigma: data['sigma'],
      acabamento: data['acabamento'],
      calibreId: data['calibre'],
      pessoaJuridica: data['pessoaJuridica'],
      cnpj: data['cnpj'],
      isSigma: data['isSigma'],
      comprimentoCano: data['comprimentoCano'],
      isSinarm: data['isSinarm'],
      calibreNome: calibre.nome,
      calibre: calibre.asMap,
    });
    return armaFactory.fromFirestore(await ref.get(), calibre, modelo);
  }

  async alterarArma(arma: Arma, formulario: FormGroup): Promise<Arma> {
    let data = formulario.value;
    let calibre: Calibre = await this.getCalibreById(data['calibre']);
    let modelo: Modelo = await this.getModeloById(data['modelo']);

    await this.firestore.collection('armas').doc(arma.id).update({
      modeloId: data['modelo'],
      modeloNome: data['modeloNome'],
      modelo: modelo.asMap,
      notaFiscal: data['notaFiscal'],
      dataNotaFiscal: data['dataNotaFiscal'],
      numero: data['numero'],
      registroFederal: data['registroFederal'],
      expedidor: data['expedidor'],
      uf: data['uf'],
      dataEmissao: data['dataEmissao'],
      comprimentoCano: data['comprimentoCano'],
      sinarm: data['sinarm'],
      sigma: data['sigma'],
      pais: data['pais'],
      acabamento: data['acabamento'],
      calibreId: data['calibre'],
      pessoaJuridica: data['pessoaJuridica'],
      cnpj: data['cnpj'],
      isSigma: data['isSigma'],
      isSinarm: data['isSinarm'],
      calibreNome: calibre.nome,
      calibre: calibre.asMap,
    });
    return this.getArmaById(arma.id);
  }

  async getArmas(): Promise<Arma[]> {
    let snapshot: QuerySnapshot<unknown> = await this.firestore
      .collection('armas')
      .get()
      .pipe(first())
      .toPromise();
    let promises: Promise<Arma>[] = [];
    try {
      promises = snapshot.docs.map(async (s) => {
        try {
          let modelo: Modelo = await this.getModeloById(s.data()['modeloId']);
          let calibre: Calibre = await this.getCalibreById(
            s.data()['calibreId']
          );
          return armaFactory.fromFirestore(s, calibre, modelo);
        } catch (error) {
          console.error('Erro na arma: ', s.data(), s.id, error);
        }
      });
      this.armas = await Promise.all(promises);
    } catch (error) {
      console.error(error);
    }
    return this.armas;
  }

  /**ATIRADORES */

  async cadastrarCliente(
    nome: string,
    sexo: string,
    dataNascimento: Date,
    cpf: string,
    identidade: string,
    naturalidade: string,
    uf: string,
    orgaoIdentidade: string,
    expedicao: string,
    titulo: string,
    estadoCivil: string,
    cr: string,
    nomeMae: string,
    nomePai: string,
    profissao: string,
    modalidade: string,
    aposentado: string,
    ordem: string,
    posto: string,
    matricula: string,
    obm: string,
    endereco1: Endereco,
    endereco2: Endereco,
    telefone: string,
    email: string,
    atividadeApostilada: string,
    observacoes: string,
    senhaGov: string,
    foto?: File
  ): Promise<Cliente> {
    let pathFoto: string;
    let downloadUrl: string;
    if (!(await this.checaBDCpf(cpf))) {
      throw 'cpf-ja-cadastrado';
    }
    if (foto) {
      let task: UploadTaskSnapshot = await this.storage
        .ref(`${cpf}/foto/${foto.name}`)
        .put(foto);
      downloadUrl = await task.ref.getDownloadURL();
      pathFoto = task.ref.fullPath;
    }
    let documentReference = await this.firestore.collection('clientes').add({
      nome: nome,
      sexo: sexo,
      dataNascimento: dataNascimento,
      cpf: cpf,
      identidade: identidade,
      orgaoIdentidade: orgaoIdentidade,
      expedicao: expedicao,
      estadoCivil: estadoCivil,
      nomeMae: nomeMae,
      nomePai: nomePai,
      profissao: profissao,
      modalidade: modalidade,
      cr: cr ?? '',
      aposentado: aposentado,
      endereco1: endereco1.asMap ?? '',
      endereco2: endereco2?.asMap ?? '',
      telefone: telefone,
      email: email,
      titulo: titulo,
      uf: uf,
      naturalidade: naturalidade,
      observacoes: observacoes ?? '',
      atividadeApostilada: atividadeApostilada ?? 'não possui',
      posto: posto,
      excluido: false,
      ordem: ordem,
      matricula: matricula ?? '',
      senhaGov: senhaGov ?? '',
      obm: obm ?? '',
      foto: downloadUrl ?? '',
      fotoPath: pathFoto ?? '',
    });
    console.log(documentReference);
    return clienteFactory.fromFirestore(
      await this.firestore
        .doc(documentReference.path)
        .get()
        .pipe(first())
        .toPromise()
    );
  }

  async alterarCliente(
    cliente: Cliente,
    nome: string,
    sexo: string,
    dataNascimento: Date,
    cpf: string,
    identidade: string,
    naturalidade: string,
    uf: string,
    orgaoIdentidade: string,
    expedicao: string,
    titulo: string,
    estadoCivil: string,
    cr: string,
    nomeMae: string,
    nomePai: string,
    profissao: string,
    modalidade: string,
    aposentado: string,
    ordem: string,
    posto: string,
    matricula: string,
    obm: string,
    endereco1: Endereco,
    endereco2: Endereco,
    telefone: string,
    email: string,
    atividadeApostilada: string,
    observacoes: string,
    senhaGov: string,
    foto?: File
  ): Promise<Cliente> {
    let pathFoto: string;
    let downloadUrl: string;
    let updateData: any;
    updateData = {
      nome: nome,
      sexo: sexo,
      uf: uf,
      dataNascimento: dataNascimento,
      cpf: cpf,
      identidade: identidade,
      naturalidade: naturalidade,
      orgaoIdentidade: orgaoIdentidade,
      estadoCivil: estadoCivil,
      expedicao: expedicao,
      nomeMae: nomeMae,
      nomePai: nomePai,
      aposentado: aposentado,
      ordem: ordem,
      cr: cr,
      posto: posto,
      profissao: profissao,
      endereco1: endereco1.asMap ?? '',
      endereco2: endereco2.asMap ?? '',
      telefone: telefone,
      email: email,
      modalidade: modalidade,
      matricula: matricula ?? '',
      titulo: titulo ?? '',
      obm: obm ?? '',
      atividadeApostilada: atividadeApostilada,
      observacoes: observacoes,
      senhaGov: senhaGov,
    };
    console.log(foto);
    if (foto) {
      let task: UploadTaskSnapshot = await this.storage
        .ref(`${cpf}/foto/${foto.name}`)
        .put(foto);
      downloadUrl = await task.ref.getDownloadURL();
      pathFoto = task.ref.fullPath;
      updateData['foto'] = downloadUrl;
      updateData['fotoPath'] = pathFoto;
      console.log('download Url', downloadUrl);
    }
    console.log('cadastrarCliente');
    await this.firestore
      .collection('clientes')
      .doc(cliente.id)
      .update(updateData);

    return this.getClienteById(cliente.id);
  }

  async getClienteById(id: string): Promise<Cliente> {
    return clienteFactory.fromFirestore(
      await this.firestore
        .collection('clientes')
        .doc(id)
        .get()
        .pipe(first())
        .toPromise()
    );
  }

  async getClientes(): Promise<Cliente[]> {
    let querySnapshot: QuerySnapshot<any> = await this.firestore
      .collection('clientes', (ref) => ref.where('excluido', '==', false))
      .get()
      .pipe(first())
      .toPromise();
    return Promise.all(
      querySnapshot.docs.map((s) => {
        return clienteFactory.fromFirestore(s);
      })
    );
  }

  excluirCliente(cliente: Cliente): Promise<any> {
    return this.firestore
      .collection('clientes')
      .doc(cliente.id)
      .update({ excluido: true });
  }

  async cadastrarArquivo(
    descricao: string,
    arquivo: File,
    cliente: Cliente
  ): Promise<Cliente> {
    let task: AngularFireUploadTask = this.storage.upload(
      `clientes/${cliente.id}/${arquivo.name}`,
      arquivo
    );
    let url: string = await (await task).ref.getDownloadURL();
    let path: string = (await task).ref.fullPath;

    await this.firestore
      .collection('clientes')
      .doc(cliente.id)
      .collection('arquivos')
      .add({
        url: url,
        path: path,
        timestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
        descricao: descricao,
      });
    return this.getClienteById(cliente.id);
  }

  async excluirArquivo(cliente: Cliente, arquivo: Arquivo): Promise<Cliente> {
    await this.storage.ref(arquivo.path).delete().pipe(first()).toPromise();
    await this.firestore
      .collection('clientes')
      .doc(cliente.id)
      .collection('arquivos')
      .doc(arquivo.id)
      .delete();
    return this.getClienteById(cliente.id);
  }

  async checaBDCpf(cpf: string): Promise<boolean> {
    let snapshot = await this.firestore
      .collection('clientes', (ref) =>
        ref.where('cpf', '==', cpf).where('excluido', '==', false)
      )
      .get()
      .pipe(first())
      .toPromise();
    console.log(snapshot.docs[0], snapshot.empty);
    return snapshot.empty;
  }

  /**PRE-CADASTROS */

  getPreCadastros(): Observable<PreCadastro[]> {
    return this.firestore
      .collection<PreCadastro>('preCadastros', (ref) =>
        ref.where('confirmado', '==', false)
      )
      .valueChanges({ idField: 'id' });
  }

  salvarPreCadastro(formValue: any): Promise<any> {
    formValue['confirmado'] = false;

    return this.firestore.collection('preCadastros').add(formValue);
  }

  async preCadastroParaCliente(preCadastro: PreCadastro): Promise<Cliente> {
    let endereco = new Endereco(
      preCadastro.logradouro,
      preCadastro.cep,
      preCadastro.bairro,
      preCadastro.complemento ?? '',
      preCadastro.numero ?? '',
      preCadastro.cidade ?? '',
      preCadastro.estado ?? ''
    );
    let cliente: Cliente = await this.cadastrarCliente(
      preCadastro.nome,
      preCadastro.sexo,
      preCadastro.dataNascimento,
      preCadastro.cpf,
      preCadastro.identidade,
      preCadastro.naturalidade,
      preCadastro.uf,
      preCadastro.orgaoIdentidade,
      preCadastro.expedicao,
      preCadastro.titulo,
      preCadastro.estadoCivil,
      preCadastro.cr,
      preCadastro.nomeMae,
      preCadastro.nomePai,
      preCadastro.profissao,
      preCadastro.modalidade,
      preCadastro.aposentado,
      preCadastro.ordem,
      preCadastro.posto,
      preCadastro.matricula,
      preCadastro.obm,
      endereco,
      null,
      preCadastro.telefone,
      preCadastro.email,
      null,
      null,
      null
    );
    await this.firestore
      .doc(`preCadastros/${preCadastro.id}`)
      .update({ confirmado: true });
    return cliente;
  }
}
