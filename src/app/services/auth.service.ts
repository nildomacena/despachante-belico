import { UtilService } from './util.service';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  codigo: string = '';
  constructor(
    private auth: AngularFireAuth,
    private firestore: AngularFirestore,
    private utilService: UtilService
  ) {
    this.firestore
      .doc('controle/controle')
      .valueChanges()
      .subscribe((data) => {
        this.codigo = data['codigo'];
      });
  }

  login(
    email: string,
    password: string
  ): Promise<firebase.default.auth.UserCredential> {
    return this.auth.signInWithEmailAndPassword(email, password);
  }

  criarUsuario(
    email: string,
    password: string,
    codigo: string
  ): Promise<firebase.default.auth.UserCredential> {
    if (codigo != this.codigo) {
      this.utilService.toastrErro('Codigo inválido');
      return;
    }
    return this.auth.createUserWithEmailAndPassword(email, password);
  }

  logout() {
    return this.auth.signOut();
  }
}
