import { Cliente } from './../model/cliente.model';
import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Modalidade } from '../model/modalidade.model';
import { Profissao } from '../model/profissao.model';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  profissoes: Profissao[];
  modalidades: Modalidade[];
  constructor(private toastrService: ToastrService, private http: HttpClient) {
    this.profissoes = [
      new Profissao('Gerente', 'gerente'),
      new Profissao('Agente de Polícia Civil', 'pc'),
      new Profissao('Policial Militar', 'pm'),
      new Profissao('Bombeiro Militar', 'bm'),
      new Profissao('Microempreendedor', 'microempreendedor'),
      new Profissao('Comerciante', 'comerciante'),
      new Profissao('Autônomo - MEI', 'autonomo'),
      new Profissao('Mecânico', 'mecanico'),
    ];

    this.modalidades = [
      new Modalidade('Policial Militar', 'pm'),
      new Modalidade('Policial Civil', 'pc'),
      new Modalidade('Policial Penal', 'pp'),
      new Modalidade('Guarda Municipal', 'gm'),
      new Modalidade('Policial Federal', 'pf'),
      new Modalidade('Policial Rodoviário Federal', 'prf'),
      new Modalidade('Bombeiro Militar', 'bm'),
      new Modalidade('CAC', 'cac'),
      new Modalidade('Cidadão', 'cidadao'),
    ];
  }

  getModalidadeById(id: string): Modalidade {
    return this.modalidades.filter((m) => {
      return m.id == id;
    })[0];
  }
  toastrSucesso(titulo?: string, mensagem?: string) {
    this.toastrService.success(
      titulo ?? 'Sucesso',
      mensagem ?? 'Informações salvas'
    );
  }
  toastrErro(titulo?: string, mensagem?: string) {
    this.toastrService.error(
      mensagem ?? 'Ocorreu um erro durante o procedimento',
      titulo ?? 'Erro!'
    );
  }

  stringParaData(str: string): Date {
    return new Date(
      str.substr(0, 2) + '/' + str.substr(2, 2) + '/' + str.substr(4, 4)
    );
  }

  formatarCPF(cpf: string) {
    if (cpf.length != 11) {
      alert('O CPF do terceiro não está no formato correto');
      return cpf;
    }
    return (
      cpf.substring(0, 3) +
      '.' +
      cpf.substring(3, 6) +
      '.' +
      cpf.substring(6, 9) +
      '-' +
      cpf.substring(9, 11)
    );
  }

  dataAtual() {
    let hoje = new Date();
    return `${hoje.getUTCDate()}/${
      hoje.getUTCMonth() + 1 < 10
        ? '0' + (hoje.getUTCMonth() + 1).toFixed(0)
        : hoje.getUTCMonth() + 1
    }/${hoje.getFullYear()}`;
  }

  async assetToBase64(path: string): Promise<ArrayBuffer> {
    let base64data;
    let blob: Blob = await this.http
      .get(path ? path : 'assets/imgs/logo-federal.png', {
        responseType: 'blob',
      })
      .pipe(first())
      .toPromise();
    const reader = new FileReader();
    reader.onloadend = () => {
      base64data = reader.result;
      console.log('onloadend');
      //console.log(base64data);
    };

    reader.readAsDataURL(blob);
    console.log(blob);
    /* .subscribe(res => {
      const reader = new FileReader();
      reader.onloadend = () => {
        var base64data = reader.result;
        console.log(base64data);
      }

      reader.readAsDataURL(res);
      console.log(res);
    }); */
    await new Promise((resolve) => setTimeout(resolve, 1000));
    return base64data;
  }

  firstLetterUppercase(str: string): string {
    str = str.toLowerCase();
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }
  async imageCliente(cliente: Cliente) {}

  /*  async timeout(){
     await

   } */

  formatarData(data: Date): string {
    let dia: string =
      data.getDate() <= 9 ? '0' + data.getDate() : data.getDate().toString();
    let mes: string =
      data.getMonth() <= 9
        ? '0' + (data.getMonth() + 1).toString()
        : (data.getMonth() + 1).toString();
    return `${dia}/${mes}/${data.getFullYear()}`;
  }

  dataAtualPorExtenso(): string {
    let hoje = new Date();
    let mes: string;

    console.log('mes: ', hoje.getUTCMonth());
    switch (hoje.getMonth()) {
      case 0:
        mes = 'janeiro';
        break;
      case 1:
        mes = 'fevereiro';
        break;
      case 2:
        mes = 'março';
        break;
      case 3:
        mes = 'abril';
        break;
      case 4:
        mes = 'maio';
        break;
      case 5:
        mes = 'junho';
        break;
      case 6:
        mes = 'julho';
        break;
      case 7:
        mes = 'agosto';
        break;
      case 8:
        mes = 'setembro';
        break;
      case 9:
        mes = 'outubro';
        break;
      case 10:
        mes = 'novembro';
        break;
      case 11:
        mes = 'dezembro';
        break;
    }
    console.log(mes);
    return `${
      hoje.getUTCDate() < 10 ? '0' + hoje.getUTCDate() : hoje.getUTCDate()
    } de ${mes} de ${hoje.getFullYear()}`;
  }
}
