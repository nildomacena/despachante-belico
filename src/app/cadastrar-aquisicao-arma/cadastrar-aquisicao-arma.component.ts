import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Aquisicao } from '../model/aquisicao.model';
import { Calibre } from '../model/calibre.model';
import { Cliente } from '../model/cliente.model';
import { Fornecedor } from '../model/fornecedor.model';
import { Modalidade } from '../model/modalidade.model';
import { Modelo } from '../model/modelo.model';
import { FireService } from '../services/fire.service';
import { ProcedimentosService } from '../services/procedimentos.service';
import { UtilService } from '../services/util.service';
declare var jQuery: any;

@Component({
  selector: 'app-cadastrar-aquisicao-arma',
  templateUrl: './cadastrar-aquisicao-arma.component.html',
  styleUrls: ['./cadastrar-aquisicao-arma.component.scss']
})
export class AquisicaoArmaComponent implements OnInit {
  cliente: Cliente;
  clientes: Cliente[];
  clienteSelecionado: Cliente;
  clientesFiltrados: Cliente[];
  modeloSelecionado: Modelo;
  modelos: Modelo[];
  modelosFiltrados: Modelo[];
  pesquisa: string;
  formAquisicao: FormGroup;
  calibres: Calibre[];
  fornecedores: Fornecedor[];
  fornecedoresFiltrados: Fornecedor[];
  fornecedorSelecionado: Fornecedor;
  salvando: boolean;
  isSingleClick: Boolean = true;   //Variável para controlar click simples e click duplo
  modalidade: Modalidade;
  constructor(
    private fireService: FireService,
    private formBuilder: FormBuilder,
    private procedimentosService: ProcedimentosService,
    private utilService: UtilService,
    private router: Router
  ) {
    this.formAquisicao = this.formBuilder.group({
      'modelo': new FormControl('', [Validators.required]),
      'modeloDescricao': new FormControl('', [Validators.required]),
      'cliente': new FormControl('', [Validators.required]),
      'clienteNome': new FormControl('', [Validators.required]),
      'fornecedor': new FormControl('', [Validators.required]),
      'fornecedorNome': new FormControl('', [Validators.required]),
      'calibre': new FormControl('', [Validators.required]),
      'acabamento': new FormControl('', [Validators.required]),
      'formaPagamento': new FormControl('', [Validators.required]),
      'email': new FormControl('miro.macena@gmail.com', [Validators.required]),
      'observacao': new FormControl('',),
      'local': new FormControl('', [Validators.required]),

    })
    this.initPromises();
    if (this.router.getCurrentNavigation()?.extras?.state) {
      let cliente: Cliente = this.router.getCurrentNavigation().extras.state.cliente
      console.log(cliente)
      this.updateForm(cliente);
      this.modalidade = this.utilService.getModalidadeById(cliente.modalidade);
      //this.formCliente.patchValue(this.atiradorNav.formGroup)
    }
  }

  ngOnInit(): void {
  }

  updateForm(cliente: Cliente) {
    this.clienteSelecionado = cliente;
    this.closeSelectedCliente();
  }

  consolee() {
    console.log(this.formAquisicao)
  }
  async initPromises() {
    this.clientesFiltrados = this.clientes = await this.fireService.getClientes();
    this.modelosFiltrados = this.modelos = await this.fireService.getModelos();
    this.fornecedoresFiltrados = this.fornecedores = await this.fireService.getFornecedores()
    this.calibres = await this.fireService.getCalibres();
    //jQuery("#selectCalibres").selectpicker("refresh");
  }

  onSearch() {
    if (this.pesquisa.length <= 0) {
      this.modelosFiltrados = this.modelos;
      return;
    }
    else {
      this.modelosFiltrados = this.modelos.filter(m =>
        m.nome.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.especie.nome.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.tipoAlma.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.fabricante.nome.toLowerCase().includes(this.pesquisa.toLowerCase()));
    }
  }

  modalidadeDescricao(modalidade: Modalidade): Modalidade {
    return this.modalidade = this.utilService.modalidades.filter(m => {
      return m.id == modalidade.id
    })[0]
  }

  selecionarModalidade(modalidade) {

    this.modalidade = modalidade;
    this.formAquisicao.controls['local'].reset()
    console.log('modalidade  PM', this.formAquisicao.controls['local']);
    console.log(this.cliente)
    if (this.modalidade.id == 'pm') {
      this.formAquisicao.patchValue({
        'local': 'DAL – Diretoria de Apoio Logístico/Seção de Armamento. Endereço: Avenida Governador Luíz Cavalcante, 1007, Tabuleiro, Maceió – AL'
      });
    }
    else if (this.modalidade.id == 'bm') {
      this.formAquisicao.patchValue({
        'local': 'QGC Corpo De Bombeiros Militar - Av. Siqueira Campos, 1739 - Trapiche Da Barra – Maceió/AL - CEP: 57010-405'
      });
    }
    else if (this.modalidade.id == 'pc') {
      this.formAquisicao.patchValue({
        'local': 'NURRCAME- Delegacia Geral de Polícia Civil de Alagoas- Avenida General Luiz de França Albuquerque (Rodovia AL 101 Norte) - CEP 57038-640'
      });
    }
    else if (this.cliente || this.clienteSelecionado) {
      console.log('entrou no else')
      this.formAquisicao.patchValue({
        'local': this.cliente ? this.cliente.endereco1.formatado : this.clienteSelecionado.endereco1.formatado
      });
    }
  }

  abrirModal(idModal:string){
    jQuery(idModal).modal('show');

  }

  onSelectFornecedor(fornecedor) {
    setTimeout(() => {
      if (this.isSingleClick) {
        if (fornecedor == this.fornecedorSelecionado) {
          this.fornecedorSelecionado = null;
        }
        else {
          this.fornecedorSelecionado = fornecedor;
        }
        console.log(this.clienteSelecionado);
      }
    }, 250)

    console.log(this.fornecedorSelecionado);
  }

  closeSelectedFornecedor(fornecedor?: Fornecedor) {
    if (fornecedor)
      this.fornecedorSelecionado = fornecedor;
    this.formAquisicao.patchValue({
      'fornecedorNome': this.fornecedorSelecionado.nome,
      'fornecedor': this.fornecedorSelecionado,
    });
    console.log(this.formAquisicao, this.fornecedorSelecionado)
    jQuery('#fornecedorModal').modal('hide');

  }

  onSelectCliente(cliente) {
    this.isSingleClick = true;

    setTimeout(() => {
      if (this.isSingleClick) {
        if (cliente == this.clienteSelecionado) {
          this.clienteSelecionado = null;
        }
        else {
          this.clienteSelecionado = cliente;
        }
        console.log(this.clienteSelecionado);
      }
    }, 250)
  }

  closeSelectedCliente(cliente?: Cliente) {
    this.isSingleClick = false;
    if (cliente)
      this.clienteSelecionado = cliente;
    this.formAquisicao.patchValue({
      'clienteNome': this.clienteSelecionado.nome,
      'cliente': this.clienteSelecionado,
      'email': this.clienteSelecionado.email,
    });
    this.modalidade = this.utilService.getModalidadeById(this.clienteSelecionado.modalidade);
    this.selecionarModalidade(this.modalidade)
    console.log(this.modalidade)
    if (this.modalidade.id != 'pm' && this.modalidade.id != 'bm') {
      this.formAquisicao.patchValue({
        'local': this.clienteSelecionado.endereco1.formatado
      });
    }
    console.log(this.clienteSelecionado)
    jQuery('#clienteModal').modal('hide');
  }

  onSelectModelo(modelo) {
    setTimeout(() => {
      if (this.isSingleClick) {
        if (modelo == this.modeloSelecionado) {
          this.modeloSelecionado = null;
        }
        else {
          this.modeloSelecionado = modelo;
        }
        console.log(this.clienteSelecionado);
      }
    }, 250)


  }

  closeSelectedModelo(modelo?: Modelo) {
    if (modelo)
      this.modeloSelecionado = modelo;
    this.formAquisicao.patchValue({
      'modeloNome': this.modeloSelecionado.nome,
      'modeloDescricao': this.modeloSelecionado.fabricante.nome + ' - ' + this.modeloSelecionado.nome,
      'modelo': this.modeloSelecionado,
    });
    jQuery('#modeloModal').modal('hide');

    console.log(this.formAquisicao, this.modeloSelecionado)
  }


  resetModalidade() {
    this.modalidade = null;
    this.formAquisicao.patchValue({ 'local': '' })

  }

  goToModelo() { }


  reset(): void {
    this.formAquisicao.reset();
    this.formAquisicao.patchValue({
      'email': '',
      'local': 'DAL – Diretoria de Apoio Logístico/Seção de Armamento. Endereço: Avenida Governador Luíz Cavalcante, 1007, Tabuleiro, Maceió – AL',
    });
    this.clienteSelecionado = this.modeloSelecionado = this.fornecedorSelecionado = null;
    this.salvando = false;
  }

  async onSubmit() {
    if (this.formAquisicao.invalid) {
      alert('Formulário inválido.\nVerifique os formulários e tente novamente.')
    }
    try {
      console.log(this.formAquisicao);
      let data = this.formAquisicao.value;
      let calibre: Calibre = this.calibres.filter(c => c.id == data['calibre'])[0];
      this.formAquisicao.patchValue({
        'calibre': calibre
      });
      data = this.formAquisicao.value;
      this.salvando = true;
      let aquisicao: Aquisicao = await this.procedimentosService.cadastrarAquisicao(
        data['modelo'],
        data['cliente'],
        data['fornecedor'],
        data['calibre'],
        data['acabamento'],
        data['formaPagamento'],
        data['email'],
        data['observacao'],
        data['local'],
        this.modalidade.id
      );
      this.utilService.toastrSucesso();
      this.reset();
      this.router.navigate(['aquisicao-detail'], { state: { aquisicao: aquisicao } });

    } catch (error) {
      console.error(error);
      this.utilService.toastrErro();
    }
  }

}
