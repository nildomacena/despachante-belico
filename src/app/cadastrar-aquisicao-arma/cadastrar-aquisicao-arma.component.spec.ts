import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AquisicaoArmaComponent } from './cadastrar-aquisicao-arma.component';

describe('AquisicaoArmaComponent', () => {
  let component: AquisicaoArmaComponent;
  let fixture: ComponentFixture<AquisicaoArmaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AquisicaoArmaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AquisicaoArmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
