import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAquisicaoComponent } from './lista-aquisicao.component';

describe('ListaAquisicaoComponent', () => {
  let component: ListaAquisicaoComponent;
  let fixture: ComponentFixture<ListaAquisicaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaAquisicaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAquisicaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
