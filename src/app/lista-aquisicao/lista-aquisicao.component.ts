import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Aquisicao } from '../model/aquisicao.model';
import { Pendencia } from '../model/pendencia.model';
import { ProcedimentosService } from '../services/procedimentos.service';

@Component({
  selector: 'app-lista-aquisicao',
  templateUrl: './lista-aquisicao.component.html',
  styleUrls: ['./lista-aquisicao.component.scss'],
})
export class ListaAquisicaoComponent implements OnInit {
  aquisicoes: Aquisicao[] = [];
  aquisicoesFiltradas: Aquisicao[] = [];
  pesquisa: string;
  filtroNome: boolean;
  filtroArma: boolean;
  filtroData: boolean;
  filtroAtividade: boolean;
  pendencias: Pendencia[] = [];
  asc: boolean = true;
  filtroPendencias: Pendencia[] = [];
  constructor(
    private procedimentosService: ProcedimentosService,
    private router: Router
  ) {

  }

  ngOnInit(): void { this.initPromises();
    this.pendencias = this.procedimentosService.pendencias;}

  pesquisar() {
    console.log(this.pesquisa);
    if (this.pesquisa.length > 0) {
      this.aquisicoesFiltradas = this.aquisicoes.filter((a) => {
        return a.pesquisa(this.pesquisa);
      });
    } else this.aquisicoesFiltradas = this.aquisicoes;
  }

  irParaAquisicao(aquisicao: Aquisicao) {
    this.router.navigate(['aquisicao-detail'], {
      state: { aquisicao: aquisicao },
    });
  }

  async initPromises() {
    this.aquisicoes = this.aquisicoesFiltradas =
      await this.procedimentosService.getAquisicoes();
    console.log(this.aquisicoes);

    this.filtroData = true;
    this.asc = false;
  }

  onFiltraPendencia(pendencia: Pendencia) {
    if (this.filtroPendencias.length == 0) {
      this.filtroPendencias.push(pendencia);
    } else {
      let indexOf = this.filtroPendencias.indexOf(pendencia);
      if (indexOf > -1) {
        this.filtroPendencias.splice(indexOf, 1);
      } else this.filtroPendencias.push(pendencia);
    }
    if (this.filtroPendencias.length == 0) {
      this.aquisicoesFiltradas = this.aquisicoes;
      return;
    }
    this.aquisicoesFiltradas = this.aquisicoes.filter((a) => {
      return !a.getPendenciaResolvida(pendencia);
    });
  }

  getPendenciaSelecionada(pendencia: Pendencia): boolean {
    return this.filtroPendencias.indexOf(pendencia) > -1;
  }

  onSort(campo: string) {
    if (campo == 'nome') {
      if (this.filtroNome) {
        this.asc = !this.asc;
      }
      this.filtroNome = true;
      this.filtroData = this.filtroArma = this.filtroAtividade = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        if (a.cliente.nome > b.cliente.nome) return this.asc ? 1 : -1;
        else if (a.cliente.nome < b.cliente.nome) return this.asc ? -1 : 1;
        else return 0;
      });
    } else if (campo == 'atividade') {
      if (this.filtroAtividade) {
        this.asc = !this.asc;
      }
      this.filtroAtividade = true;
      this.filtroData = this.filtroArma = this.filtroNome = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        console.log(a.ultimaAtividade.getTime(), b.ultimaAtividade.getTime());
        if (a.ultimaAtividade.getTime() > b.ultimaAtividade.getTime())
          return this.asc ? 1 : -1;
        else if (a.ultimaAtividade.getTime() < b.ultimaAtividade.getTime())
          return this.asc ? -1 : 1;
        else return 0;
      });
    } else if (campo == 'arma') {
      if (this.filtroArma) {
        this.asc = !this.asc;
      }
      this.filtroArma = true;
      this.filtroData = this.filtroNome = this.filtroAtividade = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        if (a.modelo.nome > b.modelo.nome) return this.asc ? 1 : -1;
        else if (a.modelo.nome < b.modelo.nome) return this.asc ? -1 : 1;
        else return 0;
      });
    } else if (campo == 'data') {
      if (this.filtroData) {
        this.asc = !this.asc;
      }
      this.filtroData = true;
      this.filtroArma = this.filtroNome = this.filtroAtividade = false;
      this.aquisicoesFiltradas = this.aquisicoes.sort((a, b) => {
        if (a.data > b.data) return this.asc ? 1 : -1;
        else if (a.modelo.nome < b.modelo.nome) return this.asc ? -1 : 1;
        else return 0;
      });
    }
  }
}
