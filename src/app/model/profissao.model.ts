export class Profissao {
    nome: string;
    id: string;

    constructor(nome: string, id: string) {
        this.nome = nome;
        this.id = id;
    }
}