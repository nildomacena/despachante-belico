import {
  DocumentChangeAction,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';

export class Fabricante {
  id: string;
  nome: string;
  cnpj: string;

  constructor(nome: string, cnpj: string, id?: string) {
    this.nome = nome;
    this.cnpj = cnpj;
    this.id = id ?? '';
  }

  get cnpjFormatado(): string {
    return (
      this.cnpj.substr(0, 2) +
      '.' +
      this.cnpj.substr(2, 3) +
      '.' +
      this.cnpj.substr(5, 3) +
      '/' +
      this.cnpj.substr(8, 4) +
      '-' +
      this.cnpj.substr(12, 2)
    );
  }

  get asMap() {
    return {
      id: this.id,
      cnpj: this.cnpj,
      nome: this.nome,
    };
  }
}

export function fromFirestore(snapshot: QueryDocumentSnapshot<unknown>) {
  let data = snapshot.data();
  console.log('data Fabricante fromFirestore: ', data);
  return new Fabricante(
    data['nome'] ?? 'Sem nome',
    data['cnpj'] ?? 'Sem cnpj',
    snapshot.id
  );
}
export function fromSnapshotFirestore(snapshot: DocumentChangeAction<unknown>) {
  let data = snapshot.payload.doc.data();
  return new Fabricante(data['nome'], data['cnpj'], snapshot.payload.doc.id);
}
export function fromMap(data: any) {
  return new Fabricante(data['nome'], data['cnpj'], data['id']);
}
