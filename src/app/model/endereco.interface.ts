export interface Endereco {
  logradouro: string;
  cep: string;
  bairro: string;
  complemento: string;
  numero: string;
  cidade: string;
  estado: string;
}
