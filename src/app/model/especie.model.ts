import {
  DocumentChangeAction,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';

export class Especie {
  id: string;
  nome: string;

  constructor(nome: string, id?: string) {
    this.nome = nome;
    this.id = id ?? '';
  }
  get asMap() {
    return {
      id: this.id,
      nome: this.nome,
    };
  }
}

export function fromFirestore(snapshot: QueryDocumentSnapshot<unknown>) {
  let data = snapshot.data();
  return new Especie(data['nome'], snapshot.id);
}
export function fromSnapshotFirestore(snapshot: DocumentChangeAction<unknown>) {
  let data = snapshot.payload.doc.data();
  return new Especie(data['nome'], snapshot.payload.doc.id);
}
