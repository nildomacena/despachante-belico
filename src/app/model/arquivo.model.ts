import { QueryDocumentSnapshot } from "@angular/fire/firestore";

export class Arquivo {
    id: string;
    descricao: string;
    url: string;
    path: string;
    data: Date;
    constructor(id: string, descricao: string, url: string, path: string, data: Date) {
        this.descricao = descricao;
        this.url = url;
        this.path = path;
        this.data = data;
        this.id = id ?? '';
    }

    get toMap() {
        return {
            'descricao': this.descricao,
            'url': this.url,
            'path': this.path,
            'data': this.data
        }
    }
}

export function fromMap(data): Arquivo {
    return new Arquivo('', data['descricao'], data['url'], data['path'], data['timestamp'] ?? new Date);
}

export function fromFirestore(snapshot: QueryDocumentSnapshot<unknown>): Arquivo {
    let data = snapshot.data();
    return new Arquivo(snapshot.id, data['descricao'], data['url'], data['path'], data['timestamp'].toDate() ?? new Date);
}