export class Endereco {
    logradouro: string;
    cep: string;
    bairro: string;
    complemento: string;
    numero: string;
    cidade: string;
    estado: string;

    constructor(logradouro: string, cep: string, bairro: string, complemento: string, numero: string, cidade: string, estado: string) {
        this.logradouro = logradouro;
        this.bairro = bairro;
        this.cep = cep;
        this.complemento = complemento;
        this.numero = numero;
        this.cidade = cidade;
        this.estado = estado;
    }

    get formatado(): string {
        return `${this.logradouro}, ${this.numero}, ${this.complemento ? this.complemento + ', ' : ''} ${this.bairro}. ${this.cidade}-${this.estado}. CEP: ${this.cep}`
    }
    get asMap() {
        return {
            logradouro: this.logradouro,
            bairro: this.bairro,
            cep: this.cep,
            complemento: this.complemento ?? '',
            numero: this.numero,
            cidade: this.cidade,
            estado: this.estado,
        }
    }
}

export function fromMap(data) {

    return new Endereco(
        data['logradouro'],
        data['cep'],
        data['bairro'],
        data['complemento'],
        data['numero'],
        data['cidade'],
        data['estado']);
}