import {
  DocumentChangeAction,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';
import { FireService } from '../services/fire.service';
import { Especie } from './especie.model';
import { Fabricante } from './fabricante.model';
import * as fabricanteFactory from './fabricante.model';
import * as especieFactory from './fabricante.model';

export class Modelo {
  id: string;
  nome: string;
  fabricante: Fabricante;
  especie: Especie;
  numeroCanos: string;
  capacidadeTiro: string;
  tipoAlma: string;
  quantidadeRaias: string;
  sentidoRaias: string;
  funcionamento: string;

  constructor(
    nome: string,
    fabricante: Fabricante,
    especie: Especie,
    numeroCanos: string,
    tipoAlma: string,
    quantidadeRaias: string,
    funcionamento: string,
    sentidoRaias,
    capacidadeTiro: string,
    id?: string
  ) {
    this.nome = nome;
    this.fabricante = fabricante;
    this.especie = especie;
    this.numeroCanos = numeroCanos;
    this.capacidadeTiro = capacidadeTiro;
    this.tipoAlma = tipoAlma;
    this.quantidadeRaias = quantidadeRaias;
    this.funcionamento = funcionamento;
    this.id = id ?? '';
    this.sentidoRaias = sentidoRaias;
  }

  get funcionamentoFormatado(): string {
    if (this.funcionamento.toLowerCase().includes('semiautomatico'))
      return 'semiautomático';
    return 'repetição';
  }
  get asMap() {
    return {
      nome: this.nome,
      fabricante: this.fabricante.asMap,
      especie: this.especie.asMap,
      numeroCanos: this.numeroCanos,
      capacidadeTiro: this.capacidadeTiro ?? '',
      tipoAlma: this.tipoAlma,
      quantidadeRaias: this.quantidadeRaias,
      funcionamento: this.funcionamento,
      id: this.id,
      sentidoRaias: this.sentidoRaias,
    };
  }

  get descricao(): string {
    return this.fabricante.nome + ' - ' + this.nome;
  }
}

export function fromFirestore(
  snapshot: QueryDocumentSnapshot<unknown>,
  fabricante: Fabricante,
  especie: Especie
): Modelo {
  let data = snapshot.data();
  return new Modelo(
    data['nome'],
    fabricante,
    especie,
    data['numeroCanos'],
    data['tipoAlma'],
    data['quantidadeRaias'],
    data['funcionamento'],
    data['sentidoRaias'],
    data['capacidadeTiro'],
    snapshot.id
  );
}
export function fromSnapshotFirestore(
  snapshot: DocumentChangeAction<unknown>,
  fabricante: Fabricante,
  especie: Especie
): Modelo {
  let data = snapshot.payload.doc.data();
  return new Modelo(
    data['nome'],
    fabricante,
    especie,
    data['numeroCanos'],
    data['tipoAlma'],
    data['quantidadeRaias'],
    data['funcionamento'],
    data['sentidoRaias'],
    data['capacidadeTiro'],
    snapshot.payload.doc.id
  );
}

export function fromMap(data: any): Modelo {
  let fabricante: Fabricante = fabricanteFactory.fromMap(data['fabricante']);
  let especie: Especie = especieFactory.fromMap(data['especie']);

  return new Modelo(
    data['nome'],
    fabricante,
    especie,
    data['numeroCanos'],
    data['tipoAlma'],
    data['quantidadeRaias'],
    data['funcionamento'],
    data['sentidoRaias'],
    data['capacidadeTiro'],
    data['id']
  );
}
