import { Arma } from './arma.model';
import { Cliente } from './cliente.model';
import * as clienteFactory from './cliente.model';
import * as armaFactory from './arma.model';
import { QueryDocumentSnapshot } from '@angular/fire/firestore';

export class Transferencia {
  id: string;
  doador: Cliente;
  recebedor: Cliente;
  arma: Arma;
  modalidade: string;
  observacoes: string;
  data: Date;
  constructor(
    id: string,
    doador: Cliente,
    recebedor: Cliente,
    arma: Arma,
    modalidade: string,
    observacoes: string,
    data: Date
  ) {
    this.id = id;
    this.doador = doador;
    this.recebedor = recebedor;
    this.modalidade = modalidade;
    this.arma = arma;
    this.observacoes = observacoes;
    this.data = data;
  }

  pesquisa(str: string): boolean {
    if (!str || str.length == 0) return true;
    str = str.toLowerCase();
    return (
      this.doador.nome?.toLowerCase().includes(str) ||
      this.recebedor.nome?.toLowerCase().includes(str) ||
      this.doador.cpf?.toLowerCase().includes(str) ||
      this.doador.identidade?.toLowerCase().includes(str) ||
      this.doador.email?.toLowerCase().includes(str) ||
      this.recebedor.cpf?.toLowerCase().includes(str) ||
      this.recebedor.identidade?.toLowerCase().includes(str) ||
      this.recebedor.email?.toLowerCase().includes(str) ||
      this.arma.modelo.nome?.toLowerCase().includes(str)
    );
  }

  get modalidadeFormatada(): string {
    if (this.modalidade == 'sigmaXsinarm') return 'Sigma X Sinarm';

    if (this.modalidade == 'sigmaXsigma') return 'Sigma X Sigma';

    if (this.modalidade == 'sinarmXsinarm') return 'Sinarm X Sinarm';

    if (this.modalidade == 'sinarmXsigma') return 'Sinarm X Sigma';
  }

  async updateClientes() {}
}
export function transferenciaTest(): Transferencia {
  return new Transferencia(
    'whz5gFXlsk5RH04xLFo9',
    clienteFactory.fromMap({
      id: '02yvGueqlmLIv7B8yXPN',
      nome: 'TARCIZIO VITORINO DA SILVA',
      sexo: 'masculino',
      dataNascimento: new Date(),
      cpf: '28496744434',
      identidade: '314341',
      orgaoIdentidade: 'SSP/AL',
      estadoCivil: 'casado',
      nomeMae: 'ROZALIA VITORINO DA SILVA',
      nomePai: 'MANOEL FERREIRA DA SILVA',
      profissao: 'Policial Civil',
      aposentado: '',
      endereco2: {
        logradouro: '',
        bairro: '',
        cep: '',
        complemento: '',
        numero: '',
        cidade: '',
        estado: '',
      },
      endereco1: {
        logradouro: 'RUA PAULINA MARIA MENDONÇA',
        bairro: 'JATIUCA',
        cep: '57035557',
        complemento: 'APTO. 801',
        numero: '514',
        cidade: 'MACEIÓ',
        estado: 'AL',
      },
      telefone: '82991786226',
      email: 'tvsdel@hotmail.com',
      atividadeApostilada: 'nao_aplica',
      naturalidade: 'Major Izidoro/AL',
      arquivos: [],
      ordem: '',
      posto: '',
      modalidade: 'pc',
      cr: '',
    }),
    clienteFactory.fromMap({
      id: '0eUdpqKMzcy8y8OurmdJ',
      nome: 'JOSE MAURICIO DA SILVA',
      sexo: 'masculino',
      dataNascimento: new Date(),
      cpf: '75794110406',
      identidade: '1017679',
      orgaoIdentidade: 'SSP/AL',
      estadoCivil: 'casado',
      nomeMae: 'MARIA LUCIA DA SILVA',
      nomePai: 'CICERO MANOEL DA SILVA',
      profissao: 'Policial Civil',
      aposentado: '',
      endereco2: {
        logradouro: '',
        bairro: '',
        cep: '',
        complemento: '',
        numero: '',
        cidade: '',
        estado: '',
      },
      endereco1: {
        logradouro: 'RUA CORONEL ALCANTARA',
        bairro: 'CENTRO',
        cep: '57995000',
        complemento: '',
        numero: '72',
        cidade: 'FLEXEIRAS',
        estado: 'AL',
      },
      telefone: '82991851851',
      email: 'mauricio.flex@hotmail.com',
      atividadeApostilada: 'nao_aplica',
      naturalidade: 'FLEIXEIRAS/AL',
      arquivos: [],
      ordem: '',
      posto: '',
      modalidade: 'pc',
      foto: '',
      cr: '',
    }),
    armaFactory.fromMap({
      notaFiscal: '123421',
      dataNotaFiscal: '2021-03-12T03:00:00.000Z',
      numero: '4321',
      registroFederal: '12341',
      expedidor: 'asdfsa',
      dataEmissao: '1992-01-11T02:00:00.000Z',
      sinarm: '4321',
      sigma: '',
      pais: 'brasil',
      calibre: {
        nome: '44-40',
        id: 'Ed41Lz2v1geNmQCZNn0n',
      },
      acabamento: 'oxidado',
      modelo: {
        nome: 'G2C',
        fabricante: {
          nome: 'Taurus',
          cnpj: '22131221000112',
          id: 'UKJeMG1e0j9Q8SqV4D2T',
        },
        especie: {
          nome: 'Pistola',
          id: 'BntwYxXpRfQAtU7QBA21',
        },
        numeroCanos: '1',
        capacidadeTiro: '12',
        tipoAlma: 'lisa',
        quantidadeRaias: 'nao_aplica',
        funcionamento: 'semiautomatico',
        id: 'IzHHQSQri7AE6USzQ6gY',
        sentidoRaias: 'nao_aplica',
      },
      cnpj: '12342134214342',
      pessoaJuridica: '123421342',
      isSigma: false,
      isSinarm: true,
      id: 'IRbmcR9vvU0u3B5D6JvC',
    }),
    'sinarmXsigma',
    '',
    new Date()
  );
}
export function fromFirestore(
  snapshot:
    | firebase.default.firestore.DocumentSnapshot<unknown>
    | QueryDocumentSnapshot<unknown>
): Transferencia {
  let data = snapshot.data();
  let doador: Cliente = clienteFactory.fromMap(data['doador']);
  let recebedor: Cliente = clienteFactory.fromMap(data['recebedor']);
  let arma: Arma = armaFactory.fromMap(data['arma']);

  return new Transferencia(
    snapshot.id,
    doador,
    recebedor,
    arma,
    data['modalidade'],
    data['observacoes'],
    data['timestamp'].toDate()
  );
}
