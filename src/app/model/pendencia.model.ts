export class Pendencia {
    descricao: string;
    resolvida: boolean;
    data: Date;
    constructor(descricao: string, resolvida?: boolean, data?: Date) {
        this.data = data;
        this.resolvida = resolvida ?? false;
        this.descricao = descricao;
    }
    get toMap() {
        return {
            'descricao': this.descricao,
            'resolvida': this.resolvida,
            'data': this.data,
        }
    }
}
export function fromMap(data: any): Pendencia {
    return new Pendencia(
        data['descricao'],
        data['resolvida'] ?? false,
        data['data'].toDate() ?? null);
}