import { Endereco } from './endereco.interface';
import * as firebase from 'firebase';

export class PreCadastro {
  id: string;
  nome: string;
  sexo: string;
  dataNascimento: any;
  cpf: string;
  identidade: string;
  naturalidade: string;
  uf: string;
  orgaoIdentidade: string;
  titulo: string;
  estadoCivil: string;
  cr: string;
  nomeMae: string;
  nomePai: string;
  profissao: string;
  modalidade: string;
  aposentado: string;
  ordem: string;
  posto: string;
  matricula: string;
  expedicao: any;
  obm: string;
  logradouro: string;
  cep: string;
  bairro: string;
  complemento: string;
  numero: string;
  estado: string;
  cidade: string;
  telefone: string;
  email: string;
}
