import { QueryDocumentSnapshot, QuerySnapshot } from "@angular/fire/firestore";
import { Calibre } from "./calibre.model";
import { Cliente } from "./cliente.model";
import { Fornecedor } from "./fornecedor.model";
import { Modelo } from "./modelo.model";
import { Pendencia } from "./pendencia.model";

import * as clienteFactory from "./cliente.model";
import * as fornecedorFactory from "./fornecedor.model";
import * as calibreFactory from "./calibre.model";
import * as modeloFactory from "./modelo.model";
import * as pendenciaFactory from "./pendencia.model";
import * as arquivoFactory from './arquivo.model';

import { Arquivo } from "./arquivo.model";
import { Modalidade } from "./modalidade.model";

export class Aquisicao {
    id: string;
    cliente: Cliente;
    fornecedor: Fornecedor;
    calibre: Calibre;
    acabamento: string;
    email: string;
    observacao: string;
    local: string;
    data: Date;
    modelo: Modelo;
    formaPagamento: string;
    pendenciasResolvidas: Pendencia[];
    ultimaPendencia: Pendencia;
    ultimaAtividade: Date;
    modalidade: string;
    arquivos: Arquivo[];
    constructor(id: string,
        cliente: Cliente,
        modelo: Modelo,
        fornecedor: Fornecedor,
        calibre: Calibre,
        acabamento: string,
        formaPagamento: string,
        email: string,
        observacao: string,
        local: string,
        pendenciasResolvidas: Pendencia[],
        ultimaPendencia: Pendencia,
        ultimaAtividade: Date,
        data: Date,
        modalidade: string,
        arquivos: Arquivo[]
    ) {
        this.id = id;
        this.cliente = cliente;
        this.fornecedor = fornecedor;
        this.calibre = calibre;
        this.acabamento = acabamento;
        this.email = email;
        this.observacao = observacao;
        this.local = local;
        this.data = data;
        this.modelo = modelo;
        this.formaPagamento = formaPagamento;
        this.pendenciasResolvidas = pendenciasResolvidas ?? [];
        this.ultimaPendencia = ultimaPendencia;
        this.ultimaAtividade = ultimaAtividade;
        this.arquivos = arquivos;
        this.modalidade = modalidade;
    }

    pesquisa(str: string): boolean {
        str = str.toLowerCase();
        return this.cliente.nome?.toLowerCase().includes(str) ||
            this.cliente.cpf?.toLowerCase().includes(str) ||
            this.cliente.identidade?.toLowerCase().includes(str) ||
            this.cliente.email?.toLowerCase().includes(str) ||
            this.modelo.nome?.toLowerCase().includes(str) ||
            this.email?.toLowerCase().includes(str);
    }

    getPendenciaResolvida(pendencia: Pendencia): boolean {
        let pendencias = this.pendenciasResolvidas.filter(p => {
            return p.descricao == pendencia.descricao;
        });
        return pendencias.length > 0;
    }

    get modalidadeFormatada(): string {
        let modalidades: Modalidade[] = [
            new Modalidade('Policial Militar', 'pm'),
            new Modalidade('Policial Civil', 'pc'),
            new Modalidade('Policial Penal', 'pp'),
            new Modalidade('Guarda Municipal', 'gm'),
            new Modalidade('Policial Federal', 'pf'),
            new Modalidade('Policial Rodoviário Federal', 'prf'),
            new Modalidade('Bombeiro Militar', 'bm'),
            new Modalidade('CAC', 'cac'),
            new Modalidade('Cidadão', 'cidadao'),
        ];
        return modalidades.filter((f) => {
            return f.id == this.modalidade
        })[0].nome

    }
    get toMap() {
        return {
            id: this.id,
            cliente: this.cliente.asMap,
            fornecedor: this.fornecedor.asMap,
            calibre: this.calibre.asMap,
            acabamento: this.acabamento,
            email: this.email,
            observacao: this.observacao,
            local: this.local,
            data: this.data,
            formaPagamento: this.formaPagamento,
            pendenciasResolvidas: this.pendenciasResolvidas.length >= 0 ? this.pendenciasResolvidas.map(p => { return pendenciaFactory.fromMap(p.descricao) }) : [],
            ultimaAtividade: this.ultimaAtividade,
            arquivos: this.arquivos
        }
    }
}

/**FAZER AS UNFÇÕES FACTORY */
/* export function fromMap(data): Aquisicao {
    return new Aquisicao('', data['descricao'], data['url'], data['path'], data['timestamp'] ?? new Date);
} */

export async function fromFirestore(snapshot: firebase.default.firestore.DocumentSnapshot<unknown> | QueryDocumentSnapshot<unknown>): Promise<Aquisicao> {
    let data = snapshot.data();
    let cliente: Cliente = clienteFactory.fromMap(data['cliente']);
    let fornecedor: Fornecedor = fornecedorFactory.fromMap(data['fornecedor']);
    let calibre: Calibre = calibreFactory.fromMap(data['calibre']);
    let modelo: Modelo = modeloFactory.fromMap(data['modelo']);
    let pendenciasResolvidas: Pendencia[] = [];
    let ultimaPendencia: Pendencia;
    let arquivos: Arquivo[] = [];
    let snapshotArquivos: QuerySnapshot<any> = await snapshot.ref.collection('arquivos').get();
    arquivos = snapshotArquivos.docs.map(s => arquivoFactory.fromFirestore(s));

    if (data['pendenciasResolvidas'] != null && data['pendenciasResolvidas'].length > 0) {
        pendenciasResolvidas = data['pendenciasResolvidas'].map(p => { return pendenciaFactory.fromMap(p) })
        ultimaPendencia = pendenciasResolvidas[pendenciasResolvidas.length - 1]
        //console.log(ultimaPendencia)
    }
    return new Aquisicao(
        snapshot.id,
        cliente,
        modelo,
        fornecedor,
        calibre,
        data['acabamento'],
        data['formaPagamento'],
        data['email'],
        data['observacao'],
        data['local'],
        pendenciasResolvidas,
        ultimaPendencia,
        data['ultimaAtividade'] != null ? data['ultimaAtividade'].toDate() : new Date(1, 1, 1970),
        data['timestamp'].toDate(),
        data['modalidade'] ?? 'pm',
        arquivos
    );
}