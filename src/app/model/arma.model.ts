import { QueryDocumentSnapshot } from '@angular/fire/firestore';
import { FormControl, Validators } from '@angular/forms';
import { Calibre } from './calibre.model';
import { Modelo } from './modelo.model';
import * as calibreFactory from './calibre.model';
import * as modeloFactory from './modelo.model';

export class Arma {
  id: string;
  notaFiscal: string;
  dataNotaFiscal: Date;
  numero: string;
  registroFederal: string;
  expedidor: string;
  uf: string;
  dataEmissao: Date;
  sinarm: string;
  sigma: string;
  pais: string;
  acabamento: string;
  calibre: Calibre;
  comprimentoCano: string;
  modelo: Modelo;
  pessoaJuridica: string;
  cnpj: string;
  isSinarm: boolean;
  isSigma: boolean;
  constructor(
    notaFiscal: string,
    dataNotaFiscal: Date,
    numero: string,
    registroFederal: string,
    expedidor: string,
    uf: string,
    dataEmissao: Date,
    sinarm: string,
    sigma: string,
    pais: string,
    calibre: Calibre,
    comprimentoCano: string,
    modelo: Modelo,
    acabamento: string,
    pessoaJuridica: string,
    cnpj: string,
    isSigma: boolean,
    isSinarm: boolean,
    id?: string
  ) {
    this.notaFiscal = notaFiscal;
    this.dataNotaFiscal = dataNotaFiscal;
    this.numero = numero;
    this.registroFederal = registroFederal;
    this.expedidor = expedidor;
    this.dataEmissao = dataEmissao;
    this.sinarm = sinarm;
    this.sigma = sigma;
    this.pais = pais;
    this.calibre = calibre;
    this.acabamento = acabamento;
    this.modelo = modelo;
    this.cnpj = cnpj;
    this.pessoaJuridica = pessoaJuridica;
    this.isSigma = isSigma;
    this.isSinarm = isSinarm;
    this.id = id ?? '';
    this.comprimentoCano = comprimentoCano;
    this.uf = uf;
  }

  get formGroup() {
    return {
      modelo: new FormControl(this.modelo.id, [Validators.required]),
      modeloNome: new FormControl(this.modelo.nome, [Validators.required]),
      notaFiscal: new FormControl(this.notaFiscal),
      dataNotaFiscal: new FormControl(this.dataNotaFiscal),
      comprimentoCano: new FormControl(
        this.comprimentoCano ?? '',
        Validators.required
      ),
      numero: new FormControl(this.numero, [Validators.required]),
      registroFederal: new FormControl(
        this.registroFederal,
        this.isSinarm ? [Validators.required] : []
      ),
      expedidor: new FormControl(this.expedidor, [Validators.required]),
      uf: new FormControl(this.uf, [Validators.required]),
      dataEmissao: new FormControl(this.dataEmissao, [Validators.required]),
      pessoaJuridica: new FormControl(this.pessoaJuridica),
      cnpj: new FormControl(this.cnpj),
      sinarm: new FormControl(
        this.sinarm,
        this.isSinarm ? [Validators.required] : []
      ),
      sigma: new FormControl(
        this.sigma,
        this.isSigma ? [Validators.required] : []
      ),
      pais: new FormControl(this.pais, [Validators.required]),
      acabamento: new FormControl(this.acabamento, [Validators.required]),
      calibre: new FormControl(this.calibre.id, [Validators.required]),
      isSigma: new FormControl(this.isSigma, [Validators.required]),
      isSinarm: new FormControl(this.isSinarm, [Validators.required]),
    };
  }

  get sinarmFormatado(): string {
    if (!this.sinarm) return '';
    return (
      this.sinarm.substr(0, 4) +
      '/' +
      this.sinarm.substr(4, 9) +
      '-' +
      this.sinarm.substr(13, 2)
    );
  }
  get descricao(): string {
    return this.modelo.descricao + ' ' + this.calibre.nome;
  }
  get asMap() {
    return {
      id: this.id,
      notaFiscal: this.notaFiscal,
      dataNotaFiscal: this.dataNotaFiscal,
      numero: this.numero,
      registroFederal: this.registroFederal,
      expedidor: this.expedidor,
      uf: this.uf,
      dataEmissao: this.dataEmissao,
      sinarm: this.sinarm ?? '',
      sigma: this.sigma ?? '',
      pais: this.pais,
      acabamento: this.acabamento,
      calibre: this.calibre.asMap,
      modelo: this.modelo.asMap,
      pessoaJuridica: this.pessoaJuridica,
      cnpj: this.cnpj,
      isSinarm: this.isSinarm ?? false,
      isSigma: this.isSigma ?? false,
    };
  }

  pesquisa(str: string): boolean {
    str = str.toLowerCase();
    return (
      this.notaFiscal?.toLowerCase().includes(str) ||
      this.registroFederal?.toLowerCase().includes(str) ||
      this.expedidor?.toLowerCase().includes(str) ||
      this.sinarm?.toLowerCase().includes(str) ||
      this.sinarm?.toLowerCase().includes(str) ||
      this.numero?.toLowerCase().includes(str) ||
      this.modelo?.nome.toLowerCase().includes(str) ||
      this.modelo?.especie.nome.toLowerCase().includes(str) ||
      this.modelo?.fabricante.nome.toLowerCase().includes(str) ||
      this.modelo?.funcionamento.toLowerCase().includes(str) ||
      this.acabamento?.toLowerCase().includes(str)
    );
  }
}

export function fromFirestore(
  snapshot:
    | QueryDocumentSnapshot<unknown>
    | firebase.default.firestore.DocumentSnapshot<any>,
  calibre: Calibre,
  modelo: Modelo
) {
  let data = snapshot.data();
  //console.log(data);
  return new Arma(
    data['notaFiscal'],
    data['dataNotaFiscal'] ? data['dataNotaFiscal'].toDate() : '',
    data['numero'],
    data['registroFederal'],
    data['expedidor'],
    data['uf'] ?? '',
    data['dataEmissao']?.toDate(),
    data['sinarm'],
    data['sigma'],
    data['pais'],
    calibre,
    data['comprimentoCano'] ?? '',
    modelo,
    data['acabamento'],
    data['pessoaJuridica'],
    data['cnpj'],
    data['isSigma'],
    data['isSinarm'],
    snapshot.id
  );
}

export function fromMap(map: any) {
  let data = map;
  let calibre: Calibre = calibreFactory.fromMap(data['calibre']);
  let modelo: Modelo = modeloFactory.fromMap(data['modelo']);

  return new Arma(
    data['notaFiscal'],
    data['dataNotaFiscal'] ? data['dataNotaFiscal'].toDate() : null,
    data['numero'],
    data['registroFederal'],
    data['expedidor'],
    data['uf'] ?? '',
    data['dataEmissao']?.toDate(),
    data['sinarm'],
    data['sigma'],
    data['pais'],
    calibre,
    data['comprimentoCano'] ?? '',
    modelo,
    data['acabamento'],
    data['pessoaJuridica'],
    data['cnpj'],
    data['isSigma'],
    data['isSinarm'],
    data['id']
  );
}
