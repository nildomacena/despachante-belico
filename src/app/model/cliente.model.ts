import { PreCadastro } from './preCadastro.model';
import { QueryDocumentSnapshot, QuerySnapshot } from '@angular/fire/firestore';
import { Endereco } from './endereco.model';
import * as enderecoFactory from './endereco.model';
import * as arquivoFactory from './arquivo.model';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Arquivo } from './arquivo.model';
import { Modalidade } from './modalidade.model';

export class Cliente {
  id: string;
  nome: string;
  sexo: string;
  dataNascimento: Date;
  cpf: string;
  identidade: string;
  naturalidade: string;
  uf: string;
  orgaoIdentidade: string;
  titulo: string;
  estadoCivil: string;
  cr: string;
  nomeMae: string;
  nomePai: string;
  profissao: string;
  modalidade: string;
  aposentado: string;
  ordem: string;
  posto: string;
  matricula: string;
  expedicao: Date;
  obm: string;
  endereco1: Endereco;
  endereco2: Endereco;
  telefone: string;
  email: string;
  atividadeApostilada: string;
  arquivos: Arquivo[];
  foto: string;
  observacoes: string; //
  senhaGov: string;
  constructor(
    id: string,
    nome: string,
    sexo: string,
    dataNascimento: Date,
    cpf: string,
    identidade: string,
    naturalidade: string,
    uf: string,
    orgaoIdentidade: string,
    expedicao: Date,
    titulo: string,
    estadoCivil: string,
    cr: string,
    nomeMae: string,
    nomePai: string,
    profissao: string,
    modalidade: string,
    aposentado: string,
    ordem: string,
    posto: string,
    matricula: string,
    obm: string,
    endereco1: Endereco,
    endereco2: Endereco,
    telefone: string,
    email: string,
    atividadeApostilada: string,
    observacoes: string,
    senhaGov: string,
    arquivos: Arquivo[],
    foto: string
  ) {
    this.id = id;
    this.nome = nome;
    this.sexo = sexo;
    this.dataNascimento = dataNascimento;
    this.cpf = cpf;
    this.identidade = identidade;
    this.orgaoIdentidade = orgaoIdentidade;
    this.estadoCivil = estadoCivil;
    this.nome = nome;
    this.nomeMae = nomeMae;
    this.nomePai = nomePai;
    this.profissao = profissao;
    this.aposentado = aposentado;
    this.endereco2 = endereco2;
    this.endereco1 = endereco1;
    this.telefone = telefone;
    this.email = email;
    this.atividadeApostilada = atividadeApostilada;
    this.naturalidade = naturalidade;
    this.arquivos = arquivos;
    this.ordem = ordem;
    this.posto = posto;
    this.matricula = matricula;
    this.obm = obm;
    this.modalidade = modalidade;
    this.foto = foto;
    this.cr = cr;
    this.uf = uf;
    this.expedicao = expedicao;
    this.observacoes = observacoes;
    this.senhaGov = senhaGov;
    this.titulo = titulo;
  }

  pesquisa(str: string): boolean {
    str = str.toLowerCase();
    return (
      this.nome?.toLowerCase().includes(str) ||
      this.cpf?.toLowerCase().includes(str) ||
      this.identidade?.toLowerCase().includes(str) ||
      this.email?.toLowerCase().includes(str)
    );
  }

  get ufIdentidade(): string {
    if (this.modalidade == 'pm')
      return this.orgaoIdentidade
        .substring(this.orgaoIdentidade.length - 2, this.orgaoIdentidade.length)
        .toUpperCase();
    return this.orgaoIdentidade
      .substring(this.orgaoIdentidade.length - 2, this.orgaoIdentidade.length)
      .toUpperCase();
  }
  get cpfFormatado(): string {
    return (
      this.cpf.substring(0, 3) +
      '.' +
      this.cpf.substring(3, 6) +
      '.' +
      this.cpf.substring(6, 9) +
      '-' +
      this.cpf.substring(9, 11)
    );
  }


  get aposentadoAMaisdeCincoAnos(): boolean {
    return this.aposentado == '+5';
  }

  get profissaoFormatada(): string {
    switch (this.profissao) {
      case 'pm':
        return 'Policial Militar';
      case 'pc':
        return 'Policial Civil';
      case 'pp':
        return 'Policial Penal';
      case 'gm':
        return 'Guarda Municipal';
      case 'pf':
        return 'Policial Federal';
      case 'bm':
        return 'Bombeiro Militar';
      case 'prf':
        return 'Policial Rodoviário Federal';
      default:
        return this.profissao;
    }
    return '';
  }

  get identidadeFormatada(): string {
    if (this.modalidade.toLowerCase().includes('bm'))
      return `RGBM: ${this.identidade}`;
    if (this.modalidade.toLowerCase().includes('pm'))
      return `RGPM: ${this.identidade}`;
    /* return this.identidade.toLowerCase().includes('rgpm')
        ? `${this.identidade} PMAL`
        : `RGPM: ${this.identidade} PMAL`; */ else
      return `${this.identidade} ${this.orgaoIdentidade}`;
  }

  get filiacao(): string {
    let filho_a = this.sexo == 'masculino' ? 'filho' : 'filha';
    if (this.nomeMae && this.nomePai) {
      return `${filho_a} de ${this.nomeMae} e ${this.nomePai}`;
    } else if (this.nomeMae) return `${filho_a} de ${this.nomeMae}`;
  }

  get modalidadeFormatada(): string {
    let modalidades: Modalidade[] = [
      new Modalidade('Policial Militar', 'pm'),
      new Modalidade('Policial Civil', 'pc'),
      new Modalidade('Policial Penal', 'pp'),
      new Modalidade('Guarda Municipal', 'gm'),
      new Modalidade('Policial Federal', 'pf'),
      new Modalidade('Policial Rodoviário Federal', 'prf'),
      new Modalidade('Bombeiro Militar', 'bm'),
      new Modalidade('CAC', 'cac'),
      new Modalidade('Cidadão', 'cidadao'),
    ];
    return modalidades.filter((f) => {
      return f.id == this.modalidade;
    })[0].nome;
  }

  get formGroup() {
    console.log(this.aposentado, this.modalidade, this.ordem, this.posto);
    return {
      nome: new FormControl(this.nome, [Validators.required]),
      sexo: new FormControl(this.sexo, [Validators.required]),
      dataNascimento: new FormControl(this.dataNascimento, [
        Validators.required,
      ]),
      cpf: new FormControl(this.cpf, [Validators.required]),
      identidade: new FormControl(this.identidade, [Validators.required]),
      naturalidade: new FormControl(this.naturalidade, [Validators.required]),
      uf: new FormControl(this.uf, [Validators.required]),
      orgaoIdentidade: new FormControl(this.orgaoIdentidade, [
        Validators.required,
      ]),
      expedicao: new FormControl(this.expedicao ?? ''),
      estadoCivil: new FormControl(this.estadoCivil),
      cr: new FormControl(this.cr),
      nomeMae: new FormControl(this.nomeMae),
      nomePai: new FormControl(this.nomePai),
      profissao: new FormControl(this.profissao, [Validators.required]),
      modalidade: new FormControl(this.modalidade, [Validators.required]),
      aposentado: new FormControl(this.aposentado),
      ordem: new FormControl(this.ordem),
      posto: new FormControl(this.posto),
      matricula: new FormControl(this.matricula),
      obm: new FormControl(this.obm),
      logradouro1: new FormControl(this.endereco1.logradouro),
      cep1: new FormControl(this.endereco1.cep),
      bairro1: new FormControl(this.endereco1.bairro),
      complemento1: new FormControl(this.endereco1.complemento),
      numero1: new FormControl(this.endereco1.numero),
      estado1: new FormControl(this.endereco1.estado),
      cidade1: new FormControl(this.endereco1.cidade),
      logradouro2: new FormControl(this.endereco2.logradouro),
      cep2: new FormControl(this.endereco2.cep),
      bairro2: new FormControl(this.endereco2.bairro),
      complemento2: new FormControl(this.endereco2.complemento),
      numero2: new FormControl(this.endereco2.numero),
      estado2: new FormControl(this.endereco2.estado),
      cidade2: new FormControl(this.endereco2.cidade),
      telefone: new FormControl(this.telefoneFormatado, [Validators.required]),
      email: new FormControl(this.email, [Validators.required]),
      atividadeApostilada: new FormControl(this.atividadeApostilada),
      observacoes: new FormControl(this.observacoes),
      senhaGov: new FormControl(this.senhaGov),
      titulo: new FormControl(this.titulo),
    };
  }
  get telefoneFormatado(): string {
    if (this.telefone.includes('(') && this.telefone.includes(')'))
      return this.telefone;
    if (this.telefone.length == 10)
      return `(${this.telefone.substr(0, 2)}) ${this.telefone.substr(
        2,
        4
      )}- ${this.telefone.substr(6, 4)}`;
    return `(${this.telefone.substr(0, 2)})${this.telefone.substr(
      2,
      5
    )}-${this.telefone.substr(7, 4)}`;
  }

  get dataNascimentoFormatada(): string {
    return `${this.dataNascimento.getUTCDate()}/${
      this.dataNascimento.getUTCMonth() + 1 < 10
        ? '0' + (this.dataNascimento.getUTCMonth() + 1).toFixed(0)
        : this.dataNascimento.getUTCMonth() + 1
    }/${this.dataNascimento.getFullYear()}`;
  }

  get endereco1Formatado(): string {
    console.log(this.endereco1.cidade + '/' + this.endereco1.estado);
    return `${this.endereco1.logradouro}, ${this.endereco1.numero}, ${
      this.endereco1.bairro
    }, ${this.endereco1.cidade + '/' + this.endereco1.estado}`;
  }
  get asMap() {
    return {
      id: this.id,
      nome: this.nome,
      sexo: this.sexo,
      dataNascimento: this.dataNascimento,
      cpf: this.cpf,
      identidade: this.identidade,
      naturalidade: this.naturalidade,
      orgaoIdentidade: this.orgaoIdentidade,
      estadoCivil: this.estadoCivil,
      nomeMae: this.nomeMae,
      nomePai: this.nomePai,
      profissao: this.profissao,
      modalidade: this.modalidade,
      aposentado: this.aposentado ?? '',
      ordem: this.ordem ?? '',
      posto: this.posto ?? '',
      telefone: this.telefone,
      email: this.email,
      atividadeApostilada: this.atividadeApostilada,
      observacoes: this.observacoes,
      titulo: this.titulo,
    };
  }
}

export function fromMap(data: any) {
  return new Cliente(
    data['id'],
    data['nome'],
    data['sexo'],
    data['dataNascimento'].toDate(),
    data['cpf'],
    data['identidade'],
    data['naturalidade'],
    data['uf'] ?? '',
    data['orgaoIdentidade'],
    data['expedicao'],
    data['titulo'],
    data['estadoCivil'],
    data['cr'] ?? '',
    data['nomeMae'],
    data['nomePai'],
    data['profissao'],
    data['modalidade'] ?? 'pm',
    data['aposentado'] ?? 'nao',
    data['ordem'],
    data['posto'],
    data['matricula'],
    data['obm'],
    null,
    null,
    data['telefone'],
    data['email'],
    data['atividadeApostilada'],
    data['observacoes'],
    data['senhaGov'] ?? '',
    [],
    data['foto']
  );
}

export async function fromFirestore(snapshot: QueryDocumentSnapshot<unknown>) {
  let data = snapshot.data();
  let endereco1: Endereco = enderecoFactory.fromMap(data['endereco1']);
  let endereco2: Endereco = enderecoFactory.fromMap(data['endereco2']);
  let arquivos: Arquivo[] = [];
  let snapshotArquivos: QuerySnapshot<any> = await snapshot.ref
    .collection('arquivos')
    .get();
  arquivos = snapshotArquivos.docs.map((s) => arquivoFactory.fromFirestore(s));
  /* if (data['arquivos'] && data['arquivos'].length > 0) {
        arquivos = data['arquivos'].map(a => arquivoFactory.fromMap(a));
    } */
  try {
    return new Cliente(
      snapshot.id,
      data['nome'],
      data['sexo'],
      data['dataNascimento']?.toDate(),
      data['cpf'],
      data['identidade'],
      data['naturalidade'],
      data['uf'] ?? '',
      data['orgaoIdentidade'],
      data['expedicao'] ? data['expedicao']?.toDate() : '',
      data['titulo'] ?? '',
      data['estadoCivil'],
      data['cr'] ?? '',
      data['nomeMae'],
      data['nomePai'],
      data['profissao'],
      data['modalidade'],
      data['aposentado'] ?? 'nao',
      data['ordem'],
      data['posto'],
      data['matricula'],
      data['obm'],
      endereco1,
      endereco2,
      data['telefone'],
      data['email'],
      data['atividadeApostilada'],
      data['observacoes'] ?? '',
      data['senhaGov'] ?? '',
      arquivos ?? [],
      data['foto']
    );
  } catch (error) {
    console.log(snapshot.data());
    console.error(`Erro no cliente ${data['nome']} - ${snapshot.id}`, error);
  }
}
