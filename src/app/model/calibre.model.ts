import {
  DocumentChangeAction,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';

export class Calibre {
  id: string;
  nome: string;

  constructor(nome: string, id?: string) {
    this.nome = nome;
    this.id = id ?? '';
  }

  get asMap() {
    return {
      id: this.id,
      nome: this.nome,
    };
  }
}

export function fromMap(data: any) {
  return new Calibre(data['nome'], data['id']);
}

export function fromFirestore(snapshot: QueryDocumentSnapshot<unknown>) {
  let data = snapshot.data();
  return new Calibre(data['nome'], snapshot.id);
}
export function fromSnapshotFirestore(snapshot: DocumentChangeAction<unknown>) {
  let data = snapshot.payload.doc.data();
  return new Calibre(data['nome'], snapshot.payload.doc.id);
}
