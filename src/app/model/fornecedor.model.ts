import { QueryDocumentSnapshot } from '@angular/fire/firestore';

export class Fornecedor {
    id: string
    nome: string;
    cnpj: string;
    cr: string;
    endereco: string;
    cep: string;
    telefone: string;
    constructor(
        nome: string,
        cnpj: string,
        cr: string,
        endereco: string,
        cep: string,
        telefone: string,
        id: string) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.cr = cr;
        this.endereco = endereco;
        this.cep = cep;
        this.telefone = telefone;
        this.id = id ?? '';
    }

    get cnpjFormatado(): string {
        return this.cnpj.substr(0, 2) + '.' + this.cnpj.substr(2, 3) + '.' + this.cnpj.substr(5, 3) + '/' + this.cnpj.substr(8, 4) + '-' + this.cnpj.substr(12, 2);
    }

    get telefoneFormatado(): string {
        if (this.telefone.length == 10)
            return `(${this.telefone.substr(0, 2)}) ${this.telefone.substr(2, 4)}- ${this.telefone.substr(6, 4)}`;
        return `(${this.telefone.substr(0, 2)})${this.telefone.substr(2, 5)}-${this.telefone.substr(7, 4)}`;
    }

    get descricaoCompleta(): string {
        return `${this.nome} CR: ${this.cr}. Endereço completo: ${this.endereco}, CEP: ${this.cep} - CNPJ: ${this.cnpjFormatado}. Telefone: ${this.telefoneFormatado}`
    }

    get asMap() {
        return {
            'id': this.id,
            'cnpj': this.cnpj,
            'nome': this.nome,
            'cr': this.cr,
            'endereco': this.endereco,
            'cep': this.cep,
            'telefone': this.telefone
        }
    }
}

export function fromMap(data: any) {
    return new Fornecedor(
        data['nome'],
        data['cnpj'],
        data['cr'],
        data['endereco'],
        data['cep'],
        data['telefone'],
        data['id']);
}

export function fromFirestore(snapshot: QueryDocumentSnapshot<unknown>) {
    let data = snapshot.data();
    return new Fornecedor(
        data['nome'],
        data['cnpj'],
        data['cr'],
        data['endereco'],
        data['cep'],
        data['telefone'],
        snapshot.id);
}