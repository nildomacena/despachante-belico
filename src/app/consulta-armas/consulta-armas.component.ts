import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Arma } from '../model/arma.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-consulta-armas',
  templateUrl: './consulta-armas.component.html',
  styleUrls: ['./consulta-armas.component.scss'],
})
export class ConsultaArmasComponent implements OnInit {
  armas: Arma[] = [];
  armasFiltradas: Arma[] = [];
  pesquisa: string;
  salvando: boolean;
  constructor(
    private fireService: FireService,
    private utilService: UtilService,
    private router: Router
  ) {
    this.fireService.getArmas().then((armas) => {
      this.armas = this.armasFiltradas = armas;
      console.log(this.armas);
    });
  }

  ngOnInit(): void {}

  limparFiltros() {
    this.pesquisa = '';
    this.armasFiltradas = this.armas;
  }

  onSearch() {
    if (this.pesquisa.length <= 0) {
      this.armasFiltradas = this.armas;
      return;
    } else {
      this.armasFiltradas = this.armas.filter((a) => a.pesquisa(this.pesquisa));
    }
  }

  onSelectArma(arma: Arma) {
    console.log('Arma: ', arma.id);
    this.router.navigateByUrl('cadastrar-arma', { state: { arma: arma } });
    return;
  }
}
