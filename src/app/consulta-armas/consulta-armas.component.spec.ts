import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaArmasComponent } from './consulta-armas.component';

describe('ConsultaArmasComponent', () => {
  let component: ConsultaArmasComponent;
  let fixture: ComponentFixture<ConsultaArmasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaArmasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaArmasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
