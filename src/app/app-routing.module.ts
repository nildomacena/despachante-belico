import { TesteComponent } from './teste/teste.component';
import { LoginComponent } from './login/login.component';
import { TransferenciaDetailComponent } from './transferencia-detail/transferencia-detail.component';
import { ListaTransferenciaComponent } from './lista-transferencia/lista-transferencia.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroArmaComponent } from './cadastro-arma/cadastro-arma.component';
import { CadastroAtiradorComponent } from './cadastro-cliente/cadastro-cliente.component';
import { CadastroFornecedorComponent } from './fornecedor/fornecedor.component';
import { CalibresComponent } from './calibres/calibres.component';
import { ConsultaArmasComponent } from './consulta-armas/consulta-armas.component';
import { ConsultaAtiradoresComponent } from './consulta-clientes/consulta-clientes.component';
import { EspeciesComponent } from './especies/especies.component';
import { FabricantesComponent } from './fabricantes/fabricantes.component';
import { ModelosComponent } from './modelos/modelos.component';
import { AquisicaoArmaComponent } from './cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component';
import { AquisicaoDetailComponent } from './aquisicao-detail/aquisicao-detail.component';
import { ListaAquisicaoComponent } from './lista-aquisicao/lista-aquisicao.component';
import { CadastrarTransferenciaComponent } from './cadastrar-transferencia/cadastrar-transferencia.component';
import { PreCadastroComponent } from './pre-cadastro/pre-cadastro.component';
import {
  AngularFireAuthGuard,
  hasCustomClaim,
  redirectUnauthorizedTo,
  redirectLoggedInTo,
} from '@angular/fire/auth-guard';
import { ListaPreCadastrosComponent } from './lista-pre-cadastros/lista-pre-cadastros.component';
import { AniversariantesComponent } from './aniversariantes/aniversariantes.component';
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLogged = () => redirectLoggedInTo(['home']);

const routes: Routes = [
  {
    path: '',
    component: CadastroAtiradorComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { authGuardPipe: redirectLogged },
  },
  {
    path: 'home',
    component: CadastroAtiradorComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'aniversariantes',
    component: AniversariantesComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'fabricantes',
    component: FabricantesComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'fornecedores',
    component: CadastroFornecedorComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'especies',
    component: EspeciesComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'calibres',
    component: CalibresComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'modelos',
    component: ModelosComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'cadastrar-arma',
    component: CadastroArmaComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'consulta-armas',
    component: ConsultaArmasComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'cadastro-cliente',
    component: CadastroAtiradorComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'consulta-clientes',
    component: ConsultaAtiradoresComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'aquisicao-arma',
    component: AquisicaoArmaComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'aquisicao-detail',
    component: AquisicaoDetailComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'consulta-aquisicao',
    component: ListaAquisicaoComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'cadastrar-transferencia',
    component: CadastrarTransferenciaComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'listar-transferencias',
    component: ListaTransferenciaComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'teste',
    component: TesteComponent,
  },
  {
    path: 'transferencia-detail',
    component: TransferenciaDetailComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'lista-precadastros',
    component: ListaPreCadastrosComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'pre-cadastro',
    component: PreCadastroComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
