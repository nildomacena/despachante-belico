import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaAtiradoresComponent } from './consulta-clientes.component';

describe('ConsultaAtiradoresComponent', () => {
  let component: ConsultaAtiradoresComponent;
  let fixture: ComponentFixture<ConsultaAtiradoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaAtiradoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaAtiradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
