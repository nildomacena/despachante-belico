import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cliente } from '../model/cliente.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-consulta-clientes',
  templateUrl: './consulta-clientes.component.html',
  styleUrls: ['./consulta-clientes.component.scss']
})
export class ConsultaAtiradoresComponent implements OnInit {
  clientes: Cliente[] = [];
  atiradoresFiltrados: Cliente[] = [];
  pesquisa: string;
  atiradorSelecionado: Cliente;
  salvando: boolean;
  constructor(private fireService: FireService, private formBuilder: FormBuilder, private utilService: UtilService, private router: Router) {
    this.fireService.getClientes().then(clientes => {
      this.clientes = this.atiradoresFiltrados = clientes;
    });
  }

  ngOnInit(): void {
  }


  limparFiltros() {
    this.pesquisa = '';
    this.atiradoresFiltrados = this.clientes;
  }

  onSearch() {
    console.log(this.pesquisa)
    if (this.pesquisa.length <= 0) {
      this.atiradoresFiltrados = this.clientes;
    }
    else {
      console.log(this.clientes);
      this.atiradoresFiltrados = this.clientes.filter(a => a.pesquisa(this.pesquisa));

    }
  }

  onSelectAtirador(cliente) {
    if (this.atiradorSelecionado == cliente) {
      this.atiradorSelecionado = null;
      return;
    }
    else {
      this.atiradorSelecionado = cliente;
      return;
    }

  }

  irParaAtirador(cliente: Cliente) {
    this.router.navigateByUrl('cadastro-cliente', { state: { cliente: cliente } });
  }

  consolee() {
  }

}
