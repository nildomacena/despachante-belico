import { UtilService } from './../services/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  formSignup: FormGroup;
  signup: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private utilService: UtilService
  ) {
    this.formLogin = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required],
    });

    this.formSignup = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required],
      confirmaSenha: ['', Validators.required],
      codigo: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  goToHome() {
    this.router.navigate(['/home']);
  }
  login() {
    this.authService.login('', '');
    this.router.navigate(['/home']);
  }

  toggleSignup() {
    this.signup = !this.signup;
  }

  async onLogin() {
    if (this.formLogin.invalid) {
      this.utilService.toastrErro('Formulário inválido');
      return;
    }
    try {
      console.log('onLogin')
      let data = this.formLogin.value;
      let user = await this.authService.login(data['email'], data['senha']);
      console.log(user);
      this.router.navigate([''])
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro('Erro', error.code);
    }
  }

  async onSignup() {
    if (this.formSignup.invalid) {
      this.utilService.toastrErro('Formulário inválido');
      return;
    }
    try {
      let data = this.formSignup.value;
      await this.authService.criarUsuario(
        data['email'],
        data['senha'],
        data['codigo']
      );
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro('Formulário inválido', error.code);
    }
  }
}
