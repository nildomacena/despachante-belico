import { TDocumentDefinitions } from 'pdfmake/interfaces';
import { Cliente } from './../model/cliente.model';
import { PdfMakeWrapper, Txt } from 'pdfmake-wrapper';
import { UtilService } from './../services/util.service';
import { Injectable } from '@angular/core';
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

(<any>pdfmake).vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root',
})
export class CadastroClienteService {
  constructor(private utilService: UtilService) {}

  gerarDeclaracaoEnderecoGuardaAcervo(cliente: Cliente, opcao: string) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE ENDEREÇO DE GUARDA DO ACERVO',
          fontSize: 16,
          alignment: 'center',
          margin: [0, 10, 0, 0],
        },
        {
          text: `Eu, ${cliente.nome}, ${
            cliente.sexo == 'feminino' ? 'brasileira' : 'brasileiro'
          }, natural de ${cliente.naturalidade}/${cliente.uf}, nascido em ${
            cliente.dataNascimentoFormatada
          }, ${cliente.profissaoFormatada}, ${
            cliente.estadoCivil
          }, residência no (a) ${cliente.endereco1Formatado} e CPF nº ${
            cliente.cpfFormatado
          }, telefone ${cliente.telefoneFormatado}, E-mail ${
            cliente.email
          },`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 60, 30, 0],
        },
        {
          text: `DECLARO, para fim de ${opcao}, que o endereço de guarda do meu acervo de atirador desportivo está situado na ${cliente.endereco1Formatado}, CEP ${cliente.endereco1.cep}`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 0],
        },
        /* {
          text: `DECLARO ainda que o endereço da residência fixa é o mesmo da guarda do acervo.`,
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 30],
        }, */
        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 10, 30, 30],
        },
        {
          text: `__________________________________\n${cliente.nome}\nCPF Nº ${cliente.cpfFormatado}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 10, 30, 30],
        },
      ],
    };
    pdfmake.createPdf(pdf).download('Declaracao Endereco Guarda Acervo');
  }

  gerarDeclaracaoInquerito(cliente: Cliente) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇAO DE INEXISTÊNCIA DE INQUÉRITOS POLICIAIS OU PROCESSOS CRIMINAIS',
          fontSize: 16,
          alignment: 'center',
          margin: [0, 10, 0, 0],
        },
        {
          text: `Eu, ${cliente.nome}, ${
            cliente.sexo == 'feminino' ? 'brasileira' : 'brasileiro'
          }, natural de ${cliente.naturalidade}/${cliente.uf}, nascido em ${
            cliente.dataNascimentoFormatada
          }, ${cliente.profissaoFormatada}, ${
            cliente.estadoCivil
          }, residência no (a) ${cliente.endereco1Formatado} e CPF nº ${
            cliente.cpfFormatado
          }, telefone ${cliente.telefoneFormatado}, E-mail ${
            cliente.email
          },`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 60, 30, 0],
        },
        {
          text: 'DECLARO, sob penas a lei, que possuo bons antecedentes e idoneidade moral, e não respondo processo criminal, nem inquérito policial tanto no estado de domicílio quanto nos demais entes federativos e estou ciente que, em caso de falsidade ideológica, ficarei sujeito as sanções prescritas no Código Penal e as demais cominações legais aplicáveis. Por ser verdade, assino ao final.'.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 0, 30, 0],
        },
        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 40, 30, 30],
        },
        {
          text: `__________________________________\n${cliente.nome}\nCPF Nº ${cliente.cpfFormatado}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 10, 30, 30],
        },
      ],
    };
    pdfmake.createPdf(pdf).download('Declaracao Inquérito Policial CAC');
  }

  gerarDeclaracaoSegurancaAcervo(cliente: Cliente, opcao: string) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE SEGURANÇA DO ACERVO',
          fontSize: 16,
          alignment: 'center',
          margin: [0, 10, 0, 0],
        },
        {
          text: `Eu, ${cliente.nome}, ${
            cliente.sexo == 'feminino' ? 'brasileira' : 'brasileiro'
          }, natural de ${cliente.naturalidade}/${cliente.uf}, nascido em ${
            cliente.dataNascimentoFormatada
          }, ${cliente.profissaoFormatada}, ${
            cliente.estadoCivil
          }, residência no (a) ${cliente.endereco1Formatado} e CPF nº ${
            cliente.cpfFormatado
          }, telefone ${cliente.telefoneFormatado}, E-mail ${cliente.email},`,
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 60, 30, 0],
        },
        {
          text: `DECLARO, para fim de ${opcao}, que o local de guarda do meu acervo de Tiro Desportivo - Atirador Desportivo, Colecionamento - Colecionador atende as condições de segurança previstas no anexo F da portaria n° 150-COLOG de 5 de Dezembro de 2019.`,
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 0, 30, 0],
        },
        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 40, 30, 30],
        },
        {
          text: `__________________________________\n${cliente.nome}\nCPF Nº ${cliente.cpfFormatado}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 10, 30, 30],
        },
      ],
    };
    pdfmake.createPdf(pdf).download('Declaracao segurança do acervo');
  }

  gerarDeclaracaoGuardaAcervo(cliente: Cliente) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DECLARAÇÃO DE SEGURANÇA DO ACERVO',
          fontSize: 16,
          bold: true,
          alignment: 'center',
          margin: [0, 10, 0, 0],
        },
        {
          text: `Eu, ${cliente.nome}, ${
            cliente.sexo == 'feminino' ? 'brasileira' : 'brasileiro'
          }, natural de ${cliente.naturalidade}/${cliente.uf}, nascido em ${
            cliente.dataNascimentoFormatada
          }, ${cliente.profissaoFormatada}, ${
            cliente.estadoCivil
          }, residência no (a) ${cliente.endereco1Formatado} e CPF nº ${
            cliente.cpfFormatado
          }, telefone ${cliente.telefoneFormatado}, E-mail ${cliente.email},`,
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 60, 30, 0],
        },
        {
          text: `DECLARO, para fim de Concessão de Registro no Exército Brasileiro, que o local de guarda do meu acervo de ATIRADOR DESPORTIVO, COLECIONADOR, atende as condições de segurança previstas no anexo F da portaria n° 150-COLOG/2019.`,
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 20, 30, 0],
        },
        {
          text: `DECLARO, ainda, que o endereço da residência fixa é o mesmo da guarda do acervo.`,
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 20, 30, 0],
        },
        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 40, 30, 30],
        },
        {
          text: `__________________________________\n${cliente.nome}\nCPF Nº${cliente.cpfFormatado}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 10, 30, 30],
        },
      ],
    };
    pdfmake.createPdf(pdf).download('Declaracao Segurança Acervo');
  }

  gerarDeclaracaoModalidade(cliente: Cliente) {
    let pdf: TDocumentDefinitions = {
      content: [
        {
          text: 'DISPENSA DE DECLARAÇÃO DE MODALIDADE E PROVA',
          fontSize: 16,
          bold: true,
          alignment: 'center',
          margin: [0, 10, 0, 0],
        },
        {
          text: `Eu ${cliente.nome}, portador do CPF: nº ${cliente.cpfFormatado}, Certificado de Registro: ${cliente.cr}, venho por meio desta solicitar a dispensa da apresentação de modalidade e prova, visto que de acordo com o Colog 136 Declaração de Modalidade e Prova são para armas de calibre restrito`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.5,
          margin: [30, 40, 30, 0],
        },
        {
          text: `Art. 8ºA aquisição de arma de fogo de uso restrito por colecionadores, atiradores desportivos e caçadores, dar-se-á da seguinte forma:`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 20, 30, 0],
        },
        {
          text: `I - autorização para a aquisição e tratativas da compra:`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 0],
        },
        {
          text: `a) a autorização está condicionada ao atendimento do prescrito nos art. 9º ao art. 12 desta portaria e será formalizada pelo despacho da Organização Militar do SisFPC de vinculação do colecionador, atirador desportivo ou caçador, no próprio requerimento (anexo E).`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 0],
        },
        {
          text: `b) o requerimento de que trata a alínea “a” deverá ser instruído com o comprovante da taxa de aquisição de PCE.`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 0],
        },
        {
          text: `c) no caso de tiro desportivo, é necessária a comprovação de que a arma pleiteada está prevista nas regras de prática, nacionais ou internacionais, da modalidade de tiro indicada pelo adquirente,`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 0],
        },
        {
          text: `d) a comprovação de que trata a alínea "c" é feita pela declaração do próprio atirador, conforme o anexo E.`.toUpperCase(),
          alignment: 'justify',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 5, 30, 0],
        },
        {
          text: `Maceió/AL, ${this.utilService.dataAtualPorExtenso()}`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.5,
          margin: [30, 40, 30, 30],
        },
        {
          text: `__________________________________\n${cliente.nome.toUpperCase()}\nCPF Nº ${
            cliente.cpfFormatado
          }`,
          alignment: 'center',
          fontSize: 12,
          lineHeight: 1.6,
          margin: [30, 10, 30, 30],
        },
      ],
    };
    pdfmake.createPdf(pdf).download('Dispensa de declaração');
  }
}
