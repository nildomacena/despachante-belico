import { CadastroClienteService } from './cadastro-cliente.service';
import { clienteMock } from './mocks/cliente-mock';
import { PdfService } from './../services/pdf.service';
import { ClipboardService } from 'ngx-clipboard';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { FireService } from '../services/fire.service';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { listLocales } from 'ngx-bootstrap/chronos';
import { Profissao } from '../model/profissao.model';
import { UtilService } from '../services/util.service';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { Endereco } from '../model/endereco.model';
import { Cliente } from '../model/cliente.model';
import bsCustomFileInput from 'bs-custom-file-input';
import { Arquivo } from '../model/arquivo.model';
import { Modalidade } from '../model/modalidade.model';
import { GenericValidator } from '../services/cpf.validator';

defineLocale('pt-br', ptBrLocale);
declare var jQuery: any;

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.scss'],
})
export class CadastroAtiradorComponent implements OnInit {
  formCliente: FormGroup;
  profissoes: Profissao[];
  foto: File;
  comprovante: File;
  salvando: boolean;
  locales = listLocales();
  clienteNav: Cliente;
  descricaoArquivo: string;
  arquivo: File;
  @ViewChild('labelImport')
  labelImport: ElementRef;
  isMilitar: boolean;
  modalidades: Modalidade[];
  avatar: File;
  urlFoto: string;
  mascaraTelefone: string = '(00)0000-0000||(00)00000-0000';
  formResidencia: FormGroup;
  orgaoResidencia: string = 'pf';
  modoteste = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private fireService: FireService,
    private toastr: ToastrService,
    private utilService: UtilService,
    private http: HttpClient,
    private localeService: BsLocaleService,
    private clipboardService: ClipboardService,
    private pdfService: PdfService,
    private clienteService: CadastroClienteService
  ) {
    this.formCliente = this.formBuilder.group({
      nome: new FormControl('', [Validators.required]),
      sexo: new FormControl('', [Validators.required]),
      dataNascimento: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [
        Validators.required,
        GenericValidator.isValidCpf(),
      ]),
      identidade: new FormControl('', [Validators.required]),
      naturalidade: new FormControl('', [Validators.required]),
      uf: new FormControl('', [Validators.required]),
      orgaoIdentidade: new FormControl('', [Validators.required]),
      expedicao: new FormControl(''),
      estadoCivil: new FormControl('', [Validators.required]),
      cr: new FormControl(''),
      nomeMae: new FormControl(''),
      nomePai: new FormControl(''),
      profissao: new FormControl('', [Validators.required]),
      aposentado: new FormControl(''),
      ordem: new FormControl(''),
      titulo: new FormControl(''),
      posto: new FormControl(''),
      matricula: new FormControl(''),
      obm: new FormControl(''),
      logradouro1: new FormControl(''),
      cep1: new FormControl(''),
      bairro1: new FormControl(''),
      complemento1: new FormControl(''),
      numero1: new FormControl(''),
      estado1: new FormControl(''),
      cidade1: new FormControl(''),
      logradouro2: new FormControl(''),
      cep2: new FormControl(''),
      bairro2: new FormControl(''),
      complemento2: new FormControl(''),
      numero2: new FormControl(''),
      estado2: new FormControl(''),
      cidade2: new FormControl(''),
      telefone: new FormControl('', [Validators.required]),
      email: new FormControl('miro.macena@gmail.com', [Validators.required]),
      observacoes: new FormControl(''),
      senhaGov: new FormControl(''),
      atividadeApostilada: new FormControl(''),
      modalidade: new FormControl('', [Validators.required]),
    });
    this.formResidencia = this.formBuilder.group({
      nome: new FormControl('', [Validators.required]),
      nacionalidade: new FormControl('brasileiro', [Validators.required]),
      naturalidade: new FormControl('', [Validators.required]),
      identidade: new FormControl('', [Validators.required]),
      orgaoIdentidade: new FormControl('', [Validators.required]),
      parentesco: new FormControl(''),
      cpf: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required]),
    });
    this.modalidades = this.utilService.modalidades;
    this.profissoes = this.utilService.profissoes;

    if (this.router.getCurrentNavigation()?.extras?.state) {
      this.updateForm(this.router.getCurrentNavigation().extras.state.cliente);
      //this.formCliente.patchValue(this.clienteNav.formGroup)
    }

    this.formCliente.controls['modalidade'].valueChanges.subscribe((value) => {
      console.log(value);
      if (value != 'cac' && value != 'cidadao') {
        this.formCliente.controls['profissao'].patchValue(
          this.utilService.getModalidadeById(value).nome
        );
        this.isMilitar = false;
      } else {
        this.formCliente.controls['profissao'].patchValue('');
      }
    });
  }

  updateForm(cliente: Cliente) {
    this.clienteNav = cliente;
    this.formCliente = this.formBuilder.group(this.clienteNav.formGroup);
    if (this.clienteNav.telefone.length > 10)
      this.mascaraTelefone = '(00)00000-0000';
    console.log(this.clienteNav, this.formCliente.value);
    if (this.clienteNav.foto) this.urlFoto = this.clienteNav.foto;
  }

  toggleOrgaoResidencia(orgao: string) {
    this.orgaoResidencia = orgao;
  }

  gerarDeclaracoes() {
    /* this.pdfService.declaracaoEfetivaNecessidade(this.clienteNav);
    this.pdfService.declaracaoLocalSeguro(this.clienteNav);
    this.pdfService.declaracaoProcesso(this.clienteNav); */
    this.pdfService.declaracoesConjuntas(this.clienteNav);
  }

  ngOnInit(): void {
    bsCustomFileInput.init();

    this.localeService.use('pt-br');
    console.log(this.locales);
    this.formCliente.controls['cep1'].valueChanges.subscribe(async (cep) => {
      if (cep.length == 8) {
        let result = await this.http
          .get(`https://viacep.com.br/ws/${cep}/json/`)
          .pipe(first())
          .toPromise();
        console.log(result);
      }
    });

    this.formCliente.controls['profissao'].valueChanges.subscribe((value) => {
      console.log(value);
      this.isMilitar = value == 'bm' || value == 'pm';
    });
  }

  abrirModalResidenciaTerceiros() {
    if (!this.clienteNav) {
      this.utilService.toastrErro('Erro', 'Usuário não encontrado');
    }
    console.log('abrirModalResidenciaTerceiros');
    this.formResidencia.controls['endereco'].setValue(
      this.clienteNav.endereco1Formatado
    );
    jQuery('#residenciaModal').modal('show');
  }

  async gerarDeclaracaoTerceiros() {
    this.salvando = true;
    if (this.formResidencia.invalid) {
      this.utilService.toastrErro(
        'Informações incompletas',
        'Confira os dados e tente novamente'
      );
      return;
    }
    try {
      let data = this.formResidencia.value;
      this.pdfService.gerarDeclaracaoResidencia(
        this.clienteNav,
        data['nome'],
        data['endereco'],
        data['nacionalidade'],
        data['naturalidade'],
        data['parentesco'],
        data['identidade'],
        data['orgaoIdentidade'],
        data['cpf'],
        this.orgaoResidencia
      );
    } catch (error) {
      this.utilService.toastrErro();
      console.error(error);
    }
    console.log(this.formResidencia);
    this.salvando = false;
  }

  resetForm() {
    this.formResidencia.reset();
  }
  abrirModalProcuracao() {
    jQuery('#outorgadoModal').modal('show');
  }

  async gerarProcuracao(outorgado: string) {
    this.salvando = true;
    this.pdfService.gerarProcuracao(this.clienteNav, outorgado);
    this.salvando = false;
  }

  downloadArquivo(arquivo: Arquivo) {
    window.open(arquivo.url, 'blank');
  }

  async excluirArquivo(arquivo: Arquivo) {
    if (!confirm(`Deseja realmente excluir o arquivo ${arquivo.descricao}?`)) {
      return;
    }
    this.salvando = true;
    try {
      this.clienteNav = await this.fireService.excluirArquivo(
        this.clienteNav,
        arquivo
      );
      this.utilService.toastrSucesso();
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro();
    }
    this.salvando = false;
  }

  onFileChange(event) {
    this.arquivo = event.target.files[0];
    this.labelImport.nativeElement.innerText = this.arquivo.name;
    console.log(this.arquivo);
  }

  onImagemChange(event) {
    this.foto = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.urlFoto = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onComprovanteChange(event) {
    this.comprovante = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        // this.urlFoto = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  consolee() {
    this.updateForm(clienteMock);
  }

  async checaCPF(alterandoCliente: boolean): Promise<boolean> {
    if (this.clienteNav) return;
    if (
      this.formCliente.controls['cpf'].invalid &&
      this.formCliente.controls['cpf'].value
    ) {
      alert('CPF Inválido.\nVerifique as informações!');
      jQuery('#cpf').focus();
    } else if (
      !alterandoCliente &&
      !(await this.fireService.checaBDCpf(
        this.formCliente.controls['cpf'].value
      ))
    ) {
      alert('CPF já cadastrado.\nVerifique as informações!');
      jQuery('#cpf').focus();
      return true;
    }
    console.log(this.formCliente.controls['cpf']);
    return this.formCliente.controls['cpf'].invalid;
  }

  onAvatarChange(event) {
    this.foto = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.urlFoto = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  async buscaCEP() {
    console.log(this.formCliente.controls['cep1']);
    if (this.formCliente.controls['cep1'].invalid) {
      return;
    }
    let url = `https://viacep.com.br/ws/${this.formCliente.value['cep1']}/json`;
    let response = await this.http.get(url).pipe(first()).toPromise();
    this.formCliente.controls['logradouro1'].patchValue(response['logradouro']);
    this.formCliente.controls['bairro1'].patchValue(response['bairro']);
    this.formCliente.controls['cidade1'].patchValue(response['localidade']);
    this.formCliente.controls['estado1'].patchValue(response['uf']);
    console.log(response);
  }

  copyText(campo: string) {
    if (!this.clienteNav) return;
    if (campo == 'endereco') {
      this.clipboardService.copy(this.clienteNav.endereco1Formatado);
      this.utilService.toastrSucesso('Copiado', 'Endereço Copiado');
    }
    this.clipboardService.copy(this.formCliente.value[campo]);
    this.utilService.toastrSucesso('Copiado', '');
  }

  async cadastrarCliente() {
    if (await this.checaCPF(false)) {
      //Checa se o cpf é inválido
      return;
    }
    if (await this.formCliente.invalid) {
      this.utilService.toastrErro('Erro', 'Preencha todas as informações');
      return;
    }
    this.salvando = true;
    let data = this.formCliente.value;
    let endereco1 = new Endereco(
      data['logradouro1'],
      data['cep1'],
      data['bairro1'],
      data['complemento1'],
      data['numero1'],
      data['cidade1'],
      data['estado1']
    );
    let endereco2 = new Endereco(
      data['logradouro2'],
      data['cep2'],
      data['bairro2'],
      data['complemento2'],
      data['numero2'],
      data['cidade2'],
      data['estado2']
    );
    console.log(this.formCliente);
    try {
      this.clienteNav = await this.fireService.cadastrarCliente(
        data['nome'],
        data['sexo'],
        data['dataNascimento'],
        data['cpf'],
        data['identidade'],
        data['naturalidade'],
        data['uf'],
        data['orgaoIdentidade'],
        data['expedicao'],
        data['titulo'],
        data['estadoCivil'],
        data['cr'],
        data['nomeMae'],
        data['nomePai'],
        data['profissao'],
        data['modalidade'],
        data['aposentado'] ?? 'nao',
        data['ordem'],
        data['posto'],
        data['matricula'],
        data['obm'],
        endereco1,
        endereco2,
        data['telefone'],
        data['email'],
        data['atividadeApostilada'],
        data['observacoes'],
        data['senhaGov'],
        this.foto
      );
      this.utilService.toastrSucesso();
      this.abrirModalAcao();
    } catch (error) {
      if (error == 'cpf-ja-cadastrado')
        this.utilService.toastrErro('Esse CPF já está cadastrado', 'Erro');
      else this.utilService.toastrErro('Preencha todas as informações', 'Erro');
      console.error(error);
    }
    this.salvando = false;
  }

  async alterarCliente() {
    if (await this.checaCPF(true)) {
      //Checa se o cpf é inválido
      return;
    }
    if (this.formCliente.invalid) {
      this.utilService.toastrErro('Erro', 'Preencha todas as informações');
      return;
    }
    this.salvando = true;
    let data = this.formCliente.value;
    let endereco1 = new Endereco(
      data['logradouro1'],
      data['cep1'],
      data['bairro1'],
      data['complemento1'],
      data['numero1'],
      data['cidade1'],
      data['estado1']
    );
    let endereco2 = new Endereco(
      data['logradouro2'],
      data['cep2'],
      data['bairro2'],
      data['complemento2'],
      data['numero2'],
      data['cidade2'],
      data['estado2']
    );
    console.log(this.formCliente);
    try {
      this.clienteNav = await this.fireService.alterarCliente(
        this.clienteNav,
        data['nome'],
        data['sexo'],
        data['dataNascimento'],
        data['cpf'],
        data['identidade'],
        data['naturalidade'],
        data['uf'],
        data['orgaoIdentidade'],
        data['expedicao'],
        data['titulo'],
        data['estadoCivil'],
        data['cr'],
        data['nomeMae'],
        data['nomePai'],
        data['profissao'],
        data['modalidade'],
        data['aposentado'] ?? 'nao',
        data['ordem'],
        data['posto'],
        data['matricula'],
        data['obm'],
        endereco1,
        endereco2,
        data['telefone'],
        data['email'],
        data['atividadeApostilada'],
        data['observacoes'],
        data['senhaGov'],
        this.foto
      );
      this.utilService.toastrSucesso();
      this.abrirModalAcao();
    } catch (error) {
      this.utilService.toastrErro('Erro', 'Preencha todas as informações');
      console.error(error);
    }
    this.salvando = false;
  }

  async submitForm() {
    if (this.clienteNav) {
      this.alterarCliente();
    } else this.cadastrarCliente();
  }

  abrirModalAcao() {
    jQuery('#acaoModal').modal('show');
  }

  async salvarArquivo() {
    if (!this.descricaoArquivo || !this.arquivo || !this.clienteNav) {
      this.utilService.toastrErro('Erro', 'Preencha todas as informações');
      return;
    }
    this.salvando = true;
    try {
      this.clienteNav = await this.fireService.cadastrarArquivo(
        this.descricaoArquivo,
        this.arquivo,
        this.clienteNav
      );
      this.updateForm(this.clienteNav);
      this.utilService.toastrSucesso();
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro();
    }
    this.salvando = false;
  }

  cadastrarAquisicao(cliente?: Cliente) {
    this.router.navigateByUrl('aquisicao-arma', {
      state: { cliente: this.clienteNav ?? cliente },
    });
  }

  async excluirCadastro() {
    if (
      !confirm(
        `Deseja realmente excluir o cliente ${this.clienteNav.nome}?\nATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!`
      )
    )
      return;
    try {
      this.salvando = true;
      await this.fireService.excluirCliente(this.clienteNav);
      this.salvando = false;
      this.router.navigateByUrl('consulta-clientes');
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
    }
  }

  gerarDeclaracaoEnderecoGuarda(opcao: string) {
    this.clienteService.gerarDeclaracaoEnderecoGuardaAcervo(
      this.clienteNav,
      opcao
    );
  }

  gerarDeclaracaoInquerito() {
    this.clienteService.gerarDeclaracaoInquerito(this.clienteNav);
  }

  gerarDeclaracaoSegurancaAcervo(opcao: string) {
    this.clienteService.gerarDeclaracaoSegurancaAcervo(this.clienteNav, opcao);
  }
  gerarDeclaracaoGuardaAcervo() {
    this.clienteService.gerarDeclaracaoGuardaAcervo(this.clienteNav);
  }

  gerarDeclaracaoModalidade() {
    this.clienteService.gerarDeclaracaoModalidade(this.clienteNav);
  }
}
