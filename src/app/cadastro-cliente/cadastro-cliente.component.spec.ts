import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroAtiradorComponent } from './cadastro-cliente.component';

describe('CadastroAtiradorComponent', () => {
  let component: CadastroAtiradorComponent;
  let fixture: ComponentFixture<CadastroAtiradorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastroAtiradorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroAtiradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
