import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from '../model/cliente.model';
import { FireService } from '../services/fire.service';

@Component({
  selector: 'app-aniversariantes',
  templateUrl: './aniversariantes.component.html',
  styleUrls: ['./aniversariantes.component.scss']
})
export class AniversariantesComponent implements OnInit {
  aniversariantes: Cliente[];
  constructor(private router: Router, private fireService: FireService) {
    if (
      this.router.getCurrentNavigation() != null &&
      this.router.getCurrentNavigation().extras?.state != null
    ) {
      this.aniversariantes =
        this.router.getCurrentNavigation().extras.state?.aniversariantes;
    }
    else {
      this.fireService.getAniversariantes().then(a => {
        this.aniversariantes = a;
      })
    }
    console.log(this.aniversariantes);
  }

  ngOnInit(): void {
  }

  enviarWhatsapp(cliente: Cliente) {
    let link = `https://api.whatsapp.com/send?phone=55${cliente.telefone}`;
    window.open(link, 'blank');
  }

  visualizarCadastro(cliente: Cliente) {
    this.router.navigateByUrl('cadastro-cliente', {
      state: { cliente: cliente },
    });
  }
}
