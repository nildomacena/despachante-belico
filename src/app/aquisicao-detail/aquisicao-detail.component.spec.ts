import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AquisicaoDetailComponent } from './aquisicao-detail.component';

describe('AquisicaoDetailComponent', () => {
  let component: AquisicaoDetailComponent;
  let fixture: ComponentFixture<AquisicaoDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AquisicaoDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AquisicaoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
