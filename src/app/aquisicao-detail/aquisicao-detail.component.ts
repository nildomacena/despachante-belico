import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Aquisicao } from '../model/aquisicao.model';
import { ProcedimentosService } from '../services/procedimentos.service';
import {
  Cell,
  PdfMakeWrapper,
  Table,
  Txt,
  Line,
  Canvas,
} from 'pdfmake-wrapper';
import { UtilService } from '../services/util.service';
import { Pendencia } from '../model/pendencia.model';
import { Router } from '@angular/router';
import { Cliente } from '../model/cliente.model';
import { FireService } from '../services/fire.service';
import { ClipboardService } from 'ngx-clipboard';
import { Arquivo } from '../model/arquivo.model';
import { PdfService } from '../services/pdf.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
declare var jQuery: any;

const BORDA_LATERAL: [boolean, boolean, boolean, boolean] = [
  true,
  false,
  true,
  false,
];
const BORDA_LATERAL_BAIXO: [boolean, boolean, boolean, boolean] = [
  true,
  false,
  true,
  true,
];

@Component({
  selector: 'app-aquisicao-detail',
  templateUrl: './aquisicao-detail.component.html',
  styleUrls: ['./aquisicao-detail.component.scss'],
})
export class AquisicaoDetailComponent implements OnInit {
  aquisicao: Aquisicao;
  pendencias: Pendencia[] = [];
  salvando: boolean;
  descricaoArquivo: string;
  arquivo: File;
  @ViewChild('labelImport')
  labelImport: ElementRef;
  formResidencia: FormGroup;
  orgaoResidencia: string = 'pf';
  cliente: Cliente;
  mostraGRU: boolean;
  mostraWhatsapp: boolean = true;
  mostraRequerimento: boolean;
  mostraProcuracao: boolean;
  mostraResidencia: boolean;
  constructor(
    private procedimentosService: ProcedimentosService,
    private fireService: FireService,
    private utilService: UtilService,
    private router: Router,
    private pdfService: PdfService,
    private clipboardService: ClipboardService,
    private formBuilder: FormBuilder
  ) {
    if (
      this.router.getCurrentNavigation() != null &&
      this.router.getCurrentNavigation().extras?.state != null
    ) {
      console.log(this.router.getCurrentNavigation().extras.state);
      this.aquisicao =
        this.router.getCurrentNavigation().extras.state?.aquisicao;
    }
    this.initPromises();
    this.formResidencia = this.formBuilder.group({
      nome: new FormControl('', [Validators.required]),
      nacionalidade: new FormControl('brasileiro', [Validators.required]),
      naturalidade: new FormControl('', [Validators.required]),
      identidade: new FormControl('', [Validators.required]),
      orgaoIdentidade: new FormControl('', [Validators.required]),
      parentesco: new FormControl(''),
      cpf: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    jQuery('#residenciaModal').on('hidden.bs.modal', (e) => {
      this.orgaoResidencia = null;
    });
  }

  copy(str: string) {
    this.clipboardService.copy(str);
    this.utilService.toastrSucesso('Copiado', 'Texto copiado');
  }

  async initPromises() {
    if (!this.aquisicao)
      this.aquisicao = await this.procedimentosService.getAquisicaoTeste();
    this.updatePendencias();
    this.cliente = this.aquisicao.cliente;
    this.mostraGRU =
      this.cliente.modalidade != null &&
      (this.cliente.modalidade == 'pm' ||
        this.cliente.modalidade == 'bm' ||
        this.cliente.modalidade == 'cac');
    this.mostraRequerimento =
      this.cliente.modalidade != null &&
      (this.cliente.modalidade == 'pm' || this.cliente.modalidade == 'bm');
    this.cliente = await this.fireService.getClienteById(
      this.aquisicao.cliente.id
    );
    console.log(this.cliente);
    this.mostraGRU =
      this.cliente.modalidade == 'pm' ||
      this.cliente.modalidade == 'bm' ||
      this.cliente.modalidade == 'cac';
    this.mostraRequerimento =
      this.cliente.modalidade == 'pm' || this.cliente.modalidade == 'bm';
  }

  /** ARQUIVOS */
  onFileChange(event) {
    this.arquivo = event.target.files[0];
    this.labelImport.nativeElement.innerText = this.arquivo.name;
    console.log(this.arquivo);
  }

  downloadArquivo(arquivo: Arquivo) {
    window.open(arquivo.url, 'blank');
  }

  visualizarCadastro() {
    this.router.navigateByUrl('cadastro-cliente', {
      state: { cliente: this.cliente },
    });
  }

  async excluirArquivo(arquivo: Arquivo) {
    if (!confirm(`Deseja realmente excluir o arquivo ${arquivo.descricao}?`)) {
      return;
    }
    this.salvando = true;
    try {
      this.aquisicao = await this.procedimentosService.excluirArquivoAquisicao(
        this.aquisicao,
        arquivo
      );
      this.utilService.toastrSucesso();
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro();
    }
    this.salvando = false;
  }

  async salvarArquivo() {
    if (!this.descricaoArquivo || !this.arquivo || !this.aquisicao) {
      this.utilService.toastrErro('Erro', 'Preencha todas as informações');
      return;
    }
    this.salvando = true;
    try {
      this.aquisicao =
        await this.procedimentosService.cadastrarArquivoAquisicao(
          this.descricaoArquivo,
          this.arquivo,
          this.aquisicao
        );
      //this.updateForm(this.clienteNav);
      this.utilService.toastrSucesso();
    } catch (error) {
      console.error(error);
      this.utilService.toastrErro();
    }
    this.salvando = false;
  }

  /**FIM ARQUIVOS */

  /**PDFs */
  async gerarRequerimento() {
    this.salvando = true;
    this.pdfService.gerarRequerimento(this.aquisicao, this.cliente);
    this.salvando = false;
  }

  abrirModalProcuracao() {
    jQuery('#outorgadoModal').modal('show');
  }

  async gerarProcuracao(outorgado: string) {
    this.salvando = true;
    this.pdfService.gerarProcuracao(this.cliente, outorgado);
    this.salvando = false;
  }

  gerarFormularioEntradaProcesso() {
    this.pdfService.gerarEntradaProcesso(this.aquisicao);
  }

  abrirModalResidenciaTerceiros() {
    console.log('abrirModalResidenciaTerceiros');
    this.formResidencia.controls['endereco'].setValue(
      this.cliente.endereco1Formatado
    );
    jQuery('#residenciaModal').modal('show');
  }

  async gerarRequerimentoBombeiro() {
    /*
    retirar isso quando sair dos testes
    if (this.aquisicao.modalidade != 'bm') {
      alert('Esse cliente não é Bombeiro.')
      return;
    } */

    this.pdfService.gerarRequerimentoBombeiro(this.aquisicao.cliente);
  }

  async gerarDeclaracaoTerceiros() {
    this.salvando = true;
    if (this.formResidencia.invalid) {
      this.utilService.toastrErro(
        'Informações incompletas',
        'Confira os dados e tente novamente'
      );
      return;
    }
    try {
      let data = this.formResidencia.value;
      this.pdfService.gerarDeclaracaoResidencia(
        this.cliente,
        data['nome'],
        data['endereco'],
        data['nacionalidade'],
        data['naturalidade'],
        data['parentesco'],
        data['identidade'],
        data['orgaoIdentidade'],
        data['cpf'],
        this.orgaoResidencia
      );
    } catch (error) {
      this.utilService.toastrErro();
      console.error(error);
    }
    console.log(this.formResidencia);
    this.salvando = false;
  }
  toggleOrgaoResidencia(orgao: string) {
    this.orgaoResidencia = orgao;
  }

  resetForm() {
    this.formResidencia.reset();
  }
  /** FIM PDFs */
  updatePendencias() {
    this.pendencias = [];
    if (this.aquisicao.pendenciasResolvidas.length == 0) {
      this.pendencias = this.procedimentosService.pendencias;
      return;
    }
    this.procedimentosService.pendencias.forEach((p1) => {
      let achou: boolean;
      this.aquisicao.pendenciasResolvidas.forEach((p2) => {
        if (p2.descricao == p1.descricao) achou = true;
      });
      if (!achou) this.pendencias.push(p1);
    });
  }
  enviarWhatsapp() {
    let link = `https://api.whatsapp.com/send?phone=55(82)999481271&text=
    TRATATIVA DE COMPRA%0a
    • Nome do cliente: ${this.cliente.nome}%0a
    • Arma: ${this.aquisicao.modelo.especie.nome} ${this.aquisicao.modelo.nome} ${this.aquisicao.calibre.nome}%0a
    • Acabamento: ${this.aquisicao.acabamento}%0a
    • Forma de pagamento: ${this.aquisicao.formaPagamento}%0a
    • Modalidade: ${this.aquisicao.cliente.modalidadeFormatada}%0a
    • Email do cliente: ${this.cliente.email}%0a
    • Telefone do cliente: ${this.cliente.telefoneFormatado}`;
    window.open(link, 'blank');
  }

  async retirarPendencia(pendencia: Pendencia) {
    try {
      this.aquisicao = await this.procedimentosService.addPendenciaResolvida(
        this.aquisicao,
        pendencia
      );
      this.updatePendencias();
    } catch (error) {
      console.error(error);
    }
  }

  async desfazerPendencias(pendencia: Pendencia) {
    try {
      this.aquisicao =
        await this.procedimentosService.removerPendenciaResolvida(
          this.aquisicao,
          pendencia
        );
      this.updatePendencias();
    } catch (error) {
      console.error(error);
    }
  }

  async deletarAquisicao() {
    console.log(this.aquisicao);
    if (
      !confirm(
        `Deseja realmente excluir realmente o cadastro dessa aquisição?\nATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!`
      )
    )
      return;
    try {
      this.salvando = true;
      await this.procedimentosService.excluirAquisicao(this.aquisicao);
      this.salvando = false;
      this.router.navigateByUrl('consulta-aquisicao');
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
    }
  }

  abrirGRU() {
    this.clipboardService.copy('167086');
    window.open(
      'https://consulta.tesouro.fazenda.gov.br/gru/gru_simples.asp',
      'blank'
    );
  }

  submitFormResidencia() {}
}
