import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Calibre } from '../model/calibre.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';


@Component({
  selector: 'app-calibres',
  templateUrl: './calibres.component.html',
  styleUrls: ['./calibres.component.scss']
})
export class CalibresComponent implements OnInit {
  calibres: Calibre[] = [];
  formCalibre: FormGroup;
  calibreSelecionado: Calibre;
  pesquisa: string;
  salvando: boolean;

  constructor(private fireService: FireService, private formBuilder: FormBuilder, private utilService: UtilService) {
    this.fireService.getCalibres().then(calibres => {
      this.calibres = calibres;
      console.log(this.calibres);
    });
    this.formCalibre = this.formBuilder.group({
      'nome': new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
  }

  onSelectCalibre(calibre: Calibre) {
    if (calibre == this.calibreSelecionado) {
      this.resetForm();
      this.calibreSelecionado = null;
    }
    else {
      this.calibreSelecionado = calibre;
      this.formCalibre.patchValue({
        nome: calibre.nome,
      })
    }
  }

  resetForm() {
    this.calibreSelecionado = null;
    this.formCalibre.reset();
  }

  async deletarCalibre() {
    if (confirm(`Deseja realmente deletar o calibre ${this.calibreSelecionado.nome}?`)) {
      try {
        this.salvando = true;
        this.calibres = await this.fireService.deleteCalibre(this.calibreSelecionado);
        this.resetForm();
        this.salvando = false;
        this.utilService.toastrSucesso();
      } catch (error) {
        this.salvando = false;
        console.error(error);
        this.utilService.toastrErro();
      }
    }
  }

  async submitForm() {
    if (this.formCalibre.invalid) {
      alert('Informações incompletas. Verifique os dados e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      this.calibres = await this.fireService.cadastrarCalibre(this.formCalibre.value['nome'].trim());
      this.salvando = false;
      this.resetForm();
      this.utilService.toastrSucesso();
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
      console.error(error);
    }
  }

  consolee() {
    console.log(this.formCalibre)
  }

}
