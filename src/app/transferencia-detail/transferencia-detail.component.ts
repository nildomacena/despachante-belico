import { Cliente } from './../model/cliente.model';
import { Arma } from './../model/arma.model';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { ClipboardService } from 'ngx-clipboard';
import { PdfService } from './../services/pdf.service';
import { UtilService } from './../services/util.service';
import { FireService } from './../services/fire.service';
import { Transferencia } from './../model/transferencia.model';
import * as transferenciaFactory from './../model/transferencia.model';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-transferencia-detail',
  templateUrl: './transferencia-detail.component.html',
  styleUrls: ['./transferencia-detail.component.scss'],
})
export class TransferenciaDetailComponent implements OnInit {
  transferencia: Transferencia;
  salvando: boolean;
  formResidencia: FormGroup;
  formRequerimento: FormGroup;
  orgaoResidencia: string = 'pf';

  constructor(
    private fireService: FireService,
    private utilService: UtilService,
    private router: Router,
    private pdfService: PdfService,
    private formBuilder: FormBuilder,
    private clipboardService: ClipboardService
  ) {
    this.formResidencia = this.formBuilder.group({
      nome: new FormControl('', [Validators.required]),
      nacionalidade: new FormControl('brasileiro', [Validators.required]),
      naturalidade: new FormControl('', [Validators.required]),
      identidade: new FormControl('', [Validators.required]),
      orgaoIdentidade: new FormControl('', [Validators.required]),
      parentesco: new FormControl(''),
      cpf: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required]),
    });
    this.formRequerimento = this.formBuilder.group({
      nomeTrabalho: new FormControl(''),
      enderecoTrabalho: new FormControl(''),
      bairroTrabalho: new FormControl(''),
      municipioTrabalho: new FormControl(''),
      ufTrabalho: new FormControl(''),
      cepTrabalho: new FormControl(''),
      telefoneTrabalho: new FormControl(''),
      cnpjTrabalho: new FormControl(''),
    });
    if (this.router.getCurrentNavigation()?.extras?.state) {
      console.log(
        this.router.getCurrentNavigation().extras.state.transferencia
      );
      this.transferencia =
        this.router.getCurrentNavigation().extras.state.transferencia;
      //this.formCliente.patchValue(this.clienteNav.formGroup)
      this.updateClientes();
    } else {
      //this.transferencia = transferenciaFactory.transferenciaTest();
      this.router.navigateByUrl('listar-transferencias');
    }
  }

  ngOnInit(): void {}

  async updateClientes() {
    this.transferencia.doador = await this.fireService.getClienteById(
      this.transferencia.doador.id
    );
    this.transferencia.recebedor = await this.fireService.getClienteById(
      this.transferencia.recebedor.id
    );
    this.transferencia.arma = await this.fireService.getArmaById(
      this.transferencia.arma.id
    );
    this.transferencia.arma.modelo = await this.fireService.getModeloById(
      this.transferencia.arma.modelo.id
    );
    console.log(this.transferencia.arma.modelo);
  }

  copy(str: string) {
    this.clipboardService.copy(str);
    this.utilService.toastrSucesso('Copiado', 'Texto copiado');
  }

  irParaArma() {
    this.router.navigateByUrl('cadastrar-arma', {
      state: { arma: this.transferencia.arma },
    });
  }

  irParaCliente(cliente: Cliente) {
    this.router.navigateByUrl('cadastro-cliente', {
      state: { cliente: cliente },
    });
  }

  anexoK() {
    this.pdfService.anexoK(this.transferencia);
  }

  anexoF1() {
    this.pdfService.anexoF1(this.transferencia);
  }

  anexoH() {
    this.pdfService.anexoH(this.transferencia);
  }

  anexoG() {
    this.pdfService.anexoG(this.transferencia);
  }

  anexoL() {
    this.pdfService.anexoL(this.transferencia);
  }

  anexoI() {
    this.pdfService.anexoI(this.transferencia);
  }

  consoleRequerimento() {
    console.log(this.formRequerimento);
  }
  requerimentoPF() {
    let data = this.formRequerimento.value;
    this.pdfService.requerimentoPF(
      this.transferencia,
      data['nomeTrabalho'],
      data['municipioTrabalho'],
      data['enderecoTrabalho'],
      data['bairroTrabalho'],
      data['ufTrabalho'],
      data['cepTrabalho'],
      data['telefoneTrabalho'],
      data['cnpjTrabalho']
    );
  }

  declaracoes() {
    this.pdfService.declaracaoEfetivaNecessidade(this.transferencia.recebedor);
    this.pdfService.declaracaoLocalSeguro(this.transferencia.recebedor);
    this.pdfService.declaracaoProcesso(this.transferencia.recebedor);
  }

  termoDeTransferencia() {
    this.pdfService.termoDeTransferencia(this.transferencia);
  }

  abrirModalResidenciaTerceiros() {
    console.log('abrirModalResidenciaTerceiros');
    this.formResidencia.controls['endereco'].setValue(
      this.transferencia.recebedor.endereco1Formatado
    );
    jQuery('#residenciaModal').modal('show');
  }

  async gerarDeclaracaoTerceiros() {
    this.salvando = true;
    if (this.formResidencia.invalid) {
      this.utilService.toastrErro(
        'Informações incompletas',
        'Confira os dados e tente novamente'
      );
      return;
    }
    try {
      let data = this.formResidencia.value;
      this.pdfService.gerarDeclaracaoResidencia(
        this.transferencia.recebedor,
        data['nome'],
        data['endereco'],
        data['nacionalidade'],
        data['naturalidade'],
        data['parentesco'],
        data['identidade'],
        data['orgaoIdentidade'],
        data['cpf'],
        this.orgaoResidencia
      );
    } catch (error) {
      this.utilService.toastrErro();
      console.error(error);
    }
    console.log(this.formResidencia);
    this.salvando = false;
  }

  resetForm() {
    this.formResidencia.reset();
  }

  toggleOrgaoResidencia(orgao: string) {
    this.orgaoResidencia = orgao;
  }

  async gerarProcuracao(outorgado: string) {
    this.salvando = true;
    this.pdfService.gerarProcuracao(this.transferencia.recebedor, outorgado);
    this.salvando = false;
  }

  sitePF() {
    window.open(
      'https://servicos.dpf.gov.br/sinarm-internet/faces/publico/incluirReqTransfRegistroArmaFogo/consultarReqTransfRegistroArmaFogo.seam',
      'blank'
    );
  }

  abrirModalProcuracao() {
    jQuery('#outorgadoModal').modal('show');
  }
}
