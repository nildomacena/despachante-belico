import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferenciaDetailComponent } from './transferencia-detail.component';

describe('TransferenciaDetailComponent', () => {
  let component: TransferenciaDetailComponent;
  let fixture: ComponentFixture<TransferenciaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferenciaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferenciaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
