import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Calibre } from '../model/calibre.model';
import { Especie } from '../model/especie.model';
import { Fabricante } from '../model/fabricante.model';
import { Modelo } from '../model/modelo.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';


@Component({
  selector: 'app-modelos',
  templateUrl: './modelos.component.html',
  styleUrls: ['./modelos.component.scss']
})
export class ModelosComponent implements OnInit {
  fabricantes: Fabricante[] = [];
  calibres: Calibre[] = [];
  especies: Especie[] = [];
  modelos: Modelo[] = [];
  formModelo: FormGroup;
  modeloSelecionado: Modelo;
  pesquisa: string;
  salvando: boolean;
  almaRaiada: boolean = false;
  modelosFiltrados: Modelo[] = [];

  constructor(private fireService: FireService, private formBuilder: FormBuilder, private utilService: UtilService, private ngZone: NgZone) {
    this.fireService.getFabricantes().then(fabricantes => {
      this.fabricantes = fabricantes;
    });

    this.fireService.getCalibres().then(calibres => {
      this.calibres = calibres;
    });

    this.fireService.getEspecies().then(especies => {
      this.especies = especies;
    });

    this.fireService.getModelos().then(modelos => {
      this.modelos = this.modelosFiltrados = modelos;
    })

    this.formModelo = this.formBuilder.group({
      'nome': new FormControl('', [Validators.required]),
      'fabricante': new FormControl('', [Validators.required]),
      'tipoAlma': new FormControl('', [Validators.required]),
      'quantidadeRaias': new FormControl('', [Validators.required]),
      'capacidadeTiro': new FormControl('', [Validators.required]),
      'sentidoRaias': new FormControl('', [Validators.required]),
      'numeroCanos': new FormControl('', [Validators.required]),
      'funcionamento': new FormControl('', [Validators.required]),
      'especie': new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
  }

  onSearch() {
    if (this.pesquisa.length <= 0) {
      this.modelosFiltrados = this.modelos;
      return;
    }
    else {
      this.modelosFiltrados = this.modelos.filter(m =>
        m.nome.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.especie.nome.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.tipoAlma.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.fabricante.nome.toLowerCase().includes(this.pesquisa.toLowerCase()));
    }
  }
  onChangeAlma() {
    this.ngZone.run(() => {
      this.almaRaiada = this.formModelo.value['tipoAlma'].includes('raiada');
    });
    if (!this.almaRaiada) {
      this.formModelo.controls['quantidadeRaias'].patchValue('nao_aplica');
      this.formModelo.controls['sentidoRaias'].patchValue('nao_aplica');
    }
    else if(!this.modeloSelecionado) {
      this.formModelo.controls['quantidadeRaias'].patchValue(null);
      this.formModelo.controls['sentidoRaias'].patchValue(null);
    }
    console.log('onChangeRaia', this.formModelo.value['tipoAlma'].includes('raiada'))
  }

  resetForm() {
    this.modeloSelecionado = null;
    this.formModelo.reset();
  }


  consolee() {
    console.log(this.formModelo)
  }

  onSelectModelo(modelo: Modelo) {
    console.log(modelo)
    if (modelo == this.modeloSelecionado) {
      this.resetForm();
      this.modeloSelecionado = null;
    }
    else {
      this.modeloSelecionado = modelo;
      this.formModelo.patchValue({
        'nome': modelo.nome,
        'fabricante': modelo.fabricante.id,
        'tipoAlma': modelo.tipoAlma,
        'quantidadeRaias': modelo.quantidadeRaias,
        'sentidoRaias': modelo.sentidoRaias,
        'numeroCanos': modelo.numeroCanos,
        'funcionamento': modelo.funcionamento,
        'especie': modelo.especie.id,
        'capacidadeTiro': modelo.capacidadeTiro
      });
      this.onChangeAlma();
      //this.almaRaiada = !(this.modeloSelecionado.tipoAlma == 'lisa')
    /*   if (this.modeloSelecionado.tipoAlma == 'lisa') {
        this.formModelo.controls['quantidadeRaias'].patchValue('nao_aplica');
        this.formModelo.controls['sentidoRaias'].patchValue('nao_aplica');
      }
      else {
        this.formModelo.controls['quantidadeRaias'].patchValue(null);
        this.formModelo.controls['sentidoRaias'].patchValue(null);
      } */
    }
  }

  async alterarModelo() {
    if (this.formModelo.invalid) {
      alert('Informações incompletas. Verifique os dados e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      this.modelos = this.modelosFiltrados = await this.fireService.alterarModelo(this.modeloSelecionado, this.formModelo);
      console.log(this.modelosFiltrados);
      this.salvando = false;
      this.resetForm();
      this.utilService.toastrSucesso();
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
      console.error(error);
    }
  }

  async submitForm() {
    if (this.formModelo.invalid) {
      alert('Informações incompletas. Verifique os dados e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      this.modelos = this.modelosFiltrados = await this.fireService.cadastrarModelo(this.formModelo);
      this.salvando = false;
      this.resetForm();
      this.utilService.toastrSucesso();
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
      console.error(error);
    }
  }

  async deleteModelo() {
    if (confirm(`Deseja realmente deletar o fabricante ${this.modeloSelecionado.nome}?`)) {
      try {
        this.salvando = true;
        this.modelosFiltrados = this.modelos = await this.fireService.deleteModelo(this.modeloSelecionado);
        this.resetForm();
        this.salvando = false;
        this.utilService.toastrSucesso();
      } catch (error) {
        this.salvando = false;
        console.error(error);
        this.utilService.toastrErro();
      }
    }

  }
}
