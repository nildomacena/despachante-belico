import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Especie } from '../model/especie.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-especies',
  templateUrl: './especies.component.html',
  styleUrls: ['./especies.component.scss']
})
export class EspeciesComponent implements OnInit {
  especies: Especie[] = [];
  formEspecie: FormGroup;
  especieSelecionada: Especie;
  pesquisa: string;
  salvando: boolean;

  constructor(private fireService: FireService, private formBuilder: FormBuilder, private utilService: UtilService) {
    this.fireService.getEspecies().then(especies => {
      this.especies = especies;
      console.log(this.especies);
    });
    this.formEspecie = this.formBuilder.group({
      'nome': new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
  }

  onSelectEspecie(especie: Especie) {
    if (especie == this.especieSelecionada) {
      this.resetForm();
      this.especieSelecionada = null;
    }
    else {
      this.especieSelecionada = especie;
      this.formEspecie.patchValue({
        nome: especie.nome,
      })
    }
  }

  resetForm() {
    this.especieSelecionada = null;
    this.formEspecie.reset();
  }

  async deletarEspecie() {
    if (confirm(`Deseja realmente deletar o especie ${this.especieSelecionada.nome}?`)) {
      try {
        this.salvando = true;
        this.especies = await this.fireService.deleteEspecie(this.especieSelecionada);
        this.resetForm();
        this.salvando = false;
        this.utilService.toastrSucesso();
      } catch (error) {
        this.salvando = false;
        console.error(error);
        this.utilService.toastrErro();
      }
    }
  }

  async submitForm() {
    if (this.formEspecie.invalid) {
      alert('Informações incompletas. Verifique os dados e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      this.especies = await this.fireService.cadastrarEspecie(this.formEspecie.value['nome'].trim());
      this.salvando = false;
      this.resetForm();
      this.utilService.toastrSucesso();
    } catch (error) {
      this.utilService.toastrErro();
      this.salvando = false;
      console.error(error);
    }
  }

  consolee() {
    console.log(this.formEspecie)
  }

}
