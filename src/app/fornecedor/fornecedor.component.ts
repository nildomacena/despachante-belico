import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Fornecedor } from '../model/fornecedor.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-fornecedor',
  templateUrl: './fornecedor.component.html',
  styleUrls: ['./fornecedor.component.scss']
})
export class CadastroFornecedorComponent implements OnInit {
  fornecedores: Fornecedor[] = [];
  formFornecedor: FormGroup;
  fornecedorSelecionado: Fornecedor;
  pesquisa: string;
  salvando: boolean;
  constructor(
    private fireService: FireService,
    private formBuilder: FormBuilder,
    private utilService: UtilService) {

    this.fireService.getFornecedores().then(fornecedores => {
      this.fornecedores = fornecedores;
      console.log(this.fornecedores);
    });
    this.formFornecedor = this.formBuilder.group({
      'nome': new FormControl('', [Validators.required]),
      'cnpj': new FormControl('', [Validators.required,/*  Validators.pattern('^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$') */]),
      'cr': new FormControl('', Validators.required),
      'endereco': new FormControl('', Validators.required),
      'cep': new FormControl('', Validators.required),
      'telefone': new FormControl('', Validators.required),
    });
  }



  ngOnInit(): void {
  }

  resetForm() {
    this.salvando = false;
    this.formFornecedor.reset();
  }

  async submitForm() {
    if (this.formFornecedor.invalid) {
      console.log(this.formFornecedor);
      alert('Formulário incompleto. Preencha as informações corretamente')
    }
    try {
      this.salvando = true;
      let data = this.formFornecedor.value;
      this.fornecedores = await this.fireService.cadastrarFornecedor(
        data['nome'].trim(),
        data['cnpj'].trim(),
        data['cr'].trim(),
        data['endereco'].trim(),
        data['cep'].trim(),
        data['telefone'].trim(),
      );
    } catch (error) {
      console.error(error);
    }
    console.log(this.formFornecedor);
    this.salvando = false;
  }

  onSelectFornecedor(fabricante: Fornecedor) {
    if (fabricante == this.fornecedorSelecionado) {
      this.resetForm();
      this.fornecedorSelecionado = null;
    }
    else {
      this.fornecedorSelecionado = fabricante;
      this.formFornecedor.patchValue({
        nome: fabricante.nome,
        cnpj: fabricante.cnpj
      })
    }
  }

  consolee() {
    console.log(this.formFornecedor)
  }

}
