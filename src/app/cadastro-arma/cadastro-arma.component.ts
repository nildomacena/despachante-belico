import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Arma } from '../model/arma.model';
import { Calibre } from '../model/calibre.model';
import { Cliente } from '../model/cliente.model';
import { Especie } from '../model/especie.model';
import { Fabricante } from '../model/fabricante.model';
import { Modelo } from '../model/modelo.model';
import { FireService } from '../services/fire.service';
import { UtilService } from '../services/util.service';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { listLocales } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

defineLocale('pt-br', ptBrLocale);

@Component({
  selector: 'app-cadastro-arma',
  templateUrl: './cadastro-arma.component.html',
  styleUrls: ['./cadastro-arma.component.scss']
})
export class CadastroArmaComponent implements OnInit {
  formArma: FormGroup;
  modelos: Modelo[] = [];
  modelosFiltrados: Modelo[] = [];
  modeloSelecionado: Modelo;
  pesquisa: string;
  fabricanteSelecionado: Fabricante;
  fabricantes: Fabricante[];
  fabricantesFiltrados: Fabricante[];
  calibres: Calibre[] = [];
  salvando: boolean;
  sigmaSinarm: string = null;
  isSigma: boolean = false;
  isSinarm: boolean = false;
  armaNav: Arma;
  doador: Cliente;
  recebedor: Cliente;
  modalidade: string;
  veioDeTransferencia: boolean;
  locales = listLocales();

  constructor(
    private fireService: FireService,
    private formBuilder: FormBuilder,
    private utilService: UtilService,
    private router: Router,
    private ngZone: NgZone,
    private localeService: BsLocaleService) {
    this.formArma = this.formBuilder.group({
      'modelo': new FormControl('', [Validators.required]),
      'modeloNome': new FormControl('', [Validators.required]),
      'comprimentoCano': new FormControl('', [Validators.required]),
      'notaFiscal': new FormControl(''),
      'dataNotaFiscal': new FormControl(''),
      'numero': new FormControl('', [Validators.required]),
      'registroFederal': new FormControl('', [Validators.required]),
      'expedidor': new FormControl('', [Validators.required]),
      'uf': new FormControl('', [Validators.required]),
      'dataEmissao': new FormControl('', [Validators.required]),
      'pessoaJuridica': new FormControl(''),
      'cnpj': new FormControl(''),
      'sinarm': new FormControl('',),
      'sigma': new FormControl('',),
      'pais': new FormControl('', [Validators.required]),
      'acabamento': new FormControl('', [Validators.required]),
      'calibre': new FormControl('', [Validators.required]),
      'isSigma': new FormControl(null, [Validators.required]),
      'isSinarm': new FormControl(null, [Validators.required]),
    });
    this.localeService.use('pt-br');

    this.fireService.getCalibres().then(calibres => {
      this.calibres = calibres;
    })
    this.fireService.getModelos().then(modelos => {
      this.modelos = this.modelosFiltrados = modelos;
    });
    this.fireService.getFabricantes().then(fabricantes => {
      this.fabricantes = this.fabricantesFiltrados = fabricantes;
    });

    if (this.router.getCurrentNavigation()?.extras?.state) {

      if (this.router.getCurrentNavigation().extras.state.arma) {
        this.armaNav = this.router.getCurrentNavigation().extras.state.arma;
        this.updateForm(this.router.getCurrentNavigation().extras.state.arma)
      }
      else if (this.router.getCurrentNavigation().extras.state.veioDeTransferencia) {
        this.doador = this.router.getCurrentNavigation().extras.state.doador;
        this.recebedor = this.router.getCurrentNavigation().extras.state.recebedor;
        this.veioDeTransferencia = this.router.getCurrentNavigation().extras.state.veioDeTransferencia
        this.modalidade = this.router.getCurrentNavigation().extras.state.modalidade
      }
    }
  }
  ngOnInit(): void {
  }

  async focusOut() {
    if (this.armaNav)
      return;
    if (await this.fireService.checkArmaCadastrada(this.formArma.value['numero'])) {
      this.utilService.toastrErro('Erro', 'Arma já cadastrada');
    }
    console.log('focusOut');
  }

  updateForm(arma: Arma) {
    this.isSigma = arma.isSigma;
    this.isSinarm = arma.isSinarm;
    this.toggleSigmaSinarm(arma.isSigma ? 'sigma' : 'sinarm');
    this.formArma = this.formBuilder.group(arma.formGroup);
    this.modeloSelecionado = arma.modelo;
    console.log(this.formArma)
  }
  consolee() {
    console.log(this.formArma)
  }

  goToModelo() {
    this.router.navigateByUrl('modelos');
  }

  voltarParaTransferencia() {
    this.router.navigateByUrl('cadastrar-transferencia', {
      state: {
        doador: this.doador,
        recebedor: this.recebedor,
        modalidade: this.modalidade
      }
    });
  }
  toggleSigmaSinarm(modo: string) {
    this.sigmaSinarm = modo;

    if (modo == 'sinarm') {
      this.formArma.controls['sinarm'].setValidators([Validators.required])
      this.formArma.controls['registroFederal'].setValidators([Validators.required])
      this.formArma.controls['registroFederal'].enable();
      this.formArma.controls['sigma'].setValidators([])
      this.formArma.controls['isSigma'].patchValue(false);
      this.formArma.controls['isSinarm'].patchValue(true);
    }
    if (modo == 'sigma') {
      console.log('sigma')
      this.formArma.controls['sigma'].setValidators([Validators.required])
      this.formArma.controls['sinarm'].setValidators([])
      //this.formArma.controls['registroFederal'].disable()
      this.formArma.controls['registroFederal'].setValidators([Validators.nullValidator])
      this.formArma.controls['registroFederal'].patchValue('')
      this.formArma.controls['isSigma'].patchValue(true);
      this.formArma.controls['isSinarm'].patchValue(false);
    }
  }

  onSelectModelo(modelo) {
    console.log(modelo)
    if (modelo == this.modeloSelecionado) {
      this.modeloSelecionado = null;
    }
    else {
      this.modeloSelecionado = modelo;
    }
  }

  onFecharModalCalibre() {
    this.fireService.getCalibres().then(calibres => {
      this.calibres = calibres;
    });
    this.formArma.controls['calibre'].patchValue(null);
  }

  cancelarModelo() {
    this.modeloSelecionado = null;
    this.formArma.patchValue({
      'modeloNome': null,
      'modelo': null,
    });
  }

  closeSelectedModelo() {
    this.formArma.patchValue({
      'modeloNome': this.modeloSelecionado.nome,
      'modelo': this.modeloSelecionado.id,
    });
  }

  resetForm() {
    this.fabricanteSelecionado = null;
    this.modeloSelecionado = null;
    this.formArma.reset();
  }

  onSearch() {
    if (this.pesquisa.length <= 0) {
      this.modelosFiltrados = this.modelos;
      return;
    }
    else {
      this.modelosFiltrados = this.modelos.filter(m =>
        m.nome.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.especie.nome.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.tipoAlma.toLowerCase().includes(this.pesquisa.toLowerCase()) ||
        m.fabricante.nome.toLowerCase().includes(this.pesquisa.toLowerCase()));
    }
  }

  submitForm() {
    if (!this.armaNav) {
      console.log('submitForm()', this.armaNav)
      this.cadastrarArma();
    }
    else
      this.alterarArma()
  }

  async cadastrarArma() {
    console.log('cadastrar arma')
    if (this.formArma.invalid) {
      this.utilService.toastrErro('Formulário incorreto', 'Verifique as informações e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      let arma = await this.fireService.cadastrarArma(this.formArma);
      this.salvando = false;
      this.utilService.toastrSucesso();
      this.resetForm();
      if (this.veioDeTransferencia) {
        this.router.navigateByUrl('cadastrar-transferencia', {
          state: {
            doador: this.doador,
            recebedor: this.recebedor,
            arma: arma,
            modalidade: this.modalidade
          }
        });
      }
    } catch (error) {
      this.utilService.toastrErro('Erro!', 'Código do erro: ' + error);
      this.salvando = false;
      console.error(error);
    }
  }

  async alterarArma() {
    if (!confirm('Deseja realmente fazer essas alterações?'))
      console.log('alterar arma');
    if (this.formArma.invalid) {
      this.utilService.toastrErro('Formulário incorreto', 'Verifique as informações e tente novamente');
      return;
    }
    try {
      this.salvando = true;
      this.armaNav = await this.fireService.alterarArma(this.armaNav, this.formArma);
      this.updateForm(this.armaNav);
      this.salvando = false;
      this.utilService.toastrSucesso();
      this.resetForm();
    } catch (error) {
      this.utilService.toastrErro('Erro!', 'Código do erro: ' + error);
      this.salvando = false;
      console.error(error);
    }
  }
}
