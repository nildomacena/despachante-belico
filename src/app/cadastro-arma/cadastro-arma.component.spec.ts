import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroArmaComponent } from './cadastro-arma.component';

describe('CadastroArmaComponent', () => {
  let component: CadastroArmaComponent;
  let fixture: ComponentFixture<CadastroArmaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastroArmaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroArmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
