import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadastroAtiradorComponent } from './cadastro-cliente/cadastro-cliente.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/**FIREBASE IMPORTS */
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxMaskModule } from 'ngx-mask';
import { FabricantesComponent } from './fabricantes/fabricantes.component';
import { EspeciesComponent } from './especies/especies.component';
import { CalibresComponent } from './calibres/calibres.component';
import { ModelosComponent } from './modelos/modelos.component';
import { CadastroArmaComponent } from './cadastro-arma/cadastro-arma.component';
import { ConsultaArmasComponent } from './consulta-armas/consulta-armas.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpClientModule } from '@angular/common/http';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { ConsultaAtiradoresComponent } from './consulta-clientes/consulta-clientes.component';
import { CadastroFornecedorComponent } from './fornecedor/fornecedor.component';
import { AquisicaoArmaComponent } from './cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component';
import { AquisicaoDetailComponent } from './aquisicao-detail/aquisicao-detail.component';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import pdfCustomFonts from "fonts/times-new-roman.js"; // The path of your custom fonts
import { PdfMakeWrapper } from 'pdfmake-wrapper';
import { ListaAquisicaoComponent } from './lista-aquisicao/lista-aquisicao.component';
import { ClipboardModule } from 'ngx-clipboard';
import { CadastrarTransferenciaComponent } from './cadastrar-transferencia/cadastrar-transferencia.component';
import { ListaTransferenciaComponent } from './lista-transferencia/lista-transferencia.component';
import { TransferenciaDetailComponent } from './transferencia-detail/transferencia-detail.component';
import { PreCadastroComponent } from './pre-cadastro/pre-cadastro.component';
import { LoginComponent } from './login/login.component';
import { ListaPreCadastrosComponent } from './lista-pre-cadastros/lista-pre-cadastros.component';
import { AniversariantesComponent } from './aniversariantes/aniversariantes.component';
import { TesteComponent } from './teste/teste.component';

//PdfMakeWrapper.setFonts(pdfFonts);
PdfMakeWrapper.setFonts(pdfCustomFonts, {
  myCustom: {
    normal: "times-new-roman.ttf",
    bold: 'times-new-roman-grassetto.ttf',
    italics: 'times-new-roman.ttf',
    bolditalics: 'times-new-roman.ttf'
  },
  Roboto: {
    normal: "times-new-roman.ttf",
    bold: 'times-new-roman-grassetto.ttf',
    italics: 'times-new-roman.ttf',
    bolditalics: 'times-new-roman.ttf'
  }
});


PdfMakeWrapper.useFont('myCustom');

defineLocale('pt-br', ptBrLocale);

const firebaseConfig = {
  apiKey: "AIzaSyBVqiiHPTckM6rvxeCKofNV1BbavTSpFzE",
  authDomain: "recepcao-c0231.firebaseapp.com",
  databaseURL: "https://recepcao-c0231.firebaseio.com",
  projectId: "recepcao-c0231",
  storageBucket: "recepcao-c0231.appspot.com",
  messagingSenderId: "824997339691",
  appId: "1:824997339691:web:1c0c66083aa3a83dcfc06e"
};

@NgModule({
  declarations: [
    AppComponent,
    CadastroAtiradorComponent,
    FabricantesComponent,
    EspeciesComponent,
    CalibresComponent,
    ModelosComponent,
    CadastroArmaComponent,
    ConsultaArmasComponent,
    ConsultaAtiradoresComponent,
    CadastroFornecedorComponent,
    AquisicaoArmaComponent,
    AquisicaoDetailComponent,
    ListaAquisicaoComponent,
    CadastrarTransferenciaComponent,
    ListaTransferenciaComponent,
    TransferenciaDetailComponent,
    PreCadastroComponent,
    LoginComponent,
    ListaPreCadastrosComponent,
    AniversariantesComponent,
    TesteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    BsDatepickerModule.forRoot(),
    HttpClientModule,
    ClipboardModule,

  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
