# Summary

Date : 2021-09-09 09:00:35

Directory c:\Users\PC\Projetos\despachante-belico

Total : 109 files,  32502 codes, 649 comments, 784 blanks, all 33935 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 9 | 19,493 | 31 | 6 | 19,530 |
| TypeScript | 61 | 9,752 | 261 | 654 | 10,667 |
| HTML | 18 | 3,021 | 347 | 82 | 3,450 |
| SCSS | 17 | 164 | 2 | 24 | 190 |
| JavaScript | 3 | 58 | 8 | 4 | 70 |
| Markdown | 1 | 14 | 0 | 14 | 28 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 109 | 32,502 | 649 | 784 | 33,935 |
| e2e | 4 | 68 | 8 | 11 | 87 |
| e2e\src | 2 | 27 | 1 | 8 | 36 |
| fonts | 1 | 1 | 0 | 0 | 1 |
| src | 94 | 12,910 | 609 | 752 | 14,271 |
| src\app | 87 | 12,863 | 538 | 730 | 14,131 |
| src\app\aquisicao-detail | 4 | 640 | 66 | 37 | 743 |
| src\app\cadastrar-aquisicao-arma | 4 | 582 | 55 | 56 | 693 |
| src\app\cadastrar-transferencia | 4 | 475 | 13 | 44 | 532 |
| src\app\cadastro-arma | 4 | 541 | 14 | 39 | 594 |
| src\app\cadastro-cliente | 5 | 1,141 | 66 | 58 | 1,265 |
| src\app\calibres | 4 | 172 | 5 | 20 | 197 |
| src\app\consulta-armas | 4 | 133 | 15 | 16 | 164 |
| src\app\consulta-clientes | 4 | 141 | 5 | 21 | 167 |
| src\app\especies | 4 | 172 | 5 | 19 | 196 |
| src\app\fabricantes | 4 | 182 | 5 | 18 | 205 |
| src\app\fornecedor | 4 | 196 | 5 | 20 | 221 |
| src\app\lista-aquisicao | 4 | 225 | 6 | 23 | 254 |
| src\app\lista-transferencia | 4 | 181 | 51 | 20 | 252 |
| src\app\model | 14 | 1,250 | 9 | 93 | 1,352 |
| src\app\modelos | 4 | 336 | 22 | 28 | 386 |
| src\app\services | 6 | 5,538 | 106 | 158 | 5,802 |
| src\app\transferencia-detail | 4 | 536 | 61 | 34 | 631 |
| src\environments | 2 | 6 | 11 | 4 | 21 |

[details](details.md)