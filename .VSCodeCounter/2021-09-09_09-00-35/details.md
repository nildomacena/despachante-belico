# Details

Date : 2021-09-09 09:00:35

Directory c:\Users\PC\Projetos\despachante-belico

Total : 109 files,  32502 codes, 649 comments, 784 blanks, all 33935 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 14 | 0 | 14 | 28 |
| [angular.json](/angular.json) | JSON | 126 | 27 | 0 | 153 |
| [e2e/protractor.conf.js](/e2e/protractor.conf.js) | JavaScript | 28 | 6 | 2 | 36 |
| [e2e/src/app.e2e-spec.ts](/e2e/src/app.e2e-spec.ts) | TypeScript | 18 | 1 | 5 | 24 |
| [e2e/src/app.po.ts](/e2e/src/app.po.ts) | TypeScript | 9 | 0 | 3 | 12 |
| [e2e/tsconfig.json](/e2e/tsconfig.json) | JSON | 13 | 1 | 1 | 15 |
| [firebase.json](/firebase.json) | JSON | 28 | 0 | 0 | 28 |
| [fonts/times-new-roman.js](/fonts/times-new-roman.js) | JavaScript | 1 | 0 | 0 | 1 |
| [karma.conf.js](/karma.conf.js) | JavaScript | 29 | 2 | 2 | 33 |
| [package-lock.json](/package-lock.json) | JSON | 19,060 | 0 | 1 | 19,061 |
| [package.json](/package.json) | JSON | 64 | 0 | 1 | 65 |
| [src/app/app-routing.module.ts](/src/app/app-routing.module.ts) | TypeScript | 41 | 0 | 4 | 45 |
| [src/app/app.component.html](/src/app/app.component.html) | HTML | 229 | 27 | 4 | 260 |
| [src/app/app.component.scss](/src/app/app.component.scss) | SCSS | 12 | 0 | 1 | 13 |
| [src/app/app.component.spec.ts](/src/app/app.component.spec.ts) | TypeScript | 31 | 0 | 5 | 36 |
| [src/app/app.component.ts](/src/app/app.component.ts) | TypeScript | 10 | 0 | 3 | 13 |
| [src/app/app.module.ts](/src/app/app.module.ts) | TypeScript | 99 | 2 | 9 | 110 |
| [src/app/aquisicao-detail/aquisicao-detail.component.html](/src/app/aquisicao-detail/aquisicao-detail.component.html) | HTML | 303 | 55 | 5 | 363 |
| [src/app/aquisicao-detail/aquisicao-detail.component.scss](/src/app/aquisicao-detail/aquisicao-detail.component.scss) | SCSS | 8 | 0 | 0 | 8 |
| [src/app/aquisicao-detail/aquisicao-detail.component.spec.ts](/src/app/aquisicao-detail/aquisicao-detail.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/aquisicao-detail/aquisicao-detail.component.ts](/src/app/aquisicao-detail/aquisicao-detail.component.ts) | TypeScript | 309 | 11 | 26 | 346 |
| [src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.html](/src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.html) | HTML | 286 | 53 | 13 | 352 |
| [src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.scss](/src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.scss) | SCSS | 19 | 0 | 4 | 23 |
| [src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.spec.ts](/src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.ts](/src/app/cadastrar-aquisicao-arma/cadastrar-aquisicao-arma.component.ts) | TypeScript | 257 | 2 | 33 | 292 |
| [src/app/cadastrar-transferencia/cadastrar-transferencia.component.html](/src/app/cadastrar-transferencia/cadastrar-transferencia.component.html) | HTML | 227 | 12 | 8 | 247 |
| [src/app/cadastrar-transferencia/cadastrar-transferencia.component.scss](/src/app/cadastrar-transferencia/cadastrar-transferencia.component.scss) | SCSS | 25 | 0 | 5 | 30 |
| [src/app/cadastrar-transferencia/cadastrar-transferencia.component.spec.ts](/src/app/cadastrar-transferencia/cadastrar-transferencia.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/cadastrar-transferencia/cadastrar-transferencia.component.ts](/src/app/cadastrar-transferencia/cadastrar-transferencia.component.ts) | TypeScript | 203 | 1 | 25 | 229 |
| [src/app/cadastro-arma/cadastro-arma.component.html](/src/app/cadastro-arma/cadastro-arma.component.html) | HTML | 263 | 14 | 11 | 288 |
| [src/app/cadastro-arma/cadastro-arma.component.scss](/src/app/cadastro-arma/cadastro-arma.component.scss) | SCSS | 11 | 0 | 1 | 12 |
| [src/app/cadastro-arma/cadastro-arma.component.spec.ts](/src/app/cadastro-arma/cadastro-arma.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/cadastro-arma/cadastro-arma.component.ts](/src/app/cadastro-arma/cadastro-arma.component.ts) | TypeScript | 247 | 0 | 21 | 268 |
| [src/app/cadastro-cliente/backup.html](/src/app/cadastro-cliente/backup.html) | HTML | 227 | 7 | 1 | 235 |
| [src/app/cadastro-cliente/cadastro-cliente.component.html](/src/app/cadastro-cliente/cadastro-cliente.component.html) | HTML | 464 | 56 | 9 | 529 |
| [src/app/cadastro-cliente/cadastro-cliente.component.scss](/src/app/cadastro-cliente/cadastro-cliente.component.scss) | SCSS | 12 | 1 | 2 | 15 |
| [src/app/cadastro-cliente/cadastro-cliente.component.spec.ts](/src/app/cadastro-cliente/cadastro-cliente.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/cadastro-cliente/cadastro-cliente.component.ts](/src/app/cadastro-cliente/cadastro-cliente.component.ts) | TypeScript | 418 | 2 | 40 | 460 |
| [src/app/calibres/calibres.component.html](/src/app/calibres/calibres.component.html) | HTML | 62 | 5 | 2 | 69 |
| [src/app/calibres/calibres.component.scss](/src/app/calibres/calibres.component.scss) | SCSS | 11 | 0 | 1 | 12 |
| [src/app/calibres/calibres.component.spec.ts](/src/app/calibres/calibres.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/calibres/calibres.component.ts](/src/app/calibres/calibres.component.ts) | TypeScript | 79 | 0 | 11 | 90 |
| [src/app/consulta-armas/consulta-armas.component.html](/src/app/consulta-armas/consulta-armas.component.html) | HTML | 58 | 15 | 3 | 76 |
| [src/app/consulta-armas/consulta-armas.component.scss](/src/app/consulta-armas/consulta-armas.component.scss) | SCSS | 11 | 0 | 1 | 12 |
| [src/app/consulta-armas/consulta-armas.component.spec.ts](/src/app/consulta-armas/consulta-armas.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/consulta-armas/consulta-armas.component.ts](/src/app/consulta-armas/consulta-armas.component.ts) | TypeScript | 44 | 0 | 6 | 50 |
| [src/app/consulta-clientes/consulta-clientes.component.html](/src/app/consulta-clientes/consulta-clientes.component.html) | HTML | 67 | 5 | 2 | 74 |
| [src/app/consulta-clientes/consulta-clientes.component.scss](/src/app/consulta-clientes/consulta-clientes.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src/app/consulta-clientes/consulta-clientes.component.spec.ts](/src/app/consulta-clientes/consulta-clientes.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/consulta-clientes/consulta-clientes.component.ts](/src/app/consulta-clientes/consulta-clientes.component.ts) | TypeScript | 54 | 0 | 12 | 66 |
| [src/app/especies/especies.component.html](/src/app/especies/especies.component.html) | HTML | 62 | 5 | 2 | 69 |
| [src/app/especies/especies.component.scss](/src/app/especies/especies.component.scss) | SCSS | 11 | 0 | 1 | 12 |
| [src/app/especies/especies.component.spec.ts](/src/app/especies/especies.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/especies/especies.component.ts](/src/app/especies/especies.component.ts) | TypeScript | 79 | 0 | 10 | 89 |
| [src/app/fabricantes/fabricantes.component.html](/src/app/fabricantes/fabricantes.component.html) | HTML | 70 | 5 | 2 | 77 |
| [src/app/fabricantes/fabricantes.component.scss](/src/app/fabricantes/fabricantes.component.scss) | SCSS | 11 | 0 | 1 | 12 |
| [src/app/fabricantes/fabricantes.component.spec.ts](/src/app/fabricantes/fabricantes.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/fabricantes/fabricantes.component.ts](/src/app/fabricantes/fabricantes.component.ts) | TypeScript | 81 | 0 | 9 | 90 |
| [src/app/fornecedor/fornecedor.component.html](/src/app/fornecedor/fornecedor.component.html) | HTML | 98 | 5 | 2 | 105 |
| [src/app/fornecedor/fornecedor.component.scss](/src/app/fornecedor/fornecedor.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src/app/fornecedor/fornecedor.component.spec.ts](/src/app/fornecedor/fornecedor.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/fornecedor/fornecedor.component.ts](/src/app/fornecedor/fornecedor.component.ts) | TypeScript | 78 | 0 | 11 | 89 |
| [src/app/lista-aquisicao/lista-aquisicao.component.html](/src/app/lista-aquisicao/lista-aquisicao.component.html) | HTML | 90 | 6 | 5 | 101 |
| [src/app/lista-aquisicao/lista-aquisicao.component.scss](/src/app/lista-aquisicao/lista-aquisicao.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src/app/lista-aquisicao/lista-aquisicao.component.spec.ts](/src/app/lista-aquisicao/lista-aquisicao.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/lista-aquisicao/lista-aquisicao.component.ts](/src/app/lista-aquisicao/lista-aquisicao.component.ts) | TypeScript | 115 | 0 | 11 | 126 |
| [src/app/lista-transferencia/lista-transferencia.component.html](/src/app/lista-transferencia/lista-transferencia.component.html) | HTML | 56 | 5 | 5 | 66 |
| [src/app/lista-transferencia/lista-transferencia.component.scss](/src/app/lista-transferencia/lista-transferencia.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src/app/lista-transferencia/lista-transferencia.component.spec.ts](/src/app/lista-transferencia/lista-transferencia.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/lista-transferencia/lista-transferencia.component.ts](/src/app/lista-transferencia/lista-transferencia.component.ts) | TypeScript | 105 | 46 | 8 | 159 |
| [src/app/model/aquisicao.model.ts](/src/app/model/aquisicao.model.ts) | TypeScript | 148 | 5 | 10 | 163 |
| [src/app/model/arma.model.ts](/src/app/model/arma.model.ts) | TypeScript | 177 | 1 | 11 | 189 |
| [src/app/model/arquivo.model.ts](/src/app/model/arquivo.model.ts) | TypeScript | 30 | 0 | 4 | 34 |
| [src/app/model/calibre.model.ts](/src/app/model/calibre.model.ts) | TypeScript | 29 | 0 | 6 | 35 |
| [src/app/model/cliente.model.ts](/src/app/model/cliente.model.ts) | TypeScript | 319 | 3 | 19 | 341 |
| [src/app/model/endereco.model.ts](/src/app/model/endereco.model.ts) | TypeScript | 42 | 0 | 4 | 46 |
| [src/app/model/especie.model.ts](/src/app/model/especie.model.ts) | TypeScript | 26 | 0 | 4 | 30 |
| [src/app/model/fabricante.model.ts](/src/app/model/fabricante.model.ts) | TypeScript | 50 | 0 | 6 | 56 |
| [src/app/model/fornecedor.model.ts](/src/app/model/fornecedor.model.ts) | TypeScript | 69 | 0 | 7 | 76 |
| [src/app/model/modalidade.model.ts](/src/app/model/modalidade.model.ts) | TypeScript | 8 | 0 | 1 | 9 |
| [src/app/model/modelo.model.ts](/src/app/model/modelo.model.ts) | TypeScript | 120 | 0 | 8 | 128 |
| [src/app/model/pendencia.model.ts](/src/app/model/pendencia.model.ts) | TypeScript | 23 | 0 | 0 | 23 |
| [src/app/model/profissao.model.ts](/src/app/model/profissao.model.ts) | TypeScript | 8 | 0 | 1 | 9 |
| [src/app/model/transferencia.model.ts](/src/app/model/transferencia.model.ts) | TypeScript | 201 | 0 | 12 | 213 |
| [src/app/modelos/modelos.component.html](/src/app/modelos/modelos.component.html) | HTML | 147 | 13 | 3 | 163 |
| [src/app/modelos/modelos.component.scss](/src/app/modelos/modelos.component.scss) | SCSS | 11 | 0 | 1 | 12 |
| [src/app/modelos/modelos.component.spec.ts](/src/app/modelos/modelos.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/modelos/modelos.component.ts](/src/app/modelos/modelos.component.ts) | TypeScript | 158 | 9 | 18 | 185 |
| [src/app/services/cpf.validator.ts](/src/app/services/cpf.validator.ts) | TypeScript | 47 | 3 | 8 | 58 |
| [src/app/services/fire.service.ts](/src/app/services/fire.service.ts) | TypeScript | 785 | 23 | 73 | 881 |
| [src/app/services/pdf.service.ts](/src/app/services/pdf.service.ts) | TypeScript | 4,304 | 61 | 41 | 4,406 |
| [src/app/services/procedimentos.service.ts](/src/app/services/procedimentos.service.ts) | TypeScript | 266 | 4 | 18 | 288 |
| [src/app/services/util.service.spec.ts](/src/app/services/util.service.spec.ts) | TypeScript | 12 | 0 | 5 | 17 |
| [src/app/services/util.service.ts](/src/app/services/util.service.ts) | TypeScript | 124 | 15 | 13 | 152 |
| [src/app/transferencia-detail/transferencia-detail.component.html](/src/app/transferencia-detail/transferencia-detail.component.html) | HTML | 299 | 59 | 4 | 362 |
| [src/app/transferencia-detail/transferencia-detail.component.scss](/src/app/transferencia-detail/transferencia-detail.component.scss) | SCSS | 22 | 0 | 1 | 23 |
| [src/app/transferencia-detail/transferencia-detail.component.spec.ts](/src/app/transferencia-detail/transferencia-detail.component.spec.ts) | TypeScript | 20 | 0 | 6 | 26 |
| [src/app/transferencia-detail/transferencia-detail.component.ts](/src/app/transferencia-detail/transferencia-detail.component.ts) | TypeScript | 195 | 2 | 23 | 220 |
| [src/environments/environment.prod.ts](/src/environments/environment.prod.ts) | TypeScript | 3 | 0 | 1 | 4 |
| [src/environments/environment.ts](/src/environments/environment.ts) | TypeScript | 3 | 11 | 3 | 17 |
| [src/index.html](/src/index.html) | HTML | 13 | 0 | 1 | 14 |
| [src/main.ts](/src/main.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src/polyfills.ts](/src/polyfills.ts) | TypeScript | 1 | 55 | 8 | 64 |
| [src/styles.scss](/src/styles.scss) | SCSS | 0 | 1 | 1 | 2 |
| [src/test.ts](/src/test.ts) | TypeScript | 18 | 4 | 4 | 26 |
| [tsconfig.app.json](/tsconfig.app.json) | JSON | 14 | 1 | 1 | 16 |
| [tsconfig.json](/tsconfig.json) | JSON | 19 | 1 | 0 | 20 |
| [tsconfig.spec.json](/tsconfig.spec.json) | JSON | 17 | 1 | 1 | 19 |
| [tslint.json](/tslint.json) | JSON | 152 | 0 | 1 | 153 |

[summary](results.md)