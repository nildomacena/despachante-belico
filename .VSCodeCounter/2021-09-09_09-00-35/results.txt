Date : 2021-09-09 09:00:35
Directory : c:\Users\PC\Projetos\despachante-belico
Total : 109 files,  32502 codes, 649 comments, 784 blanks, all 33935 lines

Languages
+------------+------------+------------+------------+------------+------------+
| language   | files      | code       | comment    | blank      | total      |
+------------+------------+------------+------------+------------+------------+
| JSON       |          9 |     19,493 |         31 |          6 |     19,530 |
| TypeScript |         61 |      9,752 |        261 |        654 |     10,667 |
| HTML       |         18 |      3,021 |        347 |         82 |      3,450 |
| SCSS       |         17 |        164 |          2 |         24 |        190 |
| JavaScript |          3 |         58 |          8 |          4 |         70 |
| Markdown   |          1 |         14 |          0 |         14 |         28 |
+------------+------------+------------+------------+------------+------------+

Directories
+---------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                                | files      | code       | comment    | blank      | total      |
+---------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                                   |        109 |     32,502 |        649 |        784 |     33,935 |
| e2e                                                                                                                 |          4 |         68 |          8 |         11 |         87 |
| e2e\src                                                                                                             |          2 |         27 |          1 |          8 |         36 |
| fonts                                                                                                               |          1 |          1 |          0 |          0 |          1 |
| src                                                                                                                 |         94 |     12,910 |        609 |        752 |     14,271 |
| src\app                                                                                                             |         87 |     12,863 |        538 |        730 |     14,131 |
| src\app\aquisicao-detail                                                                                            |          4 |        640 |         66 |         37 |        743 |
| src\app\cadastrar-aquisicao-arma                                                                                    |          4 |        582 |         55 |         56 |        693 |
| src\app\cadastrar-transferencia                                                                                     |          4 |        475 |         13 |         44 |        532 |
| src\app\cadastro-arma                                                                                               |          4 |        541 |         14 |         39 |        594 |
| src\app\cadastro-cliente                                                                                            |          5 |      1,141 |         66 |         58 |      1,265 |
| src\app\calibres                                                                                                    |          4 |        172 |          5 |         20 |        197 |
| src\app\consulta-armas                                                                                              |          4 |        133 |         15 |         16 |        164 |
| src\app\consulta-clientes                                                                                           |          4 |        141 |          5 |         21 |        167 |
| src\app\especies                                                                                                    |          4 |        172 |          5 |         19 |        196 |
| src\app\fabricantes                                                                                                 |          4 |        182 |          5 |         18 |        205 |
| src\app\fornecedor                                                                                                  |          4 |        196 |          5 |         20 |        221 |
| src\app\lista-aquisicao                                                                                             |          4 |        225 |          6 |         23 |        254 |
| src\app\lista-transferencia                                                                                         |          4 |        181 |         51 |         20 |        252 |
| src\app\model                                                                                                       |         14 |      1,250 |          9 |         93 |      1,352 |
| src\app\modelos                                                                                                     |          4 |        336 |         22 |         28 |        386 |
| src\app\services                                                                                                    |          6 |      5,538 |        106 |        158 |      5,802 |
| src\app\transferencia-detail                                                                                        |          4 |        536 |         61 |         34 |        631 |
| src\environments                                                                                                    |          2 |          6 |         11 |          4 |         21 |
+---------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+---------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| filename                                                                                                            | language   | code       | comment    | blank      | total      |
+---------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| c:\Users\PC\Projetos\despachante-belico\README.md                                                                   | Markdown   |         14 |          0 |         14 |         28 |
| c:\Users\PC\Projetos\despachante-belico\angular.json                                                                | JSON       |        126 |         27 |          0 |        153 |
| c:\Users\PC\Projetos\despachante-belico\e2e\protractor.conf.js                                                      | JavaScript |         28 |          6 |          2 |         36 |
| c:\Users\PC\Projetos\despachante-belico\e2e\src\app.e2e-spec.ts                                                     | TypeScript |         18 |          1 |          5 |         24 |
| c:\Users\PC\Projetos\despachante-belico\e2e\src\app.po.ts                                                           | TypeScript |          9 |          0 |          3 |         12 |
| c:\Users\PC\Projetos\despachante-belico\e2e\tsconfig.json                                                           | JSON       |         13 |          1 |          1 |         15 |
| c:\Users\PC\Projetos\despachante-belico\firebase.json                                                               | JSON       |         28 |          0 |          0 |         28 |
| c:\Users\PC\Projetos\despachante-belico\fonts\times-new-roman.js                                                    | JavaScript |          1 |          0 |          0 |          1 |
| c:\Users\PC\Projetos\despachante-belico\karma.conf.js                                                               | JavaScript |         29 |          2 |          2 |         33 |
| c:\Users\PC\Projetos\despachante-belico\package-lock.json                                                           | JSON       |     19,060 |          0 |          1 |     19,061 |
| c:\Users\PC\Projetos\despachante-belico\package.json                                                                | JSON       |         64 |          0 |          1 |         65 |
| c:\Users\PC\Projetos\despachante-belico\src\app\app-routing.module.ts                                               | TypeScript |         41 |          0 |          4 |         45 |
| c:\Users\PC\Projetos\despachante-belico\src\app\app.component.html                                                  | HTML       |        229 |         27 |          4 |        260 |
| c:\Users\PC\Projetos\despachante-belico\src\app\app.component.scss                                                  | SCSS       |         12 |          0 |          1 |         13 |
| c:\Users\PC\Projetos\despachante-belico\src\app\app.component.spec.ts                                               | TypeScript |         31 |          0 |          5 |         36 |
| c:\Users\PC\Projetos\despachante-belico\src\app\app.component.ts                                                    | TypeScript |         10 |          0 |          3 |         13 |
| c:\Users\PC\Projetos\despachante-belico\src\app\app.module.ts                                                       | TypeScript |         99 |          2 |          9 |        110 |
| c:\Users\PC\Projetos\despachante-belico\src\app\aquisicao-detail\aquisicao-detail.component.html                    | HTML       |        303 |         55 |          5 |        363 |
| c:\Users\PC\Projetos\despachante-belico\src\app\aquisicao-detail\aquisicao-detail.component.scss                    | SCSS       |          8 |          0 |          0 |          8 |
| c:\Users\PC\Projetos\despachante-belico\src\app\aquisicao-detail\aquisicao-detail.component.spec.ts                 | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\aquisicao-detail\aquisicao-detail.component.ts                      | TypeScript |        309 |         11 |         26 |        346 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-aquisicao-arma\cadastrar-aquisicao-arma.component.html    | HTML       |        286 |         53 |         13 |        352 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-aquisicao-arma\cadastrar-aquisicao-arma.component.scss    | SCSS       |         19 |          0 |          4 |         23 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-aquisicao-arma\cadastrar-aquisicao-arma.component.spec.ts | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-aquisicao-arma\cadastrar-aquisicao-arma.component.ts      | TypeScript |        257 |          2 |         33 |        292 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-transferencia\cadastrar-transferencia.component.html      | HTML       |        227 |         12 |          8 |        247 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-transferencia\cadastrar-transferencia.component.scss      | SCSS       |         25 |          0 |          5 |         30 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-transferencia\cadastrar-transferencia.component.spec.ts   | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastrar-transferencia\cadastrar-transferencia.component.ts        | TypeScript |        203 |          1 |         25 |        229 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-arma\cadastro-arma.component.html                          | HTML       |        263 |         14 |         11 |        288 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-arma\cadastro-arma.component.scss                          | SCSS       |         11 |          0 |          1 |         12 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-arma\cadastro-arma.component.spec.ts                       | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-arma\cadastro-arma.component.ts                            | TypeScript |        247 |          0 |         21 |        268 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-cliente\backup.html                                        | HTML       |        227 |          7 |          1 |        235 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-cliente\cadastro-cliente.component.html                    | HTML       |        464 |         56 |          9 |        529 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-cliente\cadastro-cliente.component.scss                    | SCSS       |         12 |          1 |          2 |         15 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-cliente\cadastro-cliente.component.spec.ts                 | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\cadastro-cliente\cadastro-cliente.component.ts                      | TypeScript |        418 |          2 |         40 |        460 |
| c:\Users\PC\Projetos\despachante-belico\src\app\calibres\calibres.component.html                                    | HTML       |         62 |          5 |          2 |         69 |
| c:\Users\PC\Projetos\despachante-belico\src\app\calibres\calibres.component.scss                                    | SCSS       |         11 |          0 |          1 |         12 |
| c:\Users\PC\Projetos\despachante-belico\src\app\calibres\calibres.component.spec.ts                                 | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\calibres\calibres.component.ts                                      | TypeScript |         79 |          0 |         11 |         90 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-armas\consulta-armas.component.html                        | HTML       |         58 |         15 |          3 |         76 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-armas\consulta-armas.component.scss                        | SCSS       |         11 |          0 |          1 |         12 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-armas\consulta-armas.component.spec.ts                     | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-armas\consulta-armas.component.ts                          | TypeScript |         44 |          0 |          6 |         50 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-clientes\consulta-clientes.component.html                  | HTML       |         67 |          5 |          2 |         74 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-clientes\consulta-clientes.component.scss                  | SCSS       |          0 |          0 |          1 |          1 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-clientes\consulta-clientes.component.spec.ts               | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\consulta-clientes\consulta-clientes.component.ts                    | TypeScript |         54 |          0 |         12 |         66 |
| c:\Users\PC\Projetos\despachante-belico\src\app\especies\especies.component.html                                    | HTML       |         62 |          5 |          2 |         69 |
| c:\Users\PC\Projetos\despachante-belico\src\app\especies\especies.component.scss                                    | SCSS       |         11 |          0 |          1 |         12 |
| c:\Users\PC\Projetos\despachante-belico\src\app\especies\especies.component.spec.ts                                 | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\especies\especies.component.ts                                      | TypeScript |         79 |          0 |         10 |         89 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fabricantes\fabricantes.component.html                              | HTML       |         70 |          5 |          2 |         77 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fabricantes\fabricantes.component.scss                              | SCSS       |         11 |          0 |          1 |         12 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fabricantes\fabricantes.component.spec.ts                           | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fabricantes\fabricantes.component.ts                                | TypeScript |         81 |          0 |          9 |         90 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fornecedor\fornecedor.component.html                                | HTML       |         98 |          5 |          2 |        105 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fornecedor\fornecedor.component.scss                                | SCSS       |          0 |          0 |          1 |          1 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fornecedor\fornecedor.component.spec.ts                             | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\fornecedor\fornecedor.component.ts                                  | TypeScript |         78 |          0 |         11 |         89 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-aquisicao\lista-aquisicao.component.html                      | HTML       |         90 |          6 |          5 |        101 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-aquisicao\lista-aquisicao.component.scss                      | SCSS       |          0 |          0 |          1 |          1 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-aquisicao\lista-aquisicao.component.spec.ts                   | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-aquisicao\lista-aquisicao.component.ts                        | TypeScript |        115 |          0 |         11 |        126 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-transferencia\lista-transferencia.component.html              | HTML       |         56 |          5 |          5 |         66 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-transferencia\lista-transferencia.component.scss              | SCSS       |          0 |          0 |          1 |          1 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-transferencia\lista-transferencia.component.spec.ts           | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\lista-transferencia\lista-transferencia.component.ts                | TypeScript |        105 |         46 |          8 |        159 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\aquisicao.model.ts                                            | TypeScript |        148 |          5 |         10 |        163 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\arma.model.ts                                                 | TypeScript |        177 |          1 |         11 |        189 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\arquivo.model.ts                                              | TypeScript |         30 |          0 |          4 |         34 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\calibre.model.ts                                              | TypeScript |         29 |          0 |          6 |         35 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\cliente.model.ts                                              | TypeScript |        319 |          3 |         19 |        341 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\endereco.model.ts                                             | TypeScript |         42 |          0 |          4 |         46 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\especie.model.ts                                              | TypeScript |         26 |          0 |          4 |         30 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\fabricante.model.ts                                           | TypeScript |         50 |          0 |          6 |         56 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\fornecedor.model.ts                                           | TypeScript |         69 |          0 |          7 |         76 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\modalidade.model.ts                                           | TypeScript |          8 |          0 |          1 |          9 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\modelo.model.ts                                               | TypeScript |        120 |          0 |          8 |        128 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\pendencia.model.ts                                            | TypeScript |         23 |          0 |          0 |         23 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\profissao.model.ts                                            | TypeScript |          8 |          0 |          1 |          9 |
| c:\Users\PC\Projetos\despachante-belico\src\app\model\transferencia.model.ts                                        | TypeScript |        201 |          0 |         12 |        213 |
| c:\Users\PC\Projetos\despachante-belico\src\app\modelos\modelos.component.html                                      | HTML       |        147 |         13 |          3 |        163 |
| c:\Users\PC\Projetos\despachante-belico\src\app\modelos\modelos.component.scss                                      | SCSS       |         11 |          0 |          1 |         12 |
| c:\Users\PC\Projetos\despachante-belico\src\app\modelos\modelos.component.spec.ts                                   | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\modelos\modelos.component.ts                                        | TypeScript |        158 |          9 |         18 |        185 |
| c:\Users\PC\Projetos\despachante-belico\src\app\services\cpf.validator.ts                                           | TypeScript |         47 |          3 |          8 |         58 |
| c:\Users\PC\Projetos\despachante-belico\src\app\services\fire.service.ts                                            | TypeScript |        785 |         23 |         73 |        881 |
| c:\Users\PC\Projetos\despachante-belico\src\app\services\pdf.service.ts                                             | TypeScript |      4,304 |         61 |         41 |      4,406 |
| c:\Users\PC\Projetos\despachante-belico\src\app\services\procedimentos.service.ts                                   | TypeScript |        266 |          4 |         18 |        288 |
| c:\Users\PC\Projetos\despachante-belico\src\app\services\util.service.spec.ts                                       | TypeScript |         12 |          0 |          5 |         17 |
| c:\Users\PC\Projetos\despachante-belico\src\app\services\util.service.ts                                            | TypeScript |        124 |         15 |         13 |        152 |
| c:\Users\PC\Projetos\despachante-belico\src\app\transferencia-detail\transferencia-detail.component.html            | HTML       |        299 |         59 |          4 |        362 |
| c:\Users\PC\Projetos\despachante-belico\src\app\transferencia-detail\transferencia-detail.component.scss            | SCSS       |         22 |          0 |          1 |         23 |
| c:\Users\PC\Projetos\despachante-belico\src\app\transferencia-detail\transferencia-detail.component.spec.ts         | TypeScript |         20 |          0 |          6 |         26 |
| c:\Users\PC\Projetos\despachante-belico\src\app\transferencia-detail\transferencia-detail.component.ts              | TypeScript |        195 |          2 |         23 |        220 |
| c:\Users\PC\Projetos\despachante-belico\src\environments\environment.prod.ts                                        | TypeScript |          3 |          0 |          1 |          4 |
| c:\Users\PC\Projetos\despachante-belico\src\environments\environment.ts                                             | TypeScript |          3 |         11 |          3 |         17 |
| c:\Users\PC\Projetos\despachante-belico\src\index.html                                                              | HTML       |         13 |          0 |          1 |         14 |
| c:\Users\PC\Projetos\despachante-belico\src\main.ts                                                                 | TypeScript |          9 |          0 |          4 |         13 |
| c:\Users\PC\Projetos\despachante-belico\src\polyfills.ts                                                            | TypeScript |          1 |         55 |          8 |         64 |
| c:\Users\PC\Projetos\despachante-belico\src\styles.scss                                                             | SCSS       |          0 |          1 |          1 |          2 |
| c:\Users\PC\Projetos\despachante-belico\src\test.ts                                                                 | TypeScript |         18 |          4 |          4 |         26 |
| c:\Users\PC\Projetos\despachante-belico\tsconfig.app.json                                                           | JSON       |         14 |          1 |          1 |         16 |
| c:\Users\PC\Projetos\despachante-belico\tsconfig.json                                                               | JSON       |         19 |          1 |          0 |         20 |
| c:\Users\PC\Projetos\despachante-belico\tsconfig.spec.json                                                          | JSON       |         17 |          1 |          1 |         19 |
| c:\Users\PC\Projetos\despachante-belico\tslint.json                                                                 | JSON       |        152 |          0 |          1 |        153 |
| Total                                                                                                               |            |     32,502 |        649 |        784 |     33,935 |
+---------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+