# Summary

Date : 2021-10-13 23:16:10

Directory c:\Users\PC\Projetos\despachante-belico

Total : 125 files,  33667 codes, 729 comments, 876 blanks, all 35272 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 9 | 19,493 | 31 | 6 | 19,530 |
| TypeScript | 71 | 10,489 | 268 | 722 | 11,479 |
| HTML | 21 | 3,446 | 420 | 103 | 3,969 |
| SCSS | 20 | 167 | 2 | 27 | 196 |
| JavaScript | 3 | 58 | 8 | 4 | 70 |
| Markdown | 1 | 14 | 0 | 14 | 28 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 125 | 33,667 | 729 | 876 | 35,272 |
| e2e | 4 | 68 | 8 | 11 | 87 |
| e2e\src | 2 | 27 | 1 | 8 | 36 |
| fonts | 1 | 1 | 0 | 0 | 1 |
| src | 110 | 14,075 | 689 | 844 | 15,608 |
| src\app | 103 | 14,028 | 618 | 822 | 15,468 |
| src\app\aquisicao-detail | 4 | 640 | 66 | 37 | 743 |
| src\app\cadastrar-aquisicao-arma | 4 | 582 | 55 | 56 | 693 |
| src\app\cadastrar-transferencia | 4 | 471 | 17 | 44 | 532 |
| src\app\cadastro-arma | 4 | 542 | 15 | 39 | 596 |
| src\app\cadastro-cliente | 5 | 1,141 | 66 | 59 | 1,266 |
| src\app\calibres | 4 | 172 | 5 | 20 | 197 |
| src\app\consulta-armas | 4 | 133 | 15 | 16 | 164 |
| src\app\consulta-clientes | 4 | 141 | 5 | 21 | 167 |
| src\app\especies | 4 | 172 | 5 | 19 | 196 |
| src\app\fabricantes | 4 | 182 | 5 | 18 | 205 |
| src\app\fornecedor | 4 | 196 | 5 | 20 | 221 |
| src\app\lista-aquisicao | 4 | 225 | 6 | 23 | 254 |
| src\app\lista-pre-cadastros | 4 | 126 | 5 | 17 | 148 |
| src\app\lista-transferencia | 4 | 181 | 51 | 20 | 252 |
| src\app\login | 4 | 197 | 19 | 23 | 239 |
| src\app\model | 16 | 1,316 | 9 | 93 | 1,418 |
| src\app\modelos | 4 | 336 | 22 | 28 | 386 |
| src\app\pre-cadastro | 4 | 483 | 47 | 31 | 561 |
| src\app\services | 8 | 5,696 | 107 | 175 | 5,978 |
| src\app\transferencia-detail | 4 | 536 | 61 | 34 | 631 |
| src\environments | 2 | 6 | 11 | 4 | 21 |

[details](details.md)