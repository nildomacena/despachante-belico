# Diff Summary

Date : 2022-02-10 16:46:28

Directory c:\Users\PC\Projetos\despachante-belico

Total : 134 files,  34803 codes, 772 comments, 907 blanks, all 36482 lines

[summary](results.md) / [details](details.md) / diff summary / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 9 | 19,568 | 29 | 6 | 19,603 |
| TypeScript | 76 | 11,382 | 292 | 743 | 12,417 |
| HTML | 22 | 3,576 | 439 | 111 | 4,126 |
| SCSS | 21 | 173 | 2 | 28 | 203 |
| JavaScript | 3 | 58 | 8 | 4 | 70 |
| JSON with Comments | 2 | 32 | 2 | 1 | 35 |
| Markdown | 1 | 14 | 0 | 14 | 28 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 134 | 34,803 | 772 | 907 | 36,482 |
| e2e | 4 | 68 | 8 | 11 | 87 |
| e2e\src | 2 | 27 | 1 | 8 | 36 |
| fonts | 1 | 1 | 0 | 0 | 1 |
| src | 117 | 15,104 | 732 | 874 | 16,710 |
| src\app | 110 | 15,057 | 661 | 852 | 16,570 |
| src\app\aniversariantes | 4 | 85 | 2 | 12 | 99 |
| src\app\aquisicao-detail | 4 | 640 | 66 | 37 | 743 |
| src\app\cadastrar-aquisicao-arma | 4 | 582 | 55 | 56 | 693 |
| src\app\cadastrar-transferencia | 4 | 523 | 17 | 39 | 579 |
| src\app\cadastro-arma | 4 | 542 | 15 | 39 | 596 |
| src\app\cadastro-cliente | 8 | 1,718 | 96 | 77 | 1,891 |
| src\app\cadastro-cliente\mocks | 1 | 34 | 0 | 2 | 36 |
| src\app\calibres | 4 | 172 | 5 | 20 | 197 |
| src\app\consulta-armas | 4 | 133 | 15 | 16 | 164 |
| src\app\consulta-clientes | 4 | 141 | 5 | 21 | 167 |
| src\app\especies | 4 | 172 | 5 | 19 | 196 |
| src\app\fabricantes | 4 | 182 | 5 | 18 | 205 |
| src\app\fornecedor | 4 | 196 | 5 | 20 | 221 |
| src\app\lista-aquisicao | 4 | 225 | 6 | 23 | 254 |
| src\app\lista-pre-cadastros | 4 | 126 | 5 | 17 | 148 |
| src\app\lista-transferencia | 4 | 181 | 51 | 20 | 252 |
| src\app\login | 4 | 197 | 19 | 23 | 239 |
| src\app\model | 16 | 1,375 | 11 | 87 | 1,473 |
| src\app\modelos | 4 | 336 | 22 | 28 | 386 |
| src\app\pre-cadastro | 4 | 483 | 47 | 31 | 561 |
| src\app\services | 8 | 5,924 | 116 | 182 | 6,222 |
| src\app\transferencia-detail | 4 | 534 | 61 | 35 | 630 |
| src\environments | 2 | 6 | 11 | 4 | 21 |
| target | 1 | 1 | 0 | 0 | 1 |

[summary](results.md) / [details](details.md) / diff summary / [diff details](diff-details.md)